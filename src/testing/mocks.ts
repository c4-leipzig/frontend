export class AuthMock {
  loginWithPassword = (username: string, password: string) => null;
  logOut = () => null;
}

export class OAuthMock {
  logOut = () => null;
  fetchTokenUsingPasswordFlowAndLoadUserProfile = (a, b) => null;
}

export class LoginServiceMock {
  startLoginProcess = () => null;
  logout = () => null;
}

export class ActivatedRouteSnapshotMock {
  constructor(private data: any) {
  }
}
