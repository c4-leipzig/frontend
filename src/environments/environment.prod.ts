const baseUrl = 'http://192.168.178.4'; // WMS6
const departmentBase = `${baseUrl}:8001`;
const userBase = `${baseUrl}:8080`;

export const environment = {
  production: true,
  auth: {
    issuer: `${baseUrl}:8081/auth/realms/master`,
    redirectUri: window.location.origin,
    clientId: 'frontend',
    scope: 'openid offline_access',
    timeoutFactor: 0.75,
    requireHttps: false,
    responseType: 'code',
    oidc: false
  },
  cacheTime: 10 * 60 * 1000,
  endpoints: {
    departments: {
      categories: {
        allCategories: `${departmentBase}/categories`,
        update: `${departmentBase}/categories/{id}`,
        create: `${departmentBase}/categories`
      },
      assignmentTerms: {
        allForDepartment: `${departmentBase}/assignments/department/{id}`
      },
      allDepartments: `${departmentBase}/departments/withActiveAssignment`,
      update: `${departmentBase}/departments/department/{id}`,
      create: `${departmentBase}/departments`

    },
    users: {
      members: `${userBase}/users/members`
    }
  },
  validation: {
    departments: {
      name: {
        maxLength: 100
      },
      description: {
        maxLength: 4000
      }
    }
  }
};
