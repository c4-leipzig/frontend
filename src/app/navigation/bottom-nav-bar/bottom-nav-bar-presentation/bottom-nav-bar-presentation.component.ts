import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BottomNav} from '../../_shared/bottom-nav';

@Component({
  selector: 'c4-bottom-nav-bar-presentation',
  templateUrl: './bottom-nav-bar-presentation.component.html',
  styleUrls: ['./bottom-nav-bar-presentation.component.scss']
})
export class BottomNavBarPresentationComponent {
  @Input() bottomNav: BottomNav;

  @Output() openSideNav = new EventEmitter<void>();
}
