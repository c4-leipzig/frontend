import {Component, Input} from '@angular/core';
import {NavButton} from '../../../_shared/nav-button';

@Component({
  selector: 'c4-bottom-nav-bar-button',
  templateUrl: './bottom-nav-bar-button.component.html',
  styleUrls: ['./bottom-nav-bar-button.component.scss']
})
export class BottomNavBarButtonComponent {
  @Input() navButton: NavButton;

  get isExecuteAction(): boolean {
    return !!this.navButton?.execute;
  }

}
