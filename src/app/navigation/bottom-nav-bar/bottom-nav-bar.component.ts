import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../_store/base.reducer';
import {toggleNav} from '../_store/navigation.actions';
import {bottomNav} from '../_store/navigation.selectors';

@Component({
  selector: 'c4-bottom-nav-bar',
  templateUrl: './bottom-nav-bar.component.html',
  styleUrls: ['./bottom-nav-bar.component.scss']
})
export class BottomNavBarComponent {
  bottomNav$ = this.store.select(bottomNav);

  constructor(private store: Store<State>) {
  }

  toggleSide(): void {
    this.store.dispatch(toggleNav());
  }

}
