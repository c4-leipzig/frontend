import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {MenuItem} from 'primeng/api';
import {Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {State} from '../../_store/base.reducer';
import {loggedInUserName} from '../../auth/_store/auth.selectors';
import {LoginService} from '../../auth/login.service';
import {closeNav} from '../_store/navigation.actions';
import {displayHeight, sideNavOpened} from '../_store/navigation.selectors';

@Component({
  selector: 'c4-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit, OnDestroy {
  sideNavOpened$ = this.store.pipe(select(sideNavOpened));
  height$ = this.store.select(displayHeight).pipe(map(height => height - 20));
  menuItems: MenuItem[] = [];

  private destroy$ = new Subject();

  constructor(private store: Store<State>,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.menuItems = SideBarComponent.staticMenuItems;
    this.store.pipe(select(loggedInUserName), takeUntil(this.destroy$)).subscribe(username => this.updateMenu(username));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  onClose(): void {
    this.store.dispatch(closeNav());
  }

  private updateMenu(username?: string): void {
    const result: MenuItem[] = [];

    result.push(this.getUserMenuItem(username), ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems,
      ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems,
      ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems,
      ...SideBarComponent.staticMenuItems, ...SideBarComponent.staticMenuItems);
    this.menuItems = result;
  }

  private getUserMenuItem(username?: string): MenuItem {
    if (username) {
      return {
        label: username,
        icon: 'fa fa-user',
        items: [
          {
            label: 'Logout',
            icon: 'fa fa-sign-out-alt',
            command: () => this.loginService.logout()
          }
        ]
      };
    }
    return {
      label: 'Login',
      icon: 'fa fa-sign-in-alt',
      command: () => this.loginService.startLoginProcess()
    };
  }

  private static get staticMenuItems(): MenuItem[] {
    return [
      {
        label: 'Ressorts',
        icon: 'fa fa-briefcase',
        items: [
          {
            label: 'Übersicht',
            icon: 'fa fa-briefcase',
            routerLink: 'departments'
          },
          {
            label: 'Kategorien',
            icon: 'fa fa-th',
            routerLink: 'departments/categories'
          }
        ]
      }
    ];
  }
}
