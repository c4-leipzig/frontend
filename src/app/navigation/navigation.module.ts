import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {Store, StoreModule} from '@ngrx/store';
import {TooltipModule} from 'primeng/tooltip';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {SidebarModule} from 'primeng/sidebar';
import {SlideMenuModule} from 'primeng/slidemenu';
import {ToolbarModule} from 'primeng/toolbar';
import {fromEvent} from 'rxjs';
import {map, startWith, throttleTime} from 'rxjs/operators';
import {SharedModule} from '../_shared/shared.module';
import {State} from '../_store/base.reducer';
import {setDisplaySize} from './_store/navigation.actions';
import * as fromNavigation from './_store/navigation.reducer';
import {BottomNavBarButtonComponent} from './bottom-nav-bar/bottom-nav-bar-presentation/bottom-nav-bar-button/bottom-nav-bar-button.component';
import {BottomNavBarPresentationComponent} from './bottom-nav-bar/bottom-nav-bar-presentation/bottom-nav-bar-presentation.component';
import {BottomNavBarComponent} from './bottom-nav-bar/bottom-nav-bar.component';
import {SideBarComponent} from './side-bar/side-bar.component';
import {TopNavBarComponent} from './top-nav/top-nav-bar/top-nav-bar.component';
import { SearchComponent } from './top-nav/search/search.component';
import { MobileTopNavBarComponent } from './top-nav/mobile-top-nav-bar/mobile-top-nav-bar.component';
import { ButtonBarComponent } from './top-nav/top-nav-bar/button-bar/button-bar.component';
import { ButtonBarButtonComponent } from './top-nav/top-nav-bar/button-bar/button-bar-button/button-bar-button.component';


@NgModule({
  declarations: [
    TopNavBarComponent,
    SideBarComponent,
    BottomNavBarComponent,
    BottomNavBarPresentationComponent,
    BottomNavBarButtonComponent,
    SearchComponent,
    MobileTopNavBarComponent,
    ButtonBarComponent,
    ButtonBarButtonComponent
  ],
  imports: [
    CommonModule,
    ToolbarModule,
    ButtonModule,
    SidebarModule,
    SlideMenuModule,
    StoreModule.forFeature(fromNavigation.navigationFeatureKey, fromNavigation.reducer),
    InputTextModule,
    TooltipModule,
    SharedModule
  ],
  providers: [
    {
      provide: Window,
      useValue: window
    }
  ],
  exports: [TopNavBarComponent, SideBarComponent, BottomNavBarComponent, MobileTopNavBarComponent]
})
export class NavigationModule {
  constructor(store: Store<State>, window: Window) {
    fromEvent(window, 'resize')
      .pipe(startWith(() => null), map(() => {
        return {height: window.innerHeight, width: window.innerWidth};
      }), throttleTime(100))
      .subscribe(size => store.dispatch(setDisplaySize(size)));
  }
}
