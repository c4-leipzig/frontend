import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {delay} from 'rxjs/operators';
import {State} from '../../../_store/base.reducer';
import {setSearchTerm} from '../../_store/navigation.actions';
import {searchConfig, searchTerm} from '../../_store/navigation.selectors';

@Component({
  selector: 'c4-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {
  searchTerm$ = this.store.select(searchTerm);
  searchConfig$ = this.store.pipe(select(searchConfig), delay(1)); // ohne Delay wird ein ExpressionChangedAfter... geworfen

  constructor(private store: Store<State>) {
  }

  ngOnInit(): void {
  }

  setSearch(search?: string): void {
    this.store.dispatch(setSearchTerm({searchTerm: search}));
  }
}
