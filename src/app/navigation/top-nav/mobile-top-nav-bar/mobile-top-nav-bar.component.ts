import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {searchConfig} from '../../_store/navigation.selectors';

@Component({
  selector: 'c4-mobile-top-nav-bar',
  templateUrl: './mobile-top-nav-bar.component.html',
  styleUrls: ['./mobile-top-nav-bar.component.scss']
})
export class MobileTopNavBarComponent {
  showSearch$ = this.store.select(searchConfig);

  constructor(private store: Store<State>) {
  }
}
