import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../../_store/base.reducer';
import {topNav} from '../../../_store/navigation.selectors';

@Component({
  selector: 'c4-button-bar',
  templateUrl: './button-bar.component.html',
  styleUrls: ['./button-bar.component.scss']
})
export class ButtonBarComponent {
  topNav$ = this.store.select(topNav);

  constructor(private store: Store<State>) {
  }
}
