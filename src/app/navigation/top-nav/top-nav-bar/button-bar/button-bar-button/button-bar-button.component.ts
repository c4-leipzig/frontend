import {Component, Input} from '@angular/core';
import {Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import {State} from '../../../../../_store/base.reducer';
import {LabelledNavButton} from '../../../../_shared/nav-button';
import {displayWidth} from '../../../../_store/navigation.selectors';

@Component({
  selector: 'c4-button-bar-button',
  templateUrl: './button-bar-button.component.html',
  styleUrls: ['./button-bar-button.component.scss']
})
export class ButtonBarButtonComponent {
  @Input() navButton: LabelledNavButton;

  // Todo das ist schon ein bisschen russisch, aber solange es ausschließlich hier verwendet wird erstmal noch Ok.
  showLabel$ = this.store.select(displayWidth).pipe(map(width => width >= 992));

  constructor(private store: Store<State>) {
  }

  get isExecuteAction(): boolean {
    return !!this.navButton?.execute;
  }
}
