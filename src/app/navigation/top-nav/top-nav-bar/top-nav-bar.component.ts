import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {isLoggedIn} from '../../../auth/_store/auth.selectors';
import {LoginService} from '../../../auth/login.service';
import {toggleNav} from '../../_store/navigation.actions';

@Component({
  selector: 'c4-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.scss']
})
export class TopNavBarComponent {
  isLoggedIn$ = this.store.select(isLoggedIn);

  constructor(private store: Store<State>,
              private loginService: LoginService) {
  }

  toggleSide(): void {
    this.store.dispatch(toggleNav());
  }

  login(): void {
    this.loginService.startLoginProcess();
  }
}
