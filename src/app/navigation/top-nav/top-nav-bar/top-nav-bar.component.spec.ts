import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {LoginServiceMock} from '../../../../testing/mocks';
import {initialState as authInitialState} from '../../../auth/_store/auth.reducer';
import {LoginService} from '../../../auth/login.service';
import {toggleNav} from '../../_store/navigation.actions';
import {initialState} from '../../_store/navigation.reducer';

import {TopNavBarComponent} from './top-nav-bar.component';

describe('NavBarComponent', () => {
  let component: TopNavBarComponent;
  let fixture: ComponentFixture<TopNavBarComponent>;
  let store: MockStore;

  const loginMock = new LoginServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TopNavBarComponent],
      providers: [
        provideMockStore({initialState: {navigation: initialState, auth: authInitialState}}),
        {provide: LoginService, useValue: loginMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.inject(MockStore);
  });

  it('should toggle sidenav', () => {
    spyOn(store, 'dispatch').and.callThrough();
    component.toggleSide();

    expect(store.dispatch).toHaveBeenCalledWith(toggleNav());
  });

  it('should start login process', () => {
    spyOn(loginMock, 'startLoginProcess').and.callThrough();

    component.login();

    expect(loginMock.startLoginProcess).toHaveBeenCalledTimes(1);
  });
});
