export interface NavButton {
  iconClass?: string;
  buttonClass?: string;
  execute?: () => void;
  routerLink?: string | string[];
}

export interface LabelledNavButton extends NavButton {
  label: string;
}
