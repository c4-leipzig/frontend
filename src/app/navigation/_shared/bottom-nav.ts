import {LabelledNavButton, NavButton} from './nav-button';

export interface BottomNav {
  button1?: NavButton;
  button2?: NavButton;
  button3?: NavButton;
  button4?: NavButton;
}


export function backButton(execute: () => void, buttonClass = 'p-button-secondary'): NavButton {
  return {
    iconClass: 'fa fa-chevron-left',
    buttonClass: buttonClass,
    execute: execute
  };
}

export function labelledBackButton(execute: () => void, label = 'Zurück', buttonClass = 'p-button-text'): LabelledNavButton {
  return {...backButton(execute, buttonClass), label: label};
}

export function labelledBackLink(routerLink: string, label = 'Zurück', buttonClass = 'p-button-text'): LabelledNavButton {
  return {
    iconClass: 'fa fa-chevron-left',
    buttonClass: buttonClass,
    routerLink: routerLink,
    label: label
  };
}

export function backButtonSeverity(execute: () => void, buttonSeverity: ButtonSeverity): NavButton {
  return {
    iconClass: 'fa fa-chevron-left',
    buttonClass: buttonSeverity,
    execute: execute
  };
}

export function backLinkSeverity(routerLink: string, buttonSeverity: ButtonSeverity): NavButton {
  return {
    iconClass: 'fa fa-chevron-left',
    buttonClass: buttonSeverity,
    routerLink: routerLink
  };
}

export function createButton(execute: () => void, buttonClass = 'p-button-success'): NavButton {
  return {
    iconClass: 'fa fa-plus',
    buttonClass: buttonClass,
    execute: execute
  };
}

export function createLink(routerLink: any[] | string, buttonClass = 'p-button-success'): NavButton {
  return {
    iconClass: 'fa fa-plus',
    buttonClass: buttonClass,
    routerLink: routerLink
  };
}

export function labelledCreateLink(routerLink: any[] | string, label: string, buttonClass = 'p-button-success'): LabelledNavButton {
  return {...createLink(routerLink, buttonClass), label: label};
}

export function labelledCreateButton(execute: () => void, label: string, buttonClass = 'p-button-success'): LabelledNavButton {
  return {
    ...createButton(execute, buttonClass),
    label: label
  };
}

export function editButton(execute: () => void, buttonClass = 'p-button-secondary'): NavButton {
  return {
    iconClass: 'fa fa-edit',
    buttonClass: buttonClass,
    execute: execute
  };
}

export function labelledEditButton(execute: () => void, label = 'Bearbeiten', buttonClass = 'p-button-secondary'): LabelledNavButton {
  return {
    ...editButton(execute, buttonClass),
    label: label
  };
}

export const enum ButtonSeverity {
  warning = 'p-button-warning',
  secondary = 'p-button-secondary'
}
