export interface SearchConfig {
  placeholder?: string;
}

export function createSearchConfig(placeholder: string): SearchConfig {
  return {placeholder: placeholder};
}
