import {LabelledNavButton} from './nav-button';

export interface TopNav {
  button1?: LabelledNavButton;
  button2?: LabelledNavButton;
  button3?: LabelledNavButton;
  button4?: LabelledNavButton;
  button5?: LabelledNavButton;
  button6?: LabelledNavButton;
  button7?: LabelledNavButton;
  button8?: LabelledNavButton;
}
