import {createAction, props} from '@ngrx/store';
import {BottomNav} from '../_shared/bottom-nav';
import {SearchConfig} from '../_shared/search-config';
import {TopNav} from '../_shared/top-nav';

export const openNav = createAction(
  '[Navigation] Open Nav'
);

export const closeNav = createAction(
  '[Navigation] Close Nav'
);

export const toggleNav = createAction(
  '[Navigation] Toggle Nav'
);

export const resetBottomNav = createAction(
  '[Navigation] resetBottomNav'
);

export const setBottomNav = createAction(
  '[Navigation] setBottomNav',
  props<{ bottomNav: BottomNav }>()
);

export const setDisplaySize = createAction(
  '[Navigation] setDisplayWidth',
  props<{ width: number, height: number }>()
);

export const setSearchTerm = createAction(
  '[Navigation] setSearchTerm',
  props<{ searchTerm?: string }>()
);

export const resetSearch = createAction(
  '[Navigation] resetSearchTerm'
);

export const setSearchConfig = createAction(
  '[Navigation] setSearchConfig',
  props<{searchConfig: SearchConfig}>()
);

export const resetTopNav = createAction(
  '[Navigation] resetTopNav'
);

export const setTopNav = createAction(
  '[Navigation] setTopNav',
  props<{ topNav: TopNav }>()
);

export const setTopAndBottomNav = createAction(
  '[Navigation] setTopAndBottomNav',
  props<{ bottomNav: BottomNav, topNav: TopNav }>()
);
