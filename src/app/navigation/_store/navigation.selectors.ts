import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromNavigation from './navigation.reducer';

export const selectNavigationState = createFeatureSelector<fromNavigation.State>(
  fromNavigation.navigationFeatureKey
);

export const sideNavOpened = createSelector(selectNavigationState, state => state.sideNavOpened);
export const displayWidth = createSelector(selectNavigationState, state => state.displaySize.width);
export const displayHeight = createSelector(selectNavigationState, state => state.displaySize.height);

export const searchTerm = createSelector(selectNavigationState, state => state.search.searchTerm);
export const searchConfig = createSelector(selectNavigationState, state => state.search.searchConfig);

export const bottomNav = createSelector(selectNavigationState, state => state.bottomNav);
export const topNav = createSelector(selectNavigationState, state => state.topNav);
