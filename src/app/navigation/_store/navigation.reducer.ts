import {createReducer, on} from '@ngrx/store';
import {BottomNav} from '../_shared/bottom-nav';
import {SearchConfig} from '../_shared/search-config';
import {TopNav} from '../_shared/top-nav';
import * as NavigationActions from './navigation.actions';

export const navigationFeatureKey = 'navigation';

export interface State {
  sideNavOpened: boolean;
  bottomNav: BottomNav;
  topNav: TopNav;
  displaySize: {
    width: number;
    height: number;
  };
  search: {
    searchTerm?: string;
    searchConfig?: SearchConfig
  };
}

export const initialBottomNavState: BottomNav = {};
export const initialTopNavState: TopNav = {};

export const initialState: State = {
  sideNavOpened: false,
  bottomNav: initialBottomNavState,
  topNav: initialTopNavState,
  displaySize: {
    width: 0,
    height: 0
  },
  search: {
    searchTerm: undefined,
    searchConfig: undefined
  }
};

export const reducer = createReducer(
  initialState,
  on(NavigationActions.openNav, state => {
    return {...state, sideNavOpened: true};
  }),
  on(NavigationActions.closeNav, state => {
    return {...state, sideNavOpened: false};
  }),
  on(NavigationActions.toggleNav, state => {
    return {...state, sideNavOpened: !state.sideNavOpened};
  }),

  on(NavigationActions.resetBottomNav, state => {
    return {...state, bottomNav: initialBottomNavState};
  }),
  on(NavigationActions.setBottomNav, (state, update) => {
    return {...state, bottomNav: update.bottomNav};
  }),

  on(NavigationActions.resetTopNav, state => {
    return {...state, topNav: initialBottomNavState};
  }),
  on(NavigationActions.setTopNav, (state, update) => {
    return {...state, topNav: update.topNav};
  }),

  on(NavigationActions.setTopAndBottomNav, (state, update) => {
    return {...state, topNav: update.topNav, bottomNav: update.bottomNav};
  }),

  on(NavigationActions.setDisplaySize, (state, update) => {
    return {...state, displaySize: {height: update.height, width: update.width}};
  }),

  on(NavigationActions.setSearchTerm, (state, update) => {
    return {...state, search: {...state.search, searchTerm: update.searchTerm}};
  }),
  on(NavigationActions.resetSearch, state => {
    return {...state, search: {...state.search, searchTerm: undefined, searchConfig: undefined}};
  }),
  on(NavigationActions.setSearchConfig, (state, config) => {
    return {...state, search: {...state.search, searchConfig: config.searchConfig}};
  })
);

