import * as fromNavigation from '../navigation.reducer';
import {initialState} from '../navigation.reducer';
import {
  bottomNav,
  displayHeight,
  displayWidth,
  searchConfig,
  searchTerm,
  selectNavigationState,
  sideNavOpened,
  topNav
} from '../navigation.selectors';

describe('NavigationSelectors', () => {
  it('should select the feature state', () => {
    const result = selectNavigationState({
      [fromNavigation.navigationFeatureKey]: {a: 1}
    });

    expect(result).toEqual({a: 1});
  });

  describe('sideNavOpened', () => {
    it('should yield the current state of the sidenav correctly', () => {
      expect(sideNavOpened({navigation: initialState})).toBe(false);
      expect(sideNavOpened({navigation: {...initialState, sideNavOpened: true}})).toBe(true);
    });
  });

  describe('bottomNav', () => {
    it('should yield the bottomNav state', () => {
      expect(bottomNav({navigation: initialState})).toStrictEqual({});

      const bottomNavState = {button1: {link: 'link'}};
      expect(bottomNav({navigation: {...initialState, bottomNav: bottomNavState}})).toStrictEqual(bottomNavState);
    });
  });

  describe('topNav', () => {
    it('should yield the bottomNav state', () => {
      expect(topNav({navigation: initialState})).toStrictEqual({});

      const topNavState = {button1: {link: 'link', label: 'b1'}};
      expect(topNav({navigation: {...initialState, topNav: topNavState}})).toStrictEqual(topNavState);
    });
  });

  describe('displayWidth', () => {
    it('should yield the displayWidth state', () => {
      expect(displayWidth({navigation: initialState})).toStrictEqual(0);

      const displayWidthState = {navigation: {...initialState, displaySize: {width: 1234, height: 4321}}};
      expect(displayWidth(displayWidthState)).toStrictEqual(1234);
    });
  });

  describe('displayHeight', () => {
    it('should yield the displayHeight state', () => {
      expect(displayWidth({navigation: initialState})).toStrictEqual(0);

      const displayHeightState = {navigation: {...initialState, displaySize: {width: 4321, height: 1234}}};

      expect(displayHeight(displayHeightState)).toStrictEqual(1234);
    });
  });

  describe('searchTerm', () => {
    it('should return the searchTerm from state', () => {
      const searchState = {navigation: {...initialState, search: {searchTerm: 'search'}}};

      expect(searchTerm(searchState)).toStrictEqual('search');
    });
  });

  describe('searchConfig', () => {
    it('should return the current searchConfig', () => {
      const searchState = {navigation: {...initialState, search: {searchConfig: {placeholder: 'test'}}}};

      expect(searchConfig(searchState)).toStrictEqual({placeholder: 'test'});
    });
  });
});
