import {BottomNav} from '../../_shared/bottom-nav';
import {TopNav} from '../../_shared/top-nav';
import {
  closeNav,
  openNav,
  resetBottomNav,
  resetSearch,
  resetTopNav,
  setBottomNav,
  setDisplaySize,
  setSearchConfig,
  setSearchTerm,
  setTopAndBottomNav,
  setTopNav,
  toggleNav
} from '../navigation.actions';
import {initialBottomNavState, initialState, reducer} from '../navigation.reducer';

describe('Navigation Reducer', () => {
  describe('OpenNav', () => {
    it('should open sidenav', () => {
      const action = openNav();

      const result = reducer(initialState, action);

      expect(result.sideNavOpened).toBe(true);
    });
  });

  describe('CloseNav', () => {
    it('should close sidenav', () => {
      const action = closeNav();

      const result = reducer({...initialState, sideNavOpened: true}, action);

      expect(result.sideNavOpened).toBe(false);
    });
  });

  describe('ToggleNav', () => {
    it('should open sidenav if it is closed', () => {
      const action = toggleNav();

      const result = reducer(initialState, action);

      expect(result.sideNavOpened).toBe(true);
    });

    it('should close sidenav if it is opened', () => {
      const action = toggleNav();

      const result = reducer({...initialState, sideNavOpened: true}, action);

      expect(result.sideNavOpened).toBe(false);
    });
  });

  describe('resetBottomNav', () => {
    it('should reset the bottom nav state', () => {
      const action = resetBottomNav();
      const initial = {
        ...initialState, bottomNav: {
          button1: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button2: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button3: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button4: {iconClass: 'fa fa-bars', action: {link: 'asdf'}}
        }
      };

      const result = reducer(initial, action);

      expect(result).toEqual({...initialState, bottomNav: {...initialBottomNavState}});
    });
  });

  describe('setBottomNav', () => {
    it('should set the bottom nav state', () => {
      const buttons = {
        button1: {iconClass: 'fa fa-bars'},
        button2: {iconClass: 'fa fa-bars'},
        button3: {iconClass: 'fa fa-bars'},
        button4: {iconClass: 'fa fa-bars'}
      };
      expect(reducer(initialState, setBottomNav({bottomNav: buttons}))).toStrictEqual({...initialState, bottomNav: buttons});
    });
  });

  describe('resetTopNav', () => {
    it('should reset the top nav state', () => {
      const action = resetTopNav();
      const initial = {
        ...initialState, topNav: {
          button1: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button2: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button3: {iconClass: 'fa fa-bars', action: {link: 'asdf'}},
          button4: {iconClass: 'fa fa-bars', action: {link: 'asdf'}}
        }
      };

      const result = reducer(initial, action);

      expect(result).toEqual({...initialState, topNav: {...initialBottomNavState}});
    });
  });

  describe('setTopNav', () => {
    it('should set the top nav state', () => {
      const buttons = {
        button1: {iconClass: 'fa fa-bars', label: 'b1'},
        button2: {iconClass: 'fa fa-bars', label: 'b1'},
        button3: {iconClass: 'fa fa-bars', label: 'b1'},
        button4: {iconClass: 'fa fa-bars', label: 'b1'}
      };
      expect(reducer(initialState, setTopNav({topNav: buttons}))).toStrictEqual({...initialState, topNav: buttons});
    });
  });

  describe('setTopAndBottomNav', () => {
    it('should set both top and bottom navigation', () => {
      const bottom: BottomNav = {
        button1: {iconClass: 'fa fa-bars'}
      };
      const top: TopNav = {
        button1: {label: 'label1'}
      };

      expect(reducer(initialState, setTopAndBottomNav({topNav: top, bottomNav: bottom})))
        .toStrictEqual({...initialState, topNav: top, bottomNav: bottom});
    });
  });

  describe('setDisplaySize', () => {
    it('should set the display dimensions', () => {
      const action = setDisplaySize({width: 1234, height: 4321});
      const result = reducer(initialState, action);

      expect(result).toEqual({...initialState, displaySize: {width: 1234, height: 4321}});
    });
  });

  describe('setSearchTerm', () => {
    it('should set the searchTerm', () => {
      expect(reducer(initialState, setSearchTerm({searchTerm: 'search'})))
        .toStrictEqual({...initialState, search: {...initialState.search, searchTerm: 'search'}});
    });
  });

  describe('resetSearch', () => {
    it('should reset the searchTerm and searchConfig', () => {
      expect(reducer({
        ...initialState, search: {...initialState.search, searchTerm: 'search', searchConfig: {placeholder: 'test'}}
      }, resetSearch()))
        .toStrictEqual(initialState);
    });
  });

  describe('setSearchConfig', () => {
    it('should set the searchConfig', () => {
      expect(reducer(initialState, setSearchConfig({searchConfig: {placeholder: 'test'}})))
        .toStrictEqual({...initialState, search: {...initialState.search, searchConfig: {placeholder: 'test'}}});
    });
  });
})
;
