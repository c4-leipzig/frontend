import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EffectsModule} from '@ngrx/effects';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {OAuthModule, OAuthStorage} from 'angular-oauth2-oidc';
import {environment} from '../environments/environment';
import {ErrorHandlerInterceptor} from './_shared/http-error-handling/error-handler.interceptor';
import {SharedModule} from './_shared/shared.module';
import {metaReducers, reducers} from './_store/base.reducer';
import {RouterEffects} from './_store/router.effects';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthModule} from './auth/auth.module';
import {NavigationModule} from './navigation/navigation.module';
import {UsersModule} from './users/users.module';

export const storageFactory: () => OAuthStorage = () => localStorage;

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
        allowedUrls: ['http://localhost', 'http://192.168.178.4']
      }
    }),
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production, name: 'C4'}),
    EffectsModule.forRoot([RouterEffects]),
    NavigationModule,
    AuthModule,
    ReactiveFormsModule,
    StoreRouterConnectingModule.forRoot(),
    UsersModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: OAuthStorage,
      useFactory: storageFactory
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
