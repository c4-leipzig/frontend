import {HttpClient} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, flatMap, map, take} from 'rxjs/operators';

/**
 * Abstrakte Klasse, die eine Ressource vom Backend lädt und eine entsprechende Action daraus generiert.
 * Implementierungen müssen folgende Parameter angeben:
 * @param endpoint Der Endpunkt, von dem die Ressource gealden werden soll
 * @param cacheAction Die Action, die generiert werden soll, wenn die Ressource gecacht ist
 * @param errorHandler Funktion, welche den generierten Fehler in eine entsprechende Action umwandelt
 * @param projection Funktion, welche die abgerufene Ressource in eine entsprechende Action umwandelt
 * <br/>
 * Copyright: Copyright (c) 21.09.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export abstract class CacheableToActionLoader<T> {
  public abstract readonly endpoint: string;
  public abstract readonly cachedAction: Action;

  public abstract readonly errorHandler: (RequestError) => Observable<Action>;
  public abstract readonly projection: (T) => Action;

  protected constructor(private http: HttpClient, public readonly cacheInvalidProvider: () => Observable<boolean>) {
  }

  public get(reload = false): Observable<Action> {
    return this.loadOnCacheMissOrElse(reload, this.http.get<T>(this.endpoint).pipe(map(this.projection), catchError(this.errorHandler)));
  }

  private loadOnCacheMissOrElse(reload: boolean, reload$: Observable<Action>): Observable<Action> {
    if (reload) {
      return reload$;
    }

    return this.cacheInvalidProvider().pipe(
      take(1),
      flatMap(invalid => invalid ? reload$ : of(this.cachedAction))
    );
  }
}
