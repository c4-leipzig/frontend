import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Injectable} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {createAction, props} from '@ngrx/store';
import {cold} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {CacheableToActionLoader} from './cacheable-to-action-loader';

const cacheAction = createAction(
  '[Load-list-to-action] cacheAction'
);

const errorAction = createAction(
  '[Load-list-to-action] errorAction',
  props<{ error: RequestError }>()
);

const successAction = createAction(
  '[Load-list-to-action] successAction',
  props<{ list: string[] }>()
);

class CacheInvalidProvider {
  public cacheInvalid(): Observable<boolean> {
    return of(false);
  }
}

@Injectable()
class CacheableToActionLoaderTestImpl extends CacheableToActionLoader<string[]> {
  cachedAction = cacheAction();
  endpoint = 'endpoint';


  constructor(http: HttpClient, cip: CacheInvalidProvider) {
    super(http, cip.cacheInvalid);
  }

  errorHandler = (error: RequestError) => of(errorAction({error: error}));

  projection = (sl: string[]) => successAction({list: sl});
}

describe('ListToActionLoader', () => {

  let cacheInvalidProvider: CacheInvalidProvider;
  let cacheInvalid = false;
  let httpMock: HttpTestingController;
  let cacheableToActionLoader;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CacheableToActionLoaderTestImpl,
        {
          provide: CacheInvalidProvider,
          useValue: {cacheInvalid: () => of(cacheInvalid)}
        }
      ]
    });

    httpMock = TestBed.inject(HttpTestingController);
    cacheableToActionLoader = TestBed.inject(CacheableToActionLoaderTestImpl);
    cacheInvalidProvider = TestBed.inject(CacheInvalidProvider);
  });

  afterEach(() => httpMock.verify());

  it('should call http and pass result to successAction if cache is invalid', (done) => {
    const list: any[] = [1, 2, 3];
    cacheInvalid = true;

    cacheableToActionLoader.get().subscribe(result => {
      expect(result).toStrictEqual(successAction({list: list}));
      done();
    });

    const req = httpMock.expectOne('endpoint');
    expect(req.request.method).toEqual('GET');
    req.flush(list);
  });

  it('should not call http and use cache if valid', () => {
    cacheInvalid = false;

    expect(cacheableToActionLoader.get()).toBeObservable(cold('(a|)', {a: cacheAction()}));
  });

  it('should always call http if reload flag is set', (done) => {
    const list: any[] = [1, 2, 3];
    cacheInvalid = false;

    cacheableToActionLoader.get(true).subscribe(result => {
      expect(result).toStrictEqual(successAction({list: list}));
      done();
    });

    const req = httpMock.expectOne('endpoint');
    expect(req.request.method).toEqual('GET');
    req.flush(list);
  });
});
