import {HttpClient} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {catchError, flatMap, map, take} from 'rxjs/operators';
import {RequestError} from '../../_shared/http-error-handling/request-error';


/**
 * Abstrakte Klasse, die eine Ressource für eine spezifische Id vom Backend lädt und eine entsprechende Action daraus generiert.
 * Implementierungen müssen folgende Parameter angeben:
 * @param endpointProvider Funktion, die den Endpunkt zurückliefert, von dem die Ressource gealden werden soll
 * @param cachedActionProvider Funktion, welche Die Action zurückliefert, die generiert werden soll, wenn die Ressource gecacht ist
 * @param cacheInvalidProvider Funktion, welche ein Observable zurückliefert, das für eine bestimmte Id die Cache-Validität beinhaltet
 * @param errorHandler Funktion, welche den generierten Fehler in eine entsprechende Action umwandelt
 * @param projection Funktion, welche die abgerufene Ressource in eine entsprechende Action umwandelt
 * <br/>
 * Copyright: Copyright (c) 25.09.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export abstract class CacheableByIdToActionLoader<T> {
  public abstract readonly endpointProvider: (id: string) => string;
  public abstract readonly cachedActionProvider: (id: string) => Action;
  public abstract readonly cacheInvalidProvider: (id: string) => Observable<boolean>;

  public abstract readonly errorHandler: (id: string, error: RequestError) => Observable<Action>;
  public abstract readonly projection: (id: string, result: T) => Action;

  protected constructor(private http: HttpClient) {
  }

  public get(id: string, reload = false): Observable<Action> {
    return this.loadOnCacheMissOrElse(id, reload,
      this.http.get<T>(this.endpointProvider(id)).pipe(
        map((result: T) => this.projection(id, result)),
        catchError((error: RequestError) => this.errorHandler(id, error))));
  }

  private loadOnCacheMissOrElse(id: string, reload: boolean, reload$: Observable<Action>): Observable<Action> {
    if (reload) {
      return reload$;
    }

    return this.cacheInvalidProvider(id).pipe(
      take(1),
      flatMap(invalid => invalid ? reload$ : of(this.cachedActionProvider(id)))
    );
  }
}
