import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Injectable} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {createAction, props} from '@ngrx/store';
import {cold} from 'jest-marbles';
import {of} from 'rxjs';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {parseUrl} from '../../_shared/helper/parse-url';
import {CacheableByIdToActionLoader} from './cacheable-by-id-to-action-loader';

const cacheAction = createAction(
  '[Load-list-to-action] cacheAction',
  props<{ id: string }>()
);

const errorAction = createAction(
  '[Load-list-to-action] errorAction',
  props<{ id: string, error: RequestError }>()
);

const successAction = createAction(
  '[Load-list-to-action] successAction',
  props<{ id: string, list: string[] }>()
);

@Injectable()
class CacheableByIdToActionLoaderTestImpl extends CacheableByIdToActionLoader<string[]> {
  constructor(http: HttpClient) {
    super(http);
  }

  readonly cacheInvalidProvider = () => of(false);

  readonly cachedActionProvider = (id: string) => cacheAction({id: id});

  readonly endpointProvider = (id: string): string => parseUrl('endpoint/{id}', id);

  readonly errorHandler = (id: string, error: RequestError) => of(errorAction({id: id, error: error}));

  readonly projection = (id: string, result: string[]) => successAction({id: id, list: result});
}

describe('ListToActionLoader', () => {

  let cacheInvalid = false;
  let httpMock: HttpTestingController;
  let cacheableByIdToActionLoader;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CacheableByIdToActionLoaderTestImpl
      ]
    });

    httpMock = TestBed.inject(HttpTestingController);
    cacheableByIdToActionLoader = TestBed.inject(CacheableByIdToActionLoaderTestImpl);
  });

  afterEach(() => httpMock.verify());

  it('should call http and pass result to successAction if cache is invalid', (done) => {
    const list: any[] = [1, 2, 3];

    spyOn(cacheableByIdToActionLoader, 'cacheInvalidProvider').and.returnValue(of(true));

    cacheableByIdToActionLoader.get('departmentId').subscribe(result => {
      expect(result).toStrictEqual(successAction({id: 'departmentId', list: list}));
      done();
    });

    const req = httpMock.expectOne('endpoint/departmentId');
    expect(req.request.method).toEqual('GET');
    req.flush(list);
  });

  it('should not call http and use cache if valid', () => {
    spyOn(cacheableByIdToActionLoader, 'cacheInvalidProvider').and.returnValue(of(false));

    expect(cacheableByIdToActionLoader.get('departmentId')).toBeObservable(cold('(a|)', {a: cacheAction({id: 'departmentId'})}));
  });

  it('should always call http if reload flag is set', (done) => {
    const list: any[] = [1, 2, 3];
    cacheInvalid = false;

    cacheableByIdToActionLoader.get('departmentId', true).subscribe(result => {
      expect(result).toStrictEqual(successAction({id: 'departmentId', list: list}));
      done();
    });

    const req = httpMock.expectOne('endpoint/departmentId');
    expect(req.request.method).toEqual('GET');
    req.flush(list);
  });
});
