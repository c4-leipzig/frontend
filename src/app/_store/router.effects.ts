import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {routerNavigatedAction} from '@ngrx/router-store';
import {Store} from '@ngrx/store';
import {EMPTY, of} from 'rxjs';
import {map, switchMap, withLatestFrom} from 'rxjs/operators';
import {closeNav, resetBottomNav, resetSearch, resetTopNav} from '../navigation/_store/navigation.actions';
import {sideNavOpened} from '../navigation/_store/navigation.selectors';
import {State} from './base.reducer';

@Injectable()
export class RouterEffects {
  private navOpened$ = this.store.select(sideNavOpened);

  constructor(private actions$: Actions,
              private store: Store<State>) {
  }

  closeSideNavOnNavigated$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      withLatestFrom(this.navOpened$),
      switchMap(([, sideNavOpen]) => sideNavOpen ? of(closeNav()) : EMPTY)
    );
  }, {dispatch: true});

  resetBottomNavOnNavigated$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      map(() => resetBottomNav())
    );
  }, {dispatch: true});

  resetTopNavOnNavigated$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      map(() => resetTopNav())
    );
  }, {dispatch: true});

  resetSearchTermOnNavigated$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      map(() => resetSearch())
    );
  }, {dispatch: true});
}
