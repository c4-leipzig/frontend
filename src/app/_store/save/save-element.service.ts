import {Action} from '@ngrx/store';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Id} from '../../_domain/id';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {SaveType} from '../../_domain/state/save-state';

/**
 * Abstrakte Baseklasse für Services, die ein Element speichern sollen (und dafür auf den Store zugreifen).
 * Es werden die Aktionen 'create' und 'update' gesondert behandelt.
 * Implementierende Klassen müssen angeben:
 * @param createProjection Funktion, welche aus der Id des Elements eine ensprechende Action generiert,
 * wenn das Element erfolgreich persistiert wurde
 * @param updateProjection Analog createProjection
 * @param createErrorHandler Funktion, welche auftretende Fehler in eine ensprechende Action umwandelt
 * @param updateErrorHandler Analog createErrorHandler
 * @param createRaw Funktion, welche das konkrete persistieren (i.d.R. per HttpClient) vornimmt.
 * Muss die Id des Elements in einem Observable zurückliefern
 * @param updateRaw Analog createRaw
 * @param navigateAfterSave Funktion, die nach einer erfolgreichen Persistierung ausgeführt wird,
 * um abhängig von der Id des Elements zu navigieren
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export abstract class SaveElementService<T> {
  protected abstract readonly createProjection: (id: Id) => Action;
  protected abstract readonly updateProjection: (id: Id) => Action;
  protected abstract readonly createErrorHandler: (error: RequestError) => Observable<Action>;
  protected abstract readonly updateErrorHandler: (error: RequestError) => Observable<Action>;

  save(element: T, saveType: SaveType): Observable<Action> {
    switch (saveType) {
      case SaveType.create:
        return this.create(element);
      case SaveType.update:
        return this.update(element);
      default:
        return throwError({error: 'NOT IMPLEMENTED'});
    }
  }

  private create(element: T): Observable<Action> {
    return this.createRaw(element).pipe(
      map(this.createProjection),
      catchError(this.createErrorHandler)
    );
  }

  protected abstract createRaw(element: T): Observable<Id>;

  private update(element: T): Observable<Action> {
    return this.updateRaw(element).pipe(
      map(this.updateProjection),
      catchError(this.updateErrorHandler)
    );
  }

  protected abstract updateRaw(element: T): Observable<Id>;

  protected abstract navigateAfterSave(id: string): void;

}
