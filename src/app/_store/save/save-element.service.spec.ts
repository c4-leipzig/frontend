import {Action, createAction, props} from '@ngrx/store';
import {cold} from 'jest-marbles';
import {Observable, of, throwError} from 'rxjs';
import {Id} from '../../_domain/id';
import {RequestErrorType} from '../../_shared/http-error-handling/http-error-type.enum';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {SaveType} from '../../_domain/state/save-state';
import {SaveElementService} from './save-element.service';

class SaveElementServiceImpl extends SaveElementService<string> {

  constructor(createErrorHandler: (error: RequestError) => Observable<Action>,
              createProjection: (id: Id) => Action,
              createRaw: (element: string) => Observable<Id>,
              navigateAfterSave: (id: string) => void,
              updateErrorHandler: (error: RequestError) => Observable<Action>,
              updateProjection: (id: Id) => Action,
              updateRaw: (element: string) => Observable<Id>) {
    super();
    this.createErrorHandler = createErrorHandler;
    this.createProjection = createProjection;
    this.createRaw = createRaw;
    this.navigateAfterSave = navigateAfterSave;
    this.updateErrorHandler = updateErrorHandler;
    this.updateProjection = updateProjection;
    this.updateRaw = updateRaw;
  }

  protected readonly createErrorHandler = (error: RequestError): Observable<Action> => undefined;

  protected readonly createProjection = (id: Id): Action => undefined;

  protected createRaw = (element: string): Observable<Id> => undefined;

  protected navigateAfterSave = (id: string): void => {
  };

  protected readonly updateErrorHandler = (error: RequestError): Observable<Action> => undefined;

  protected readonly updateProjection = (id: Id): Action => undefined;

  protected updateRaw = (element: string): Observable<Id> => undefined;
}

export const elementSaved = createAction(
  '[Save-element] elementSaved',
  props<Id>()
);

export const elementSaveError = createAction(
  '[Save-element] elementSaveError',
  props<{ error: RequestError }>()
);


describe('SaveElementService', () => {
  it('should call createRaw and createProjection on success', () => {
    const spy = {createRaw: jest.fn(), createProjection: jest.fn()};
    spyOn(spy, 'createRaw').and.returnValue(of({id: 'anyId'}));
    spyOn(spy, 'createProjection').and.returnValue(elementSaved({id: 'anyId'}));
    const saveElement =
      new SaveElementServiceImpl(jest.fn(), spy.createProjection, spy.createRaw, jest.fn(), jest.fn(), jest.fn(), jest.fn());
    const result$ = saveElement.save('string', SaveType.create);
    expect(result$).toBeObservable(cold('(a|)', {a: elementSaved({id: 'anyId'})}));
    expect(spy.createRaw).toHaveBeenCalledWith('string');
  });

  it('should call createRaw and createErrorHandler on error', () => {
    const spy = {createRaw: jest.fn(), createErrorHandler: jest.fn()};
    spyOn(spy, 'createRaw').and.returnValue(throwError({type: RequestErrorType.server}));
    spyOn(spy, 'createErrorHandler').and.returnValue(of(elementSaveError({error: {type: RequestErrorType.server}})));
    const saveElement =
      new SaveElementServiceImpl(spy.createErrorHandler, jest.fn(), spy.createRaw, jest.fn(), jest.fn(), jest.fn(), jest.fn());
    const result$ = saveElement.save('string', SaveType.create);
    expect(result$).toBeObservable(cold('(a|)', {a: elementSaveError({error: {type: RequestErrorType.server}})}));
    expect(spy.createRaw).toHaveBeenCalledWith('string');
  });

  it('should call updateRaw and updateProjection on success', () => {
    const spy = {updateRaw: jest.fn(), updateProjection: jest.fn()};
    spyOn(spy, 'updateRaw').and.returnValue(of({id: 'anyId'}));
    spyOn(spy, 'updateProjection').and.returnValue(elementSaved({id: 'anyId'}));
    const saveElement =
      new SaveElementServiceImpl(jest.fn(), jest.fn(), jest.fn(), jest.fn(), jest.fn(), spy.updateProjection, spy.updateRaw);
    const result$ = saveElement.save('string', SaveType.update);
    expect(result$).toBeObservable(cold('(a|)', {a: elementSaved({id: 'anyId'})}));
    expect(spy.updateRaw).toHaveBeenCalledWith('string');
  });

  it('should call updateRaw and updateErrorHandler on error', () => {
    const spy = {updateRaw: jest.fn(), updateErrorHandler: jest.fn()};
    spyOn(spy, 'updateRaw').and.returnValue(throwError({type: RequestErrorType.server}));
    spyOn(spy, 'updateErrorHandler').and.returnValue(of(elementSaveError({error: {type: RequestErrorType.server}})));
    const saveElement =
      new SaveElementServiceImpl(jest.fn(), jest.fn(), jest.fn(), jest.fn(), spy.updateErrorHandler, jest.fn(), spy.updateRaw);
    const result$ = saveElement.save('string', SaveType.update);
    expect(result$).toBeObservable(cold('(a|)', {a: elementSaveError({error: {type: RequestErrorType.server}})}));
    expect(spy.updateRaw).toHaveBeenCalledWith('string');
  });
});
