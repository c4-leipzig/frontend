import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {routerNavigatedAction} from '@ngrx/router-store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {closeNav, resetBottomNav, resetSearch, resetTopNav} from '../../navigation/_store/navigation.actions';
import {sideNavOpened} from '../../navigation/_store/navigation.selectors';
import {RouterEffects} from '../router.effects';

describe('RouterEffects', () => {
  let actions$: Observable<any>;
  let effects: RouterEffects;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RouterEffects,
        provideMockActions(() => actions$),
        provideMockStore({})
      ]
    });

    effects = TestBed.inject(RouterEffects);
    mockStore = TestBed.inject(MockStore);
  });

  describe('RouterNavigatedAction', () => {
    it('should emit closeNav action if sidebar is open', () => {
      mockStore.overrideSelector(sideNavOpened, true);
      actions$ = of(routerNavigatedAction({payload: null}));
      expect(effects.closeSideNavOnNavigated$).toBeObservable(cold('(a|)', {a: closeNav()}));
    });

    it('should not emit closeNav if sidebar is not open', () => {
      mockStore.overrideSelector(sideNavOpened, false);
      actions$ = of(routerNavigatedAction({payload: null}));
      expect(effects.closeSideNavOnNavigated$).toBeObservable(cold('|'));
    });

    it('should emit resetBottomNav', () => {
      actions$ = of(routerNavigatedAction({payload: null}));
      expect(effects.resetBottomNavOnNavigated$).toBeObservable(cold('(a|)', {a: resetBottomNav()}));
    });

    it('should emit resetBottomNav', () => {
      actions$ = of(routerNavigatedAction({payload: null}));
      expect(effects.resetTopNavOnNavigated$).toBeObservable(cold('(a|)', {a: resetTopNav()}));
    });

    it('should emit resetSearchTerm', () => {
      actions$ = of(routerNavigatedAction({payload: null}));
      expect(effects.resetSearchTermOnNavigated$).toBeObservable(cold('(a|)', {a: resetSearch()}));
    });
  });
});
