import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {environment} from '../../../../environments/environment';
import {RequestErrorType} from '../../../_shared/http-error-handling/http-error-type.enum';
import {departmentListCached, departmentListLoaded, departmentListLoadError} from '../../_store/department.actions';
import {departmentListCacheInvalid} from '../../_store/department.selectors';

import {LoadDepartmentListService} from '../load-department-list.service';

describe('LoadDepartmentListService', () => {
  let service: LoadDepartmentListService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore(),
        LoadDepartmentListService
      ]
    });
    service = TestBed.inject(LoadDepartmentListService);
    mockStore = TestBed.inject(MockStore);
  });

  it('should use the correct endpoint', () => {
    expect(service.endpoint).toStrictEqual(environment.endpoints.departments.allDepartments);
  });

  it('should select its cacheValidity correctly', () => {
    mockStore.overrideSelector(departmentListCacheInvalid, 'test' as any);

    expect(service.cacheInvalidProvider()).toBeObservable(cold('(a)', {a: 'test'}));
  });

  it('should use the correct cachedAction', () => {
    expect(service.cachedAction).toStrictEqual(departmentListCached());
  });

  it('should map errors to the correct action', () => {
    const error = {type: RequestErrorType.user};

    expect(service.errorHandler(error)).toBeObservable(cold('(a|)', {a: departmentListLoadError({error: error})}));
  });

  it('should map the loaded result to the correct action', () => {
    const res: any[] = [1, 2, 3];
    expect(service.projection(res)).toStrictEqual(departmentListLoaded({departmentList: res}));
  });
});
