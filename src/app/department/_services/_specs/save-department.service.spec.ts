import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {parseUrl} from '../../../_shared/helper/parse-url';
import {ErrorHandlerInterceptor} from '../../../_shared/http-error-handling/error-handler.interceptor';
import {RequestErrorType} from '../../../_shared/http-error-handling/http-error-type.enum';
import {SaveType} from '../../../_domain/state/save-state';
import {departmentSaved, departmentSaveError} from '../../_store/department.actions';

import {SaveDepartmentService} from '../save-department.service';

describe('SaveDepartmentService', () => {
  let service: SaveDepartmentService;
  let httpTestingController: HttpTestingController;
  const routerMock = {
    navigate: jest.fn()
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorHandlerInterceptor,
          multi: true
        },
        {
          provide: Router,
          useValue: routerMock
        }
      ]
    });

    service = TestBed.inject(SaveDepartmentService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('create', () => {
    it('should call correct endpoint and map to correct action', (done) => {
      service.save({dep: 'artment'} as any, SaveType.create).subscribe(action => {
        expect(action).toStrictEqual(departmentSaved({id: 'resultId'}));
        done();
      });

      const testRequest = httpTestingController.expectOne(environment.endpoints.departments.create);
      expect(testRequest.request.method).toBe('POST');
      testRequest.flush({id: 'resultId'});
    });

    it('should call correct endpoint and map error to correct action', (done) => {
      service.save({dep: 'artment'} as any, SaveType.create).subscribe(action => {
        expect(action).toStrictEqual(departmentSaveError({error: {type: RequestErrorType.server}}));
        done();
      });

      const testRequest = httpTestingController.expectOne(environment.endpoints.departments.create);
      expect(testRequest.request.method).toBe('POST');
      testRequest.error({type: RequestErrorType.server} as any);
    });
  });

  describe('update', () => {
    it('should call correct endpoint and map to correct action', (done) => {
      service.save({dep: 'artment', id: 'depId'} as any, SaveType.update).subscribe(action => {
        expect(action).toStrictEqual(departmentSaved({id: 'depId'}));
        done();
      });

      const testRequest = httpTestingController.expectOne(parseUrl(environment.endpoints.departments.update, 'depId'));
      expect(testRequest.request.method).toBe('PUT');
      testRequest.flush({id: 'depId'});
    });

    it('should call correct endpoint and map error to correct action', (done) => {
      service.save({dep: 'artment', id: 'depId'} as any, SaveType.update).subscribe(action => {
        expect(action).toStrictEqual(departmentSaveError({error: {type: RequestErrorType.server}}));
        done();
      });

      const testRequest = httpTestingController.expectOne(parseUrl(environment.endpoints.departments.update, 'depId'));
      expect(testRequest.request.method).toBe('PUT');
      testRequest.error({type: RequestErrorType.server} as any);
    });
  });

  describe('navigateAfterSave', () => {
    it('should navigate based on id', () => {
      service.navigateAfterSave('anyId');
      expect(routerMock.navigate).toHaveBeenCalledWith(['/departments', 'anyId'], {state: {canDeactivate: true}});
    });
  });
});
