import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {State} from '../../_store/base.reducer';
import {CacheableToActionLoader} from '../../_store/cacheable/cacheable-to-action-loader';
import {departmentListCached, departmentListLoaded, departmentListLoadError} from '../_store/department.actions';
import {departmentListCacheInvalid} from '../_store/department.selectors';
import {DepartmentWithActiveAssignment} from '../_shared/domain/dtos/department-with-active-assignment';

@Injectable()
export class LoadDepartmentListService extends CacheableToActionLoader<DepartmentWithActiveAssignment[]> {

  constructor(http: HttpClient, store: Store<State>) {
    super(http, () => store.select(departmentListCacheInvalid));
  }

  endpoint = environment.endpoints.departments.allDepartments;
  cachedAction = departmentListCached();

  errorHandler = (error: RequestError) => of(departmentListLoadError({error: error}));

  projection = (departmentList: DepartmentWithActiveAssignment[]): Action => departmentListLoaded({departmentList: departmentList});

}
