import {Injectable} from '@angular/core';
import {ValidatorFn} from '@angular/forms';
import {Store} from '@ngrx/store';
import {validateValueInList} from '../../_shared/validators/validate-value-in-list';
import {validateValueNotInList} from '../../_shared/validators/validate-value-not-in-list';
import {State} from '../../_store/base.reducer';
import {departmentNameList} from '../_store/department.selectors';
import {categoryIdList} from '../category/_store/category.selectors';

@Injectable({
  providedIn: 'root'
})
export class DepartmentValidationService {

  constructor(private store: Store<State>) {
  }

  get categoryIdValidator(): ValidatorFn {
    return validateValueInList(this.store.select(categoryIdList));
  }

  departmentNameValidator(selfName?: string): ValidatorFn {
    return validateValueNotInList(this.store.select(departmentNameList), selfName);
  }
}
