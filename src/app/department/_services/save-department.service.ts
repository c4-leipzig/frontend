import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {Id} from '../../_domain/id';
import {parseUrl} from '../../_shared/helper/parse-url';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {SaveElementService} from '../../_store/save/save-element.service';
import {Department} from '../_shared/domain/department';
import {departmentSaved, departmentSaveError} from '../_store/department.actions';

/**
 * Service zum Speichern von Änderungen an einem {@link Department}.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Injectable({
  providedIn: 'root'
})
export class SaveDepartmentService extends SaveElementService<Department> {

  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  protected readonly createErrorHandler = (error: RequestError) => of(departmentSaveError({error: error}));
  protected readonly updateErrorHandler = (error: RequestError) => of(departmentSaveError({error: error}));

  protected readonly createProjection = (id: Id) => departmentSaved(id);
  protected readonly updateProjection = (id: Id) => departmentSaved(id);

  protected createRaw = (element: Department) => this.http.post<Id>(environment.endpoints.departments.create, element);

  protected updateRaw = (element: Department) =>
    this.http.put<void>(parseUrl(environment.endpoints.departments.update, element.id), element)
      .pipe(map(() => {
        return {id: element.id};
      }));

  navigateAfterSave = (id: string) => this.router.navigate(['/departments', id], {state: {canDeactivate: true}});
}



