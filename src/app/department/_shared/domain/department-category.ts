export interface DepartmentCategory {
  id: string;
  name: string;
}

export function emptyDepartmentCategory(): DepartmentCategory {
  return {id: '', name: ''};
}

