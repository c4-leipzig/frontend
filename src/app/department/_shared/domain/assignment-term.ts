import dayjs from 'dayjs';

export interface AssignmentTerm {
  start: dayjs.Dayjs;
  end: dayjs.Dayjs;
  description: string;
}
