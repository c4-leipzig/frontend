import {Role} from '../../../auth/domain/role.enum';

export interface Department {
  id: string;
  name: string;
  description: string;
  departmentCategoryId: string;
  possibleUserTypes: Role[];
}

export function emptyDepartment(): Department {
  return {
    id: null,
    name: null,
    description: null,
    departmentCategoryId: null,
    possibleUserTypes: [Role.MEMBER]
  };
}
