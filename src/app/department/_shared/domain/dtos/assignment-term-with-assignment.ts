import {AssignmentTerm} from '../assignment-term';

export interface AssignmentTermWithAssignment {
  userId: string;
  departmentId: string;
  departmentAssignmentId: string;
  assignmentTerm: AssignmentTerm;
}

export interface AssignmentTermWithAssignmentAndUsername extends AssignmentTermWithAssignment {
  username: string;
}
