import {Department} from '../department';
import {DepartmentCategory} from '../department-category';
import {AssignmentTermWithAssignment} from './assignment-term-with-assignment';

export interface DepartmentWithActiveAssignment {
  department: Department;
  assignmentTerm: AssignmentTermWithAssignment;
}

export interface DepartmentWithCategory extends DepartmentWithActiveAssignment {
  category: DepartmentCategory;
}

export interface DepartmentWithCategoryAndUsername extends DepartmentWithCategory {
  username: string;
}
