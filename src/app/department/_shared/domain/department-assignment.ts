import {AssignmentTerm} from './assignment-term';

export interface DepartmentAssignment {
  id: string;
  departmentId: string;
  userId: string;
  terms: AssignmentTerm[];
}
