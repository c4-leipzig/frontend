import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AssignmentTermEffects} from './_store/assignment-term.effects';
import * as fromAssignmentTerm from './_store/assignment-term.reducer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAssignmentTerm.assignmentTermFeatureKey, fromAssignmentTerm.reducer),
    EffectsModule.forFeature([AssignmentTermEffects])
  ]
})
export class AssignmentTermModule {
}
