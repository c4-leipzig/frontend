import {environment} from '../../../../../environments/environment';
import {currentTime} from '../../../../_shared/helper/time-helper';
import {initialListStore} from '../../../../_domain/state/list-state';
import {initialState} from '../assignment-term.reducer';
import {
  assignmentTermsCacheInvalidForDepartment,
  assignmentTermsForDepartmentLoading,
  assignmentTermsForDepartmentWithUsername
} from '../assignment-term.selectors';

describe('AssignmentTerm Selectors', () => {
  describe('cacheInvalidForDepartment', () => {
    it('should yield true if the assignmentTerm state for the department does not exist', () => {
      const state = {assignmentTerm: initialState};

      expect(state.assignmentTerm.assignmentTerms[`departmentId`]).toBeUndefined();
      expect(assignmentTermsCacheInvalidForDepartment('departmentId')(state)).toBe(true);
    });

    it('should yield true if cache is too old', () => {
      const state = {
        assignmentTerm: {
          assignmentTerms: {
            departmentId: {
              ...initialListStore,
              cacheTime: currentTime() - (environment.cacheTime + 100)
            }
          }
        }
      };

      expect(assignmentTermsCacheInvalidForDepartment('departmentId')(state)).toBe(true);
    });

    it('should yield false if cache is still valid', () => {
      const state = {
        assignmentTerm: {
          assignmentTerms: {
            departmentId: {
              ...initialListStore,
              cacheTime: currentTime() - (environment.cacheTime - 100)
            }
          }
        }
      };

      expect(assignmentTermsCacheInvalidForDepartment('departmentId')(state)).toBe(false);
    });
  });

  describe('assignmentTermsForDepartmentLoading', () => {
    it('should yield isLoading flag for given department', () => {
      let state = {assignmentTerm: {assignmentTerms: {departmentId: {isLoading: true}}}};
      expect(assignmentTermsForDepartmentLoading('departmentId')(state)).toBe(true);

      state = {assignmentTerm: {assignmentTerms: {departmentId: {isLoading: false}}}};
      expect(assignmentTermsForDepartmentLoading('departmentId')(state)).toBe(false);
    });

    it('should yield false if assignmentTerm for department is not present in state', () => {
      const state = {assignmentTerm: {assignmentTerms: {}}};
      expect(assignmentTermsForDepartmentLoading('departmentId')(state)).toBeFalsy();
    });
  });

  describe('assignmentTermsForDepartmentWithUsername', () => {
    it('should correctly map usernames to assignmentTerms', () => {
      const state = {
        assignmentTerm: {
          assignmentTerms: {departmentId: {list: [{userId: 'u1', departmentId: 'test'}, {userId: 'u2', departmentId: 'test'}]}}
        },
        users: {memberList: {list: [{id: 'u1', username: 'user1'}, {id: 'u2', username: 'user2'}]}}
      };

      expect(assignmentTermsForDepartmentWithUsername('departmentId')(state)).toStrictEqual([
        {userId: 'u1', username: 'user1', departmentId: 'test'}, {userId: 'u2', username: 'user2', departmentId: 'test'}]);
    });

    it('should not throw an error if assignmentTerms for department are not present', () => {
      const state = {assignmentTerm: {assignmentTerms: {}}};

      expect(assignmentTermsForDepartmentWithUsername('departmentId')(state)).toBeUndefined();
    });

    it('should not throw an error if no list of assignmentTerms for department is present', () => {
      const state = {assignmentTerm: {assignmentTerms: {departmentId: initialListStore}}};

      expect(assignmentTermsForDepartmentWithUsername('departmentId')(state)).toBeUndefined();
    });

    it('should not throw if user is not found', () => {
      const state = {
        assignmentTerm: {
          assignmentTerms: {departmentId: {list: [{userId: 'u1', departmentId: 'test'}, {userId: 'u2', departmentId: 'test'}]}}
        },
        users: {memberList: {list: [{id: 'u1', username: 'user1'}]}}
      };

      expect(assignmentTermsForDepartmentWithUsername('departmentId')(state)).toStrictEqual([
        {userId: 'u1', username: 'user1', departmentId: 'test'}, {userId: 'u2', username: undefined, departmentId: 'test'}]);
    });
  });
});
