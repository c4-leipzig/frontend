import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {cold} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {loadMemberList} from '../../../../users/_store/users.actions';
import {LoadAssignmentTermsForDepartmentService} from '../../_shared/load-assignment-terms-for-department.service';
import {assignmentTermsLoaded, loadAssignmentTerms} from '../assignment-term.actions';

import {AssignmentTermEffects} from '../assignment-term.effects';

describe('AssignmentTermEffects', () => {
  let actions$: Observable<any>;
  let effects: AssignmentTermEffects;
  let mockLoadService;
  const list: any[] = [1, 2, 3];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AssignmentTermEffects,
        provideMockActions(() => actions$),
        {
          provide: LoadAssignmentTermsForDepartmentService,
          useValue: {get: jest.fn()}
        }
      ]
    });

    effects = TestBed.inject(AssignmentTermEffects);
    mockLoadService = TestBed.inject(LoadAssignmentTermsForDepartmentService);
  });

  describe('loadAssignmentTermsForDepartment', () => {
    it('should load assignmentTerms and trigger loading of members', () => {
      const resultAction = assignmentTermsLoaded({departmentId: 'departmentId', assignmentTerms: list});
      spyOn(mockLoadService, 'get').and.returnValue(of(resultAction));

      actions$ = of(loadAssignmentTerms({departmentId: 'departmentId'}));

      expect(effects.loadAssignmentTermsForDepartment$).toBeObservable(cold('(ab|)', {a: resultAction, b: loadMemberList()}));
    });
  });
});
