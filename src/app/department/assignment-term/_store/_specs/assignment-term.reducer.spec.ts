import {initialListStore} from '../../../../_domain/state/list-state';
import {assignmentTermsCached, assignmentTermsLoaded, assignmentTermsLoadError, loadAssignmentTerms} from '../assignment-term.actions';
import {reducer} from '../assignment-term.reducer';

describe('AssignmentTerm Reducer', () => {
  describe('loadAssignmentTerms', () => {
    it('should set the loading flag', () => {
      const initial = {assignmentTerms: {departmentId: initialListStore}};

      const result = reducer(initial, loadAssignmentTerms({departmentId: 'departmentId'}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: {...initial.assignmentTerms.departmentId, isLoading: true}}
      });
    });

    it('should create an initialListStore and set the loading flag if the department is not yet present in the state', () => {
      const initial = {assignmentTerms: {departmentId: undefined}};

      const result = reducer(initial, loadAssignmentTerms({departmentId: 'departmentId'}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: {...initialListStore, isLoading: true}}
      });
    });

    it('should not alter another department\'s state', () => {
      const initial = {assignmentTerms: {departmentId: initialListStore, departmentId2: initialListStore}};

      const result = reducer(initial, loadAssignmentTerms({departmentId: 'departmentId'}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: {...initial.assignmentTerms.departmentId, isLoading: true}}
      });
    });
  });

  describe('assignmentTermsLoaded', () => {
    it('should reset loading flag and error, set the cacheTime and the list', () => {
      const initial = {assignmentTerms: {departmentId: {...initialListStore, isLoading: true, loadError: true as any}}};
      const list: any[] = [1, 2, 3];

      const result = reducer(initial, assignmentTermsLoaded({departmentId: 'departmentId', assignmentTerms: list}));

      expect(result.assignmentTerms.departmentId.isLoading).toBe(false);
      expect(result.assignmentTerms.departmentId.cacheTime).toBeGreaterThan(0);
      expect(result.assignmentTerms.departmentId.list).toStrictEqual(list);
      expect(result.assignmentTerms.departmentId.loadError).toBeUndefined();
    });

    it('should not alter another department\'s state', () => {
      const initial = {
        assignmentTerms: {
          departmentId: {...initialListStore, isLoading: true, loadError: true as any},
          departmentId2: {...initialListStore, isLoading: true}
        }
      };
      const list: any[] = [1, 2, 3];

      const result = reducer(initial, assignmentTermsLoaded({departmentId: 'departmentId', assignmentTerms: list}));

      expect(result.assignmentTerms.departmentId2).toEqual(initial.assignmentTerms.departmentId2);
    });
  });

  describe('assignmentTermsCached', () => {
    it('should reset loading flag', () => {
      const initial = {assignmentTerms: {departmentId: {...initialListStore, isLoading: true}}};

      const result = reducer(initial, assignmentTermsCached({departmentId: 'departmentId'}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: initialListStore}
      });
    });

    it('should not alter another department\'s state', () => {
      const initial = {
        assignmentTerms: {
          departmentId: {...initialListStore, isLoading: true},
          departmentId2: {...initialListStore, isLoading: true}
        }
      };

      const result = reducer(initial, assignmentTermsCached({departmentId: 'departmentId'}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: initialListStore}
      });
    });
  });

  describe('assignmentTermsLoadError', () => {
    it('should reset loading flag and save error', () => {
      const initial = {assignmentTerms: {departmentId: {...initialListStore, isLoading: true}}};

      const result = reducer(initial, assignmentTermsLoadError({departmentId: 'departmentId', error: 'error' as any}));

      expect(result).toStrictEqual({
        ...initial,
        assignmentTerms: {...initial.assignmentTerms, departmentId: {...initialListStore, loadError: 'error'}}
      });
    });

    it('should not alter another department\'s state', () => {
      const initial = {
        assignmentTerms: {
          departmentId: {...initialListStore, isLoading: true},
          departmentId2: {...initialListStore, isLoading: true}
        }
      };

      const result = reducer(initial, assignmentTermsLoadError({departmentId: 'departmentId', error: 'error' as any}));

      expect(result.assignmentTerms.departmentId2).toBe(initial.assignmentTerms.departmentId2);
    });
  });
});
