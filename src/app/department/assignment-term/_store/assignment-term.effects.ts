import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {merge, of} from 'rxjs';

import {flatMap} from 'rxjs/operators';
import {loadMemberList} from '../../../users/_store/users.actions';
import {LoadAssignmentTermsForDepartmentService} from '../_shared/load-assignment-terms-for-department.service';

import * as AssignmentTermActions from './assignment-term.actions';

@Injectable()
export class AssignmentTermEffects {

  loadAssignmentTermsForDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AssignmentTermActions.loadAssignmentTerms),
      flatMap(loadAction =>
        merge(
          this.loadAssignmentTermsForDepartmentService.get(loadAction.departmentId, loadAction.reload),
          of(loadMemberList(loadAction.reload))))
    );
  }, {dispatch: true});

  constructor(private actions$: Actions,
              private loadAssignmentTermsForDepartmentService: LoadAssignmentTermsForDepartmentService) {
  }

}
