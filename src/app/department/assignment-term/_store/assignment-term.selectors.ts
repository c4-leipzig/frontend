import {createFeatureSelector, createSelector} from '@ngrx/store';
import {isCacheInvalid} from '../../../_shared/helper/time-helper';
import {memberList} from '../../../users/_store/users.selectors';
import * as fromAssignmentTerm from './assignment-term.reducer';

export const selectAssignmentTermState = createFeatureSelector<fromAssignmentTerm.State>(
  fromAssignmentTerm.assignmentTermFeatureKey
);

export const assignmentTermsCacheInvalidForDepartment =
  (departmentId: string) => createSelector(selectAssignmentTermState, state =>
    !state.assignmentTerms[departmentId] || isCacheInvalid(state.assignmentTerms[departmentId]));

export const assignmentTermsForDepartmentLoading =
  (departmentId: string) => createSelector(selectAssignmentTermState, state => state.assignmentTerms[departmentId]?.isLoading);

export const assignmentTermsForDepartmentWithUsername = (departmentId: string) =>
  createSelector(selectAssignmentTermState, memberList, (assignmentState, members) =>
    assignmentState.assignmentTerms[departmentId]?.list?.map(assignmentTerm => {
      return {...assignmentTerm, username: members.find(member => member.id === assignmentTerm.userId)?.username};
    })
  );
