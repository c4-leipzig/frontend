import {createAction, props} from '@ngrx/store';
import {RequestError} from '../../../_shared/http-error-handling/request-error';
import {AssignmentTermWithAssignment} from '../../_shared/domain/dtos/assignment-term-with-assignment';

export const loadAssignmentTerms = createAction(
  '[AssignmentTerm] Load AssignmentTerms',
  props<{ departmentId: string, reload?: boolean }>()
);

export const assignmentTermsLoaded = createAction(
  '[Assignment-term] assignmentTermsLoaded',
  props<{ departmentId: string, assignmentTerms: AssignmentTermWithAssignment[] }>()
);

export const assignmentTermsCached = createAction(
  '[Assignment-term] assignmentTermsCached',
  props<{ departmentId: string }>()
);

export const assignmentTermsLoadError = createAction(
  '[Assignment-term] assignmentTErmsLoadError',
  props<{ departmentId: string, error: RequestError }>()
);
