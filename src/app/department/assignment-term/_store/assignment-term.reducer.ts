import {createReducer, on} from '@ngrx/store';
import {currentTime} from '../../../_shared/helper/time-helper';
import {initialListStore, ListState} from '../../../_domain/state/list-state';
import {AssignmentTermWithAssignment} from '../../_shared/domain/dtos/assignment-term-with-assignment';
import * as AssignmentTermActions from './assignment-term.actions';

export const assignmentTermFeatureKey = 'assignmentTerm';

export interface State {
  assignmentTerms: { [id: string]: ListState<AssignmentTermWithAssignment> };
}

export const initialState: State = {
  assignmentTerms: {}
};


export const reducer = createReducer(
  initialState,
  on(AssignmentTermActions.loadAssignmentTerms, (state, action) => {
    return {
      ...state, assignmentTerms: {
        ...state.assignmentTerms,
        [action.departmentId]: {...(state.assignmentTerms[action.departmentId] || initialListStore), isLoading: true}
      }
    };
  }),
  on(AssignmentTermActions.assignmentTermsLoaded, (state, action) => {
    return {
      ...state, assignmentTerms: {
        ...state.assignmentTerms,
        [action.departmentId]: {
          ...state.assignmentTerms[action.departmentId],
          isLoading: false,
          cacheTime: currentTime(),
          list: action.assignmentTerms,
          loadError: undefined
        }
      }
    };
  }),
  on(AssignmentTermActions.assignmentTermsCached, (state, action) => {
    return {
      ...state, assignmentTerms: {
        ...state.assignmentTerms,
        [action.departmentId]: {
          ...state.assignmentTerms[action.departmentId], isLoading: false
        }
      }
    };
  }),
  on(AssignmentTermActions.assignmentTermsLoadError, (state, action) => {
    return {
      ...state, assignmentTerms: {
        ...state.assignmentTerms,
        [action.departmentId]: {
          ...state.assignmentTerms[action.departmentId], isLoading: false, loadError: action.error
        }
      }
    };
  })
);

