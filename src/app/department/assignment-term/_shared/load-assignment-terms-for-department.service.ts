import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {parseUrl} from '../../../_shared/helper/parse-url';
import {RequestError} from '../../../_shared/http-error-handling/request-error';
import {State} from '../../../_store/base.reducer';
import {CacheableByIdToActionLoader} from '../../../_store/cacheable/cacheable-by-id-to-action-loader';
import {AssignmentTermWithAssignment} from '../../_shared/domain/dtos/assignment-term-with-assignment';
import {assignmentTermsCached, assignmentTermsLoaded, assignmentTermsLoadError} from '../_store/assignment-term.actions';
import {assignmentTermsCacheInvalidForDepartment} from '../_store/assignment-term.selectors';

@Injectable({
  providedIn: 'root'
})
export class LoadAssignmentTermsForDepartmentService extends CacheableByIdToActionLoader<AssignmentTermWithAssignment[]> {

  constructor(http: HttpClient, private store: Store<State>) {
    super(http);
  }

  readonly cacheInvalidProvider = (id: string) => this.store.select(assignmentTermsCacheInvalidForDepartment(id));

  readonly endpointProvider = (id: string) => parseUrl(environment.endpoints.departments.assignmentTerms.allForDepartment, id);

  readonly cachedActionProvider = (id: string) => assignmentTermsCached({departmentId: id});

  readonly errorHandler = (id: string, error: RequestError) => of(assignmentTermsLoadError({departmentId: id, error: error}));

  readonly projection = (id: string, result: AssignmentTermWithAssignment[]) =>
    assignmentTermsLoaded({departmentId: id, assignmentTerms: result});
}
