import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {environment} from '../../../../environments/environment';
import {RequestErrorType} from '../../../_shared/http-error-handling/http-error-type.enum';
import {parseUrl} from '../../../_shared/helper/parse-url';
import {assignmentTermsCached, assignmentTermsLoaded, assignmentTermsLoadError} from '../_store/assignment-term.actions';
import * as AssignmentTermSelectors from '../_store/assignment-term.selectors';

import {LoadAssignmentTermsForDepartmentService} from './load-assignment-terms-for-department.service';

describe('LoadAssignmentTermsForDepartmentService', () => {
  let service: LoadAssignmentTermsForDepartmentService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore()
      ]
    });
    service = TestBed.inject(LoadAssignmentTermsForDepartmentService);
    mockStore = TestBed.inject(MockStore);
  });

  it('should use the correct endpointProvider', () => {
    expect(service.endpointProvider('testId'))
      .toStrictEqual(parseUrl(environment.endpoints.departments.assignmentTerms.allForDepartment, 'testId'));
  });

  it('should use the correct cacheValidityProvider', () => {
    spyOn(AssignmentTermSelectors, 'assignmentTermsCacheInvalidForDepartment').and.returnValue(() => 'test');

    expect(service.cacheInvalidProvider('testId')).toBeObservable(cold('(a)', {a: 'test'}));
  });

  it('should use the correct cachedActionProvider', () => {
    expect(service.cachedActionProvider('testId')).toStrictEqual(assignmentTermsCached({departmentId: 'testId'}));
  });

  it('should map errors to the correct action', () => {
    const error = {type: RequestErrorType.user};

    expect(service.errorHandler('testId', error))
      .toBeObservable(cold('(a|)', {a: assignmentTermsLoadError({departmentId: 'testId', error: error})}));
  });

  it('should map the loaded result to the correct action', () => {
    const res: any[] = [1, 2, 3];
    expect(service.projection('testId', res)).toStrictEqual(assignmentTermsLoaded({departmentId: 'testId', assignmentTerms: res}));
  });
});
