import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LeaveFormGuard} from '../_shared/guards/leave-form/leave-form-guard.service';
import {AddCategoryComponent} from './category/components/category-form/add-category/add-category.component';
import {EditCategoryComponent} from './category/components/category-form/edit-category/edit-category.component';
import {CategoryListComponent} from './category/components/category-list/category-list.component';
import {DepartmentDetailComponent} from './components/department-detail/department-detail.component';
import {AddDepartmentComponent} from './components/department-form/add-department/add-department.component';
import {EditDepartmentComponent} from './components/department-form/edit-department/edit-department.component';
import {DepartmentListComponent} from './components/department-list/department-list.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'list'},
  {path: 'list', component: DepartmentListComponent},
  {path: 'new', component: AddDepartmentComponent, canDeactivate: [LeaveFormGuard]},
  {
    path: 'categories', children: [
      {path: '', redirectTo: 'list'},
      {path: 'list', component: CategoryListComponent},
      {path: 'new', component: AddCategoryComponent, canDeactivate: [LeaveFormGuard]},
      {path: ':id/edit', component: EditCategoryComponent, canDeactivate: [LeaveFormGuard]}
    ]
  },
  {
    path: ':id', children: [
      {path: '', redirectTo: 'overview'},
      {path: 'overview', component: DepartmentDetailComponent},
      {path: 'edit', component: EditDepartmentComponent, canDeactivate: [LeaveFormGuard]}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepartmentRoutingModule {
}
