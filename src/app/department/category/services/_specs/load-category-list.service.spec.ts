import {HttpClientTestingModule} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {environment} from '../../../../../environments/environment';
import {RequestErrorType} from '../../../../_shared/http-error-handling/http-error-type.enum';
import {categoryListCached, categoryListLoaded, categoryListLoadError} from '../../_store/category.actions';
import {categoryListCacheInvalid} from '../../_store/category.selectors';

import { LoadCategoryListService } from '../load-category-list.service';

describe('LoadCategoryListService', () => {
  let service: LoadCategoryListService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore(),
        LoadCategoryListService
      ]
    });
    service = TestBed.inject(LoadCategoryListService);
    mockStore = TestBed.inject(MockStore);
  });

  it('should use the correct endpoint', () => {
    expect(service.endpoint).toStrictEqual(environment.endpoints.departments.categories.allCategories);
  });

  it('should select its cacheValidity correctly', () => {
    mockStore.overrideSelector(categoryListCacheInvalid, 'test' as any);

    expect(service.cacheInvalidProvider()).toBeObservable(cold('(a)', {a: 'test'}));
  });

  it('should use the correct cachedAction', () => {
    expect(service.cachedAction).toStrictEqual(categoryListCached());
  });

  it('should map errors to the correct action', () => {
    const error = {type: RequestErrorType.user};

    expect(service.errorHandler(error)).toBeObservable(cold('(a|)', {a: categoryListLoadError({error: error})}));
  });

  it('should map the loaded result to the correct action', () => {
    const res: any[] = [1, 2, 3];
    expect(service.projection(res)).toStrictEqual(categoryListLoaded({categoryList: res}));
  });
});
