import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {RequestError} from '../../../_shared/http-error-handling/request-error';
import {State} from '../../../_store/base.reducer';
import {CacheableToActionLoader} from '../../../_store/cacheable/cacheable-to-action-loader';
import {DepartmentCategory} from '../../_shared/domain/department-category';
import {categoryListCached, categoryListLoaded, categoryListLoadError} from '../_store/category.actions';
import {categoryListCacheInvalid} from '../_store/category.selectors';

@Injectable()
export class LoadCategoryListService extends CacheableToActionLoader<DepartmentCategory[]> {


  constructor(http: HttpClient, store: Store<State>) {
    super(http, () => store.select(categoryListCacheInvalid));
  }

  endpoint = environment.endpoints.departments.categories.allCategories;
  cachedAction = categoryListCached();

  errorHandler = (error: RequestError) => of(categoryListLoadError({error: error}));

  readonly projection = categoryList => categoryListLoaded({categoryList: categoryList});
}
