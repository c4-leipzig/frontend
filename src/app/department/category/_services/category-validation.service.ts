import {Injectable} from '@angular/core';
import {ValidatorFn} from '@angular/forms';
import {Store} from '@ngrx/store';
import {validateValueNotInList} from '../../../_shared/validators/validate-value-not-in-list';
import {State} from '../../../_store/base.reducer';
import {categoryNameList} from '../_store/category.selectors';

@Injectable({
  providedIn: 'root'
})
export class CategoryValidationService {

  constructor(private store: Store<State>) {
  }

  categoryNameValidator(selfName?: string): ValidatorFn {
    return validateValueNotInList(this.store.select(categoryNameList), selfName);
  }
}
