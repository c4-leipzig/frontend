import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import {Id} from '../../../_domain/id';
import {parseUrl} from '../../../_shared/helper/parse-url';
import {RequestError} from '../../../_shared/http-error-handling/request-error';
import {SaveElementService} from '../../../_store/save/save-element.service';
import {DepartmentCategory} from '../../_shared/domain/department-category';
import {categorySaved, categorySaveError} from '../_store/category.actions';

@Injectable({
  providedIn: 'root'
})
export class SaveCategoryService extends SaveElementService<DepartmentCategory> {


  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  protected readonly createErrorHandler = (error: RequestError) => of(categorySaveError({error: error}));

  protected readonly createProjection = (id: Id) => categorySaved(id);

  protected createRaw = (element: DepartmentCategory) => this.http.post<Id>(environment.endpoints.departments.categories.create, element);

  navigateAfterSave = (id: string) => this.router.navigate(['/departments/categories'], {state: {canDeactivate: true}});

  protected readonly updateErrorHandler = (error: RequestError) => of(categorySaveError({error: error}));

  protected readonly updateProjection = (id: Id) => categorySaved(id);

  protected updateRaw = (element: DepartmentCategory) =>
    this.http.put(parseUrl(environment.endpoints.departments.categories.update, element.id), element)
      .pipe(map(() => {
        return {id: element.id};
      }));

}
