import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {TableModule} from 'primeng/table';
import {SharedModule} from '../../_shared/shared.module';
import {CategoryEffects} from './_store/category.effects';
import * as fromCategory from './_store/category.reducer';
import {AddCategoryComponent} from './components/category-form/add-category/add-category.component';
// tslint:disable-next-line:max-line-length
import {CategoryFormPresentationComponent} from './components/category-form/category-form-presentation/category-form-presentation.component';
import {EditCategoryComponent} from './components/category-form/edit-category/edit-category.component';
// tslint:disable-next-line:max-line-length
import {CategoryListPresentationComponent} from './components/category-list/category-list-presentation/category-list-presentation.component';
import {CategoryListComponent} from './components/category-list/category-list.component';

@NgModule({
  declarations: [
    CategoryListComponent,
    CategoryListPresentationComponent,
    EditCategoryComponent,
    CategoryFormPresentationComponent,
    AddCategoryComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromCategory.categoryFeatureKey, fromCategory.reducer),
    EffectsModule.forFeature([CategoryEffects]),
    SharedModule,
    CardModule,
    TableModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    RouterModule.forChild([])
  ]
})
export class CategoryModule {
}
