import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {DepartmentCategory} from '../../../../_shared/domain/department-category';
import {CategoryValidationService} from '../../../_services/category-validation.service';

@Component({
  selector: 'c4-category-form-presentation',
  templateUrl: './category-form-presentation.component.html',
  styleUrls: ['./category-form-presentation.component.scss']
})
export class CategoryFormPresentationComponent implements OnInit, OnDestroy {
  @Input() category: DepartmentCategory;

  @Output() save = new EventEmitter<DepartmentCategory>();
  @Output() dirtied = new EventEmitter<void>();

  categoryForm: FormGroup;

  private destroy$ = new Subject();

  constructor(private fb: FormBuilder,
              private categoryValidationService: CategoryValidationService) {
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  private initializeForm(): void {
    this.categoryForm = this.fb.group({
      name: [this.category?.name, [Validators.required], [this.categoryValidationService.categoryNameValidator(this.category?.name)]]
    });

    this.categoryForm.valueChanges.pipe(take(1), takeUntil(this.destroy$)).subscribe(() => this.dirtied.next());
  }

  submit(): void {
    this.save.emit({...this.category, name: this.categoryForm.controls.name.value});
  }
}
