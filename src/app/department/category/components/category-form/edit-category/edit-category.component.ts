import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {SaveType} from '../../../../../_domain/state/save-state';
import {FormComponent} from '../../../../../_shared/guards/leave-form/form-component';
import {State} from '../../../../../_store/base.reducer';
import {backLinkSeverity, ButtonSeverity, labelledBackLink} from '../../../../../navigation/_shared/bottom-nav';
import {setTopAndBottomNav} from '../../../../../navigation/_store/navigation.actions';
import {DepartmentCategory} from '../../../../_shared/domain/department-category';
import {loadCategoryList, saveCategory} from '../../../_store/category.actions';
import {isSubmittingCategory, singleCategory} from '../../../_store/category.selectors';

@Component({
  selector: 'c4-edit-category',
  templateUrl: './edit-category.component.html'
})
export class EditCategoryComponent implements OnInit, FormComponent {
  id: string;
  category$: Observable<DepartmentCategory>;
  isSubmitting$ = this.store.select(isSubmittingCategory);
  isDirty = false;

  constructor(private route: ActivatedRoute,
              private store: Store<State>) { }

  ngOnInit(): void {
    this.store.dispatch(loadCategoryList());
    this.id = this.route.snapshot.paramMap.get('id');
    this.category$ = this.store.select(singleCategory(this.id));
    this.setNavigation();
  }

  private setNavigation(buttonSeverity = ButtonSeverity.secondary): void {
    this.store.dispatch(setTopAndBottomNav({
      topNav: {button1: labelledBackLink('/departments/categories', 'Abbrechen')},
      bottomNav: {button1: backLinkSeverity('/departments/categories', buttonSeverity)}
    }));
  }

  save(category: DepartmentCategory): void {
    this.store.dispatch(saveCategory({category: category, saveType: SaveType.update}));
  }

  setDirty(): void {
    if (!this.isDirty) {
      this.isDirty = true;
      this.setNavigation(ButtonSeverity.warning);
    }
  }

}
