import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {SaveType} from '../../../../../_domain/state/save-state';
import {FormComponent} from '../../../../../_shared/guards/leave-form/form-component';
import {State} from '../../../../../_store/base.reducer';
import {backLinkSeverity, ButtonSeverity, labelledBackLink} from '../../../../../navigation/_shared/bottom-nav';
import {setTopAndBottomNav} from '../../../../../navigation/_store/navigation.actions';
import {DepartmentCategory, emptyDepartmentCategory} from '../../../../_shared/domain/department-category';
import {loadCategoryList, saveCategory} from '../../../_store/category.actions';
import {isSubmittingCategory} from '../../../_store/category.selectors';

@Component({
  selector: 'c4-add-category',
  templateUrl: './add-category.component.html'
})
export class AddCategoryComponent implements OnInit, FormComponent {
  category = emptyDepartmentCategory();
  isDirty: boolean;
  isSubmitting$ = this.store.select(isSubmittingCategory);

  constructor(private store: Store<State>) {
  }


  ngOnInit(): void {
    this.store.dispatch(loadCategoryList());
    this.store.dispatch(setTopAndBottomNav({
      topNav: {button1: labelledBackLink('/departments/categories', 'Abbrechen')},
      bottomNav: {button1: backLinkSeverity('/departments/categories', ButtonSeverity.warning)}
    }));
  }

  save(category: DepartmentCategory): void {
    this.store.dispatch(saveCategory({category: category, saveType: SaveType.create}));
  }
}
