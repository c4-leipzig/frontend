import {Component, Input, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {DepartmentCategory} from '../../../../_shared/domain/department-category';

@Component({
  selector: 'c4-category-list-presentation',
  templateUrl: './category-list-presentation.component.html',
  styleUrls: ['./category-list-presentation.component.scss']
})
export class CategoryListPresentationComponent {
  @Input() categories: DepartmentCategory[];
  @Input() set searchTerm(searchTerm: string) {
    this.categoryTableRef?.filterGlobal(searchTerm, 'contains');
  }

  @ViewChild('categoryTable') categoryTableRef: Table;
}
