import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from '../../../../_store/base.reducer';
import {backButton, createLink, labelledBackButton, labelledCreateLink} from '../../../../navigation/_shared/bottom-nav';
import {createSearchConfig} from '../../../../navigation/_shared/search-config';
import {setSearchConfig, setTopAndBottomNav} from '../../../../navigation/_store/navigation.actions';
import {searchTerm} from '../../../../navigation/_store/navigation.selectors';
import {loadCategoryList} from '../../_store/category.actions';
import {categoryList, categoryListIsLoading} from '../../_store/category.selectors';

@Component({
  selector: 'c4-category-list',
  templateUrl: './category-list.component.html'
})
export class CategoryListComponent implements OnInit {
  categories$ = this.store.select(categoryList);
  categoriesLoading$ = this.store.select(categoryListIsLoading);
  categorySearch$ = this.store.select(searchTerm);

  constructor(private store: Store<State>, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.store.dispatch(loadCategoryList());
    this.store.dispatch(setSearchConfig({searchConfig: createSearchConfig('Kategorie suchen')}));
    this.store.dispatch(setTopAndBottomNav({
      topNav: {
        button1: labelledBackButton(this.navigateBack, 'Ressort-Übersicht'),
        button2: labelledCreateLink('/departments/categories/new', 'Kategorie erstellen')
      },
      bottomNav: {
        button1: backButton(this.navigateBack),
        button4: createLink('/departments/categories/new')
      }
    }));
  }

  private get navigateBack(): () => void {
    return () => this.router.navigate(['../..'], {relativeTo: this.route});
  }

}
