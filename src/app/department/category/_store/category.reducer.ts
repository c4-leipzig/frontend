import {createReducer, on} from '@ngrx/store';
import {initialListStore, ListState} from '../../../_domain/state/list-state';
import {initialSaveStore, SaveState} from '../../../_domain/state/save-state';
import {currentTime} from '../../../_shared/helper/time-helper';
import {DepartmentCategory} from '../../_shared/domain/department-category';
import * as CategoryActions from './category.actions';

export const categoryFeatureKey = 'departmentCategories';

export interface State {
  categoryList: ListState<DepartmentCategory>;
  saveCategory: SaveState<DepartmentCategory>;
}

export const initialState: State = {
  categoryList: initialListStore,
  saveCategory: initialSaveStore
};

export const reducer = createReducer(
  initialState,

  on(CategoryActions.loadCategoryList, state => {
    return {...state, categoryList: {...state.categoryList, isLoading: true}};
  }),
  on(CategoryActions.categoryListLoaded, (state, action) => {
    return {
      ...state,
      categoryList: {...state.categoryList, isLoading: false, cacheTime: currentTime(), list: action.categoryList, loadError: undefined}
    };
  }),
  on(CategoryActions.categoryListCached, state => {
    return {...state, categoryList: {...state.categoryList, isLoading: false}};
  }),
  on(CategoryActions.categoryListLoadError, (state, update) => {
    return {...state, categoryList: {...state.categoryList, isLoading: false, loadError: update.error}};
  }),
  on(CategoryActions.invalidateCategoryListCache, state => {
    return {...state, categoryList: {...state.categoryList, cacheTime: 0}};
  }),

  on(CategoryActions.saveCategory, (state, action) => {
    return {...state, saveCategory: {...state.saveCategory, isSubmitting: true, element: action.category, submitError: undefined}};
  }),
  on(CategoryActions.categorySaved, state => {
    return {...state, saveCategory: {...state.saveCategory, isSubmitting: false, element: undefined}};
  }),
  on(CategoryActions.categorySaveError, (state, action) => {
    return {...state, saveCategory: {...state.saveCategory, isSubmitting: false, submitError: action.error}};
  })
);

