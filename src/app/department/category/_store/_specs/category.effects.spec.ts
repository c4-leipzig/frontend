import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {cold, hot} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {SaveType} from '../../../../_domain/state/save-state';
import {SaveCategoryService} from '../../_services/save-category.service';
import {LoadCategoryListService} from '../../services/load-category-list.service';
import {categoryListLoaded, categorySaved, invalidateCategoryListCache, loadCategoryList, saveCategory} from '../category.actions';

import {CategoryEffects} from '../category.effects';

describe('CategoryEffects', () => {
  let actions$: Observable<any>;
  let effects: CategoryEffects;
  let mockLoadCategoryListService: LoadCategoryListService;
  let mockSaveCategoryService: SaveCategoryService;
  const list = [1, 2, 3] as any[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CategoryEffects,
        provideMockActions(() => actions$),
        {
          provide: LoadCategoryListService,
          useValue: {get: jest.fn()}
        },
        {
          provide: SaveCategoryService,
          useValue: {save: jest.fn(), navigateAfterSave: jest.fn()}
        }
      ]
    });

    effects = TestBed.inject(CategoryEffects);
    mockLoadCategoryListService = TestBed.inject(LoadCategoryListService);
    mockSaveCategoryService = TestBed.inject(SaveCategoryService);
  });

  describe('loadCategoryList', () => {
    it('should load categories', () => {
      actions$ = hot('--a', {a: loadCategoryList()});
      spyOn(mockLoadCategoryListService, 'get').and.returnValue(of(categoryListLoaded({categoryList: list})));
      expect(effects.loadCategoryList$).toBeObservable(cold('--a', {a: categoryListLoaded({categoryList: list})}));
    });
  });

  describe('saveCategory', () => {
    it('should trigger saving of category and yield according action', () => {
      actions$ = hot('--a', {a: saveCategory({category: {dep: 'artment'} as any, saveType: SaveType.create})});
      spyOn(mockSaveCategoryService, 'save').and.returnValue(of(categorySaved({id: 'anyId'})));
      expect(effects.saveCategory$).toBeObservable(cold('--a', {a: categorySaved({id: 'anyId'})}));
    });
  });

  describe('categorySaved', () => {
    it('should navigate to saved category and invalidate categoryListCache', () => {
      actions$ = hot('--a', {a: categorySaved({id: 'anyId'})});
      expect(effects.categorySaved$).toBeObservable(cold('--a', {a: invalidateCategoryListCache()}));
    });
  });
});
