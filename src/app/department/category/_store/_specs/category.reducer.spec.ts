import {initialSaveStore, SaveState, SaveType} from '../../../../_domain/state/save-state';
import {RequestErrorType} from '../../../../_shared/http-error-handling/http-error-type.enum';
import {DepartmentCategory} from '../../../_shared/domain/department-category';
import {
  categoryListCached,
  categoryListLoaded,
  categoryListLoadError,
  categorySaved,
  categorySaveError,
  invalidateCategoryListCache,
  loadCategoryList,
  saveCategory
} from '../category.actions';
import {initialState, reducer} from '../category.reducer';

describe('Category Reducer', () => {
  describe('loadCategoryList', () => {
    it('should set the loading flag', () => {
      const expected = {...initialState, categoryList: {...initialState.categoryList, isLoading: true}};

      expect(reducer(initialState, loadCategoryList())).toStrictEqual(expected);
    });
  });

  describe('categoryListCached', () => {
    it('should reset loading flag', () => {
      const initial = {...initialState, categoryList: {...initialState.categoryList, isLoading: true}};

      expect(reducer(initial, categoryListCached())).toStrictEqual(initialState);
    });
  });

  describe('categoryListLoaded', () => {
    it('should save list of categories and reset loading flag', () => {
      const initial = {...initialState, categoryList: {...initialState.categoryList, isLoading: true}};
      const list: any[] = [1, 2, 3];

      const result = reducer(initial, categoryListLoaded({categoryList: list}));

      expect(result.categoryList.isLoading).toBe(false);
      expect(result.categoryList.list).toBe(list);
      expect(result.categoryList.cacheTime).toBeGreaterThan(0);

    });
  });

  describe('categoryListLoadError', () => {
    it('should save error and reset loading flag', () => {
      const initial = {...initialState, categoryList: {...initialState.categoryList, isLoading: true}};
      const error = {type: RequestErrorType.user};
      const result = reducer(initial, categoryListLoadError({error: error}));

      expect(result.categoryList.isLoading).toBe(false);
      expect(result.categoryList.loadError).toBe(error);
      expect(result.categoryList.cacheTime).toBe(0);
    });
  });

  describe('saveCategory', () => {
    it('should set submit-flag and element and reset errors', () => {
      const saveCategoryState: SaveState<DepartmentCategory> = {...initialSaveStore, submitError: {type: RequestErrorType.server}};
      const category: any = {cat: 'egory'};
      expect(reducer({...initialState, saveCategory: saveCategoryState},
        saveCategory({category: category, saveType: SaveType.update})).saveCategory)
        .toStrictEqual({isSubmitting: true, submitError: undefined, element: category});
    });
  });

  describe('categorySaved', () => {
    it('should reset submit-flag and element', () => {
      const saveCategoryState: SaveState<DepartmentCategory> = {...initialSaveStore, isSubmitting: true, element: {cat: 'egory'} as any};
      expect(reducer({...initialState, saveCategory: saveCategoryState}, categorySaved({id: 'id'})).saveCategory)
        .toStrictEqual({isSubmitting: false, element: undefined});
    });
  });

  describe('categorySaveError', () => {
    it('should set error but not reset category', () => {
      const saveCategoryState: SaveState<DepartmentCategory> = {...initialSaveStore, isSubmitting: true, element: {cat: 'egory'} as any};
      expect(reducer({...initialState, saveCategory: saveCategoryState}, categorySaveError({
        error: {
          type: RequestErrorType.server
        }
      })).saveCategory).toStrictEqual({isSubmitting: false, element: {cat: 'egory'}, submitError: {type: RequestErrorType.server}});
    });
  });

  describe('invalidateCategoryListCache', () => {
    it('should reset cacheTime', () => {
      const initial = {...initialState, categoryList: {...initialState.categoryList, cacheTime: 100}};
      const result = reducer(initial, invalidateCategoryListCache());

      expect(result.categoryList).toStrictEqual({...initial.categoryList, cacheTime: 0});
    });
  });
});
