import {environment} from '../../../../../environments/environment';
import {currentTime} from '../../../../_shared/helper/time-helper';
import {
  categoryIdList,
  categoryList,
  categoryListCacheInvalid,
  categoryListIsLoading,
  categoryNameList,
  isSubmittingCategory,
  singleCategory
} from '../category.selectors';

describe('Category Selectors', () => {
  describe('categoryListCacheInvalid', () => {
    it('should yield true if cacheTime is too far in the past', () => {
      const state = {departmentCategories: {categoryList: {cacheTime: currentTime() - (environment.cacheTime + 100)}}};

      expect(categoryListCacheInvalid(state)).toBe(true);
    });

    it('should yield false if cache is still valid', () => {
      const state = {departmentCategories: {categoryList: {cacheTime: currentTime() - (environment.cacheTime - 100)}}};

      expect(categoryListCacheInvalid(state)).toBe(false);
    });
  });

  describe('withState', () => {
// tslint:disable-next-line:one-variable-per-declaration
    let cat1, cat2, user1, user2, state;
    beforeEach(() => {
      cat1 = {id: 'c1', name: 'cn1'};
      cat2 = {id: 'c2', name: 'cn2'};
      user1 = {id: 'u1', username: 'name1'};
      user2 = {id: 'u2', username: 'name2'};

      state = {
        departmentCategories: {
          categoryList: {list: [cat1, cat2]}
        },
        users: {memberList: {list: [user1, user2]}}
      };
    });

    describe('categoryIdList', () => {
      it('should return a list of all category ids', () => {
        expect(categoryIdList(state)).toStrictEqual(['c1', 'c2']);
      });
    });

    describe('categoryNameList', () => {
      it('should return a list of all category names', () => {
        expect(categoryNameList(state)).toStrictEqual(['cn1', 'cn2']);
      });
    });

    describe('categoryList', () => {
      it('should return the category list', () => {
        expect(categoryList(state)).toStrictEqual([cat1, cat2]);
      });
    });

    describe('singleCategory', () => {
      it('should return a category by its id', () => {
        expect(singleCategory('c1')(state)).toStrictEqual(cat1);
      });

      it('should return undefined if department ist not found', () => {
        expect(singleCategory('asdf')(state)).toBeUndefined();
      });
    });
  });


  describe('categoryListIsLoading', () => {
    it('should be true if flag is set', () => {
      const state = {departmentCategories: {categoryList: {isLoading: true}}};
      expect(categoryListIsLoading(state)).toBe(true);
    });

    it('should be false if flag is not set', () => {
      const state = {departmentCategories: {categoryList: {isLoading: false}}};
      expect(categoryListIsLoading(state)).toBe(false);
    });
  });

  describe('isSubmittingCategory', () => {
    it('should return the value of the submit flag for saving a category', () => {
      const state = {departmentCategories: {saveCategory: {isSubmitting: 'asdf' as any}}};
      expect(isSubmittingCategory(state)).toStrictEqual('asdf');
    });
  });
});
