import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import {flatMap, map, tap} from 'rxjs/operators';
import {SaveCategoryService} from '../_services/save-category.service';
import {LoadCategoryListService} from '../services/load-category-list.service';
import * as CategoryActions from './category.actions';
import {invalidateCategoryListCache} from './category.actions';

@Injectable()
export class CategoryEffects {

  loadCategoryList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CategoryActions.loadCategoryList),
      flatMap(action => this.loadCategoryListService.get(action.reload))
    );
  }, {dispatch: true});

  saveCategory$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CategoryActions.saveCategory),
      flatMap(action => this.saveCategoryService.save(action.category, action.saveType))
    );
  }, {dispatch: true});

  categorySaved$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CategoryActions.categorySaved),
      tap(action => this.saveCategoryService.navigateAfterSave(action.id)),
      map(() => invalidateCategoryListCache())
    );
  }, {dispatch: true});


  constructor(private actions$: Actions,
              private loadCategoryListService: LoadCategoryListService,
              private saveCategoryService: SaveCategoryService) {
  }

}
