import {createFeatureSelector, createSelector} from '@ngrx/store';
import {isCacheInvalid} from '../../../_shared/helper/time-helper';
import * as fromCategory from './category.reducer';

export const selectCategoryState = createFeatureSelector<fromCategory.State>(fromCategory.categoryFeatureKey);

export const categoryListCacheInvalid = createSelector(selectCategoryState, state => isCacheInvalid(state.categoryList));
export const categoryList = createSelector(selectCategoryState, state => state.categoryList.list || []);
export const categoryIdList = createSelector(categoryList, categories => categories.map(category => category.id));
export const categoryNameList = createSelector(categoryList, categories => categories.map(category => category.name));
export const categoryListIsLoading = createSelector(selectCategoryState, state => state.categoryList.isLoading);
export const singleCategory =
  (id: string) => createSelector(selectCategoryState, state => state.categoryList.list?.find(cat => cat.id === id));
export const isSubmittingCategory = createSelector(selectCategoryState, state => state.saveCategory.isSubmitting);
