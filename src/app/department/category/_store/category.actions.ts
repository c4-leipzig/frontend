import { createAction, props } from '@ngrx/store';
import {Id} from '../../../_domain/id';
import {SaveType} from '../../../_domain/state/save-state';
import {RequestError} from '../../../_shared/http-error-handling/request-error';
import {DepartmentCategory} from '../../_shared/domain/department-category';

export const loadCategoryList = createAction(
  '[DepartmentCategory] Load Category list',
  (reload = false) => ({reload: reload})
);

export const categoryListLoaded = createAction(
  '[DepartmentCategory] Category list loaded',
  props<{ categoryList: DepartmentCategory[] }>()
);

export const categoryListCached = createAction(
  '[DepartmentCategory] Category list cached'
);

export const categoryListLoadError = createAction(
  '[DepartmentCategory] categoryListLoadError',
  props<{ error: RequestError }>()
);

export const invalidateCategoryListCache = createAction(
  '[Category] invalidateCategoryListCache'
);

export const saveCategory = createAction(
  '[Category] saveCategory',
  props<{ category: DepartmentCategory, saveType: SaveType }>()
);

export const categorySaved = createAction(
  '[Category] categorySaved',
  props<Id>()
);

export const categorySaveError = createAction(
  '[Category] categorySaveError',
  props<{ error: RequestError }>()
);
