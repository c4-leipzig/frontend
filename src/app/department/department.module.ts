import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {PanelModule} from 'primeng/panel';
import {TableModule} from 'primeng/table';
import {SharedModule} from '../_shared/shared.module';
import {C4EditorModule} from '../editor/c4-editor.module';
import {LoadCategoryListService} from './category/services/load-category-list.service';
import {LoadDepartmentListService} from './_services/load-department-list.service';
import {DepartmentEffects} from './_store/department.effects';
import * as fromDepartment from './_store/department.reducer';
import {AssignmentTermModule} from './assignment-term/assignment-term.module';
import {DepartmentDetailPresentationComponent} from './components/department-detail/department-detail-presentation/department-detail-presentation.component';
import {DepartmentDetailComponent} from './components/department-detail/department-detail.component';
import {DepartmentFormPresentationComponent} from './components/department-form/department-form-presentation/department-form-presentation.component';
import {EditDepartmentComponent} from './components/department-form/edit-department/edit-department.component';
import {DepartmentListPresentationComponent} from './components/department-list/department-list-presentation/department-list-presentation.component';
import {DepartmentListComponent} from './components/department-list/department-list.component';
import {DepartmentRoutingModule} from './department-routing.module';
import { AddDepartmentComponent } from './components/department-form/add-department/add-department.component';
import { CategoryModule } from './category/category.module';


@NgModule({
  declarations: [
    DepartmentListComponent,
    DepartmentListPresentationComponent,
    DepartmentDetailComponent,
    DepartmentDetailPresentationComponent,
    EditDepartmentComponent,
    DepartmentFormPresentationComponent,
    AddDepartmentComponent,
  ],
  imports: [
    CommonModule,
    DepartmentRoutingModule,
    StoreModule.forFeature(fromDepartment.departmentFeatureKey, fromDepartment.reducer),
    EffectsModule.forFeature([DepartmentEffects]),
    SharedModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    CardModule,
    PanelModule,
    AssignmentTermModule,
    AccordionModule,
    ReactiveFormsModule,
    DropdownModule,
    C4EditorModule,
    CategoryModule
  ],
  providers: [
    LoadDepartmentListService,
    LoadCategoryListService
  ]
})
export class DepartmentModule {
}
