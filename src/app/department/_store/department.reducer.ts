import {createReducer, on} from '@ngrx/store';
import {currentTime} from '../../_shared/helper/time-helper';
import {SaveState, initialSaveStore} from '../../_domain/state/save-state';
import {initialListStore, ListState} from '../../_domain/state/list-state';
import {Department} from '../_shared/domain/department';
import {DepartmentWithActiveAssignment} from '../_shared/domain/dtos/department-with-active-assignment';
import * as DepartmentActions from './department.actions';

export const departmentFeatureKey = 'department';

export interface State {
  departmentList: ListState<DepartmentWithActiveAssignment>;
  saveDepartment: SaveState<Department>;
}

export const initialState: State = {
  departmentList: initialListStore,
  saveDepartment: initialSaveStore
};


export const reducer = createReducer(
  initialState,
  on(DepartmentActions.loadDepartmentList, state => {
    return {...state, departmentList: {...state.departmentList, isLoading: true}};
  }),
  on(DepartmentActions.departmentListLoaded, (state, action) => {
    return {
      ...state,
      departmentList: {
        ...state.departmentList,
        isLoading: false,
        cacheTime: currentTime(),
        list: action.departmentList,
        loadError: undefined
      }
    };
  }),
  on(DepartmentActions.departmentListCached, state => {
    return {...state, departmentList: {...state.departmentList, isLoading: false}};
  }),
  on(DepartmentActions.departmentListLoadError, (state, update) => {
    return {...state, departmentList: {...state.departmentList, isLoading: false, loadError: update.error}};
  }),
  on(DepartmentActions.invalidateDepartmentListCache, state => {
    return {...state, departmentList: {...state.departmentList, cacheTime: 0}};
  }),

  on(DepartmentActions.saveDepartment, (state, action) => {
    return {...state, saveDepartment: {...state.saveDepartment, isSubmitting: true, element: action.department, submitError: undefined}};
  }),
  on(DepartmentActions.departmentSaved, state => {
    return {...state, saveDepartment: {...state.saveDepartment, isSubmitting: false, element: undefined}};
  }),
  on(DepartmentActions.departmentSaveError, (state, action) => {
    return {...state, saveDepartment: {...state.saveDepartment, isSubmitting: false, submitError: action.error}};
  })
);

