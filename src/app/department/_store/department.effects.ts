import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {merge, of} from 'rxjs';
import {flatMap, map, switchMap, tap} from 'rxjs/operators';
import {loadMemberList} from '../../users/_store/users.actions';
import {LoadDepartmentListService} from '../_services/load-department-list.service';
import {SaveDepartmentService} from '../_services/save-department.service';
import {loadCategoryList} from '../category/_store/category.actions';
import * as DepartmentActions from './department.actions';
import {invalidateDepartmentListCache, loadDepartmentList} from './department.actions';


@Injectable()
export class DepartmentEffects {
  loadDepartmentList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DepartmentActions.loadDepartmentList),
      switchMap(action =>
        merge(
          this.loadDepartmentListService.get(action.reload),
          of(loadCategoryList(action.reload)),
          of(loadMemberList(action.reload))
        )
      )
    );
  }, {dispatch: true});

  loadSingleDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DepartmentActions.loadSingleDepartment),
      map(action => loadDepartmentList(action.reload))
    );
  }, {dispatch: true});

  saveDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DepartmentActions.saveDepartment),
      flatMap(action => this.saveDepartmentService.save(action.department, action.saveType))
    );
  }, {dispatch: true});

  departmentSaved$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DepartmentActions.departmentSaved),
      tap(action => this.saveDepartmentService.navigateAfterSave(action.id)),
      map(() => invalidateDepartmentListCache())
    );
  }, {dispatch: true});

  constructor(private actions$: Actions,
              private loadDepartmentListService: LoadDepartmentListService,
              private saveDepartmentService: SaveDepartmentService) {
  }

}
