import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {provideMockStore} from '@ngrx/store/testing';
import {cold, hot} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {SaveType} from '../../../_domain/state/save-state';
import {loadMemberList} from '../../../users/_store/users.actions';
import {LoadDepartmentListService} from '../../_services/load-department-list.service';
import {SaveDepartmentService} from '../../_services/save-department.service';
import {loadCategoryList} from '../../category/_store/category.actions';
import {
  departmentListLoaded,
  departmentSaved,
  invalidateDepartmentListCache,
  loadDepartmentList,
  loadSingleDepartment,
  saveDepartment
} from '../department.actions';

import {DepartmentEffects} from '../department.effects';
import {initialState} from '../department.reducer';

describe('DepartmentEffects', () => {
  let actions$: Observable<any>;
  let effects: DepartmentEffects;
  let mockLoadDepartmentListService;
  let mockSaveDepartmentService;
  const list = [1, 2, 3] as any[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DepartmentEffects,
        provideMockActions(() => actions$),
        provideMockStore({initialState: {department: initialState}}),
        {
          provide: LoadDepartmentListService,
          useValue: {get: jest.fn()}
        },
        {
          provide: SaveDepartmentService,
          useValue: {save: jest.fn(), navigateAfterSave: jest.fn()}
        }
      ]
    });

    effects = TestBed.inject(DepartmentEffects);
    mockLoadDepartmentListService = TestBed.inject(LoadDepartmentListService);
    mockSaveDepartmentService = TestBed.inject(SaveDepartmentService);
  });

  describe('loadDepartmentList', () => {
    it('should load departments and trigger loading of categories and users', () => {
      actions$ = hot('--a', {a: loadDepartmentList()});
      spyOn(mockLoadDepartmentListService, 'get').and.returnValue(of(departmentListLoaded({departmentList: list})));
      expect(effects.loadDepartmentList$).toBeObservable(cold('--(abc)',
        {
          a: departmentListLoaded({departmentList: list}),
          b: loadCategoryList(false),
          c: loadMemberList(false)
        }));
    });
  });

  describe('loadSingleDepartment', () => {
    it('should trigger loading of the department list', () => {
      actions$ = hot('a', {a: loadSingleDepartment({id: 'departmentId'})});
      expect(effects.loadSingleDepartment$).toBeObservable(cold('a', {a: loadDepartmentList()}));
    });
  });

  describe('saveDepartment', () => {
    it('should trigger saving of department and yield according action', () => {
      actions$ = hot('--a', {a: saveDepartment({department: {dep: 'artment'} as any, saveType: SaveType.create})});
      spyOn(mockSaveDepartmentService, 'save').and.returnValue(of(departmentSaved({id: 'anyId'})));
      expect(effects.saveDepartment$).toBeObservable(cold('--a', {a: departmentSaved({id: 'anyId'})}));
    });
  });

  describe('departmentSaved', () => {
    it('should navigate to saved department and invalidate departmentListCache', () => {
      actions$ = hot('--a', {a: departmentSaved({id: 'anyId'})});
      expect(effects.departmentSaved$).toBeObservable(cold('--a', {a: invalidateDepartmentListCache()}));
    });
  });
});
