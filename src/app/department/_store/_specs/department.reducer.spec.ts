import {RequestErrorType} from '../../../_shared/http-error-handling/http-error-type.enum';
import {initialSaveStore, SaveState, SaveType} from '../../../_domain/state/save-state';
import {Department} from '../../_shared/domain/department';
import {
  departmentListCached,
  departmentListLoaded,
  departmentListLoadError,
  departmentSaved,
  departmentSaveError, invalidateDepartmentListCache,
  loadDepartmentList,
  loadSingleDepartment,
  saveDepartment
} from '../department.actions';
import {initialState, reducer} from '../department.reducer';

describe('Department Reducer', () => {
  describe('loadDepartmentList', () => {
    it('should set the loading flag', () => {
      const expected = {...initialState, departmentList: {...initialState.departmentList, isLoading: true}};

      expect(reducer(initialState, loadDepartmentList())).toStrictEqual(expected);
    });
  });

  describe('departmentListCached', () => {
    it('should reset loading flag', () => {
      const initial = {...initialState, departmentList: {...initialState.departmentList, isLoading: true}};

      expect(reducer(initial, departmentListCached)).toStrictEqual(initialState);
    });
  });

  describe('departmentListLoaded', () => {
    it('should save list of departments and reset loading flag', () => {
      const initial = {...initialState, departmentList: {...initialState.departmentList, isLoading: true}};
      const list: any[] = [1, 2, 3];

      const result = reducer(initial, departmentListLoaded({departmentList: list}));

      expect(result.departmentList.isLoading).toBe(false);
      expect(result.departmentList.list).toBe(list);
      expect(result.departmentList.cacheTime).toBeGreaterThan(0);
    });
  });

  describe('departmentListLoadError', () => {
    it('should save error and reset loading flag', () => {
      const initial = {...initialState, departmentList: {...initialState.departmentList, isLoading: true}};
      const error = {type: RequestErrorType.notFound};
      const result = reducer(initial, departmentListLoadError({error: error}));

      expect(result.departmentList.isLoading).toBe(false);
      expect(result.departmentList.loadError).toBe(error);
      expect(result.departmentList.cacheTime).toBe(0);
    });
  });

  describe('invalidateDepartmentListCache', () => {
    it('should reset cacheTime', () => {
      const initial = {...initialState, departmentList: {...initialState.departmentList, cacheTime: 100}};
      const result = reducer(initial, invalidateDepartmentListCache());

      expect(result.departmentList).toStrictEqual({...initial.departmentList, cacheTime: 0});
    });
  });

  describe('loadSingleDepartment', () => {
    it('should not alter the state', () => {
      const result = reducer({asdf: 'qwer'} as any, loadSingleDepartment({id: 'id'}));

      expect(result).toEqual({asdf: 'qwer'});
    });
  });

  describe('saveDepartment', () => {
    it('should set submit-flag and element and reset errors', () => {
      const saveDepartmentState: SaveState<Department> = {...initialSaveStore, submitError: {type: RequestErrorType.server}};
      const department: any = {dep: 'artment'};
      expect(reducer({...initialState, saveDepartment: saveDepartmentState}, saveDepartment({
        department: department,
        saveType: SaveType.update
      })).saveDepartment)
        .toStrictEqual({isSubmitting: true, submitError: undefined, element: department});
    });
  });

  describe('departmentSaved', () => {
    it('should reset submit-flag and element', () => {
      const saveDepartmentState: SaveState<Department> = {...initialSaveStore, isSubmitting: true, element: {dep: 'artment'} as any};
      expect(reducer({...initialState, saveDepartment: saveDepartmentState}, departmentSaved({id: 'id'})).saveDepartment)
        .toStrictEqual({isSubmitting: false, element: undefined});
    });
  });

  describe('departmentSaveError', () => {
    it('should set error but not reset department', () => {
      const saveDepartmentState: SaveState<Department> = {...initialSaveStore, isSubmitting: true, element: {dep: 'artment'} as any};
      expect(reducer({...initialState, saveDepartment: saveDepartmentState}, departmentSaveError({
        error: {
          type: RequestErrorType.server
        }
      })).saveDepartment).toStrictEqual({isSubmitting: false, element: {dep: 'artment'}, submitError: {type: RequestErrorType.server}});
    });
  });
});
