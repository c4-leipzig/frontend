import {environment} from '../../../../environments/environment';
import {currentTime} from '../../../_shared/helper/time-helper';
import {initialListStore} from '../../../_domain/state/list-state';
import * as fromDepartment from '../department.reducer';
import {
  singleDepartmentWithCategoryAndUsername,
  departmentListCacheInvalid,
  departmentListIsLoading,
  departmentListWithCategoryAndUsername,
  selectDepartmentState, singleDepartment, departmentNameList, isSubmittingDepartment
} from '../department.selectors';

describe('Department Selectors', () => {
  it('should select the feature state', () => {
    const result = selectDepartmentState({
      [fromDepartment.departmentFeatureKey]: {a: 1}
    });

    expect(result).toEqual({a: 1});
  });

  describe('withState', () => {
    // tslint:disable-next-line:one-variable-per-declaration
    let dep1, dep2, dep3, cat1, cat2, user1, user2, state;
    beforeEach(() => {
      dep1 = {department: {departmentCategoryId: 'c1', id: 'id1', name: 'name1'}, assignmentTerm: {userId: 'u1'}};
      dep2 = {department: {departmentCategoryId: 'c2', name: 'name2'}, assignmentTerm: {userId: 'u2'}};
      dep3 = {department: {departmentCategoryId: 'c3', name: 'name3'}, assignmentTerm: {userId: 'u3'}};
      cat1 = {id: 'c1'};
      cat2 = {id: 'c2'};
      user1 = {id: 'u1', username: 'name1'};
      user2 = {id: 'u2', username: 'name2'};

      state = {
        department: {
          departmentList: {list: [dep1, dep2, dep3]},
        },
        departmentCategories: {
          categoryList: {list: [cat1, cat2]}
        },
        users: {memberList: {list: [user1, user2]}}
      };
    });

    it('should map categories and usernames to the departments correctly', () => {
      const result = [{...dep1, category: cat1, username: user1.username},
        {...dep2, category: cat2, username: user2.username},
        {...dep3, category: undefined, username: undefined}];

      expect(departmentListWithCategoryAndUsername(state)).toStrictEqual(result);
    });

    describe('singleDepartmentWithCategoryAndUsername', () => {
      it('should retrieve the correct department', () => {
        expect(singleDepartmentWithCategoryAndUsername('id1')(state)).toStrictEqual({...dep1, category: cat1, username: user1.username});
      });

      it('should return undefined if the list of departments is not available', () => {
        state = {...state, department: {...state.department, departmentList: initialListStore}};

        expect(singleDepartmentWithCategoryAndUsername('id1')(state)).toBeUndefined();
      });
    });

    describe('singleDepartment', () => {
      it('should return a department by its id', () => {
        expect(singleDepartment('id1')(state)).toStrictEqual(dep1.department);
      });

      it('should return undefined if department ist not found', () => {
        expect(singleDepartment('asdf')(state)).toBeUndefined();
      });
    });

    describe('departmentNameList', () => {
      it('should return a list of all department names', () => {
        expect(departmentNameList(state)).toStrictEqual([dep1.department.name, dep2.department.name, dep3.department.name]);
      });
    });
  });


  describe('departmentListCacheInvalid', () => {
    it('should yield true if cacheTime is too far in the past', () => {
      const state = {department: {departmentList: {cacheTime: currentTime() - (environment.cacheTime + 100)}}};

      expect(departmentListCacheInvalid(state)).toBe(true);
    });

    it('should yield false if cache is still valid', () => {
      const state = {department: {departmentList: {cacheTime: currentTime() - (environment.cacheTime - 100)}}};

      expect(departmentListCacheInvalid(state)).toBe(false);
    });
  });

  describe('departmentListIsLoading', () => {
    it('should be true if flag is set', () => {
      const state = {department: {departmentList: {isLoading: true}}};
      expect(departmentListIsLoading(state)).toBe(true);
    });

    it('should be false if flag is not set', () => {
      const state = {department: {departmentList: {isLoading: false}}};
      expect(departmentListIsLoading(state)).toBe(false);
    });
  });

  describe('isSubmittingDepartment', () => {
    it('should return the value of the submit flag for saving a department', () => {
      const state = {department: {saveDepartment: {isSubmitting: 'asdf' as any}}};
      expect(isSubmittingDepartment(state)).toStrictEqual('asdf');
    });
  });
});
