import {createFeatureSelector, createSelector} from '@ngrx/store';
import {isCacheInvalid} from '../../_shared/helper/time-helper';
import {memberList} from '../../users/_store/users.selectors';
import {selectCategoryState} from '../category/_store/category.selectors';
import * as fromDepartment from './department.reducer';

export const selectDepartmentState = createFeatureSelector<fromDepartment.State>(
  fromDepartment.departmentFeatureKey
);

export const departmentNameList = createSelector(selectDepartmentState,
    state => state.departmentList.list?.map(department => department.department.name));

export const departmentListWithCategoryAndUsername = createSelector(selectDepartmentState, selectCategoryState, memberList,
  (depState, categoryState, members) => depState.departmentList.list?.map(departmentWithActiveAssignment => {
    return {
      ...departmentWithActiveAssignment,
      category: categoryState.categoryList.list?.find(
        category => category.id === departmentWithActiveAssignment.department.departmentCategoryId),
      username: members.find(user => user.id === departmentWithActiveAssignment.assignmentTerm?.userId)?.username
    };
  }));

export const singleDepartmentWithCategoryAndUsername = (id: string) => createSelector(
  selectDepartmentState, departmentListWithCategoryAndUsername,
  (state, departmentList) =>
    departmentList?.find(department => department.department?.id === id));

export const singleDepartment = (id: string) => createSelector(
  selectDepartmentState, state => state.departmentList.list?.find(department => department.department.id === id)?.department);

export const departmentListCacheInvalid = createSelector(selectDepartmentState, state => isCacheInvalid(state.departmentList));
export const departmentListIsLoading = createSelector(selectDepartmentState, state => state.departmentList.isLoading);

export const isSubmittingDepartment = createSelector(selectDepartmentState, state => state.saveDepartment.isSubmitting);
