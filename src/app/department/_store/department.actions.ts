import {createAction, props} from '@ngrx/store';
import {Id} from '../../_domain/id';
import {SaveType} from '../../_domain/state/save-state';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {Department} from '../_shared/domain/department';
import {DepartmentWithActiveAssignment} from '../_shared/domain/dtos/department-with-active-assignment';

export const loadDepartmentList = createAction(
  '[Department] Load Department list',
  (reload = false) => ({reload: reload})
);

export const departmentListLoaded = createAction(
  '[Department] Department list loaded',
  props<{ departmentList: DepartmentWithActiveAssignment[] }>()
);

export const departmentListCached = createAction(
  '[Department] Department list cached'
);

export const departmentListLoadError = createAction(
  '[Department] departmentListLoadError',
  props<{ error: RequestError }>()
);

export const invalidateDepartmentListCache = createAction(
  '[Department] invalidateDepartmentListCache'
);

export const loadSingleDepartment = createAction(
  '[Department] loadSingleDepartment',
  props<{ id: string, reload?: boolean }>()
);

export const saveDepartment = createAction(
  '[Department] saveDepartment',
  props<{ department: Department, saveType: SaveType }>()
);

export const departmentSaved = createAction(
  '[Department] departmentSaved',
  props<Id>()
);

export const departmentSaveError = createAction(
  '[Department] departmentSaveError',
  props<{ error: RequestError }>()
);
