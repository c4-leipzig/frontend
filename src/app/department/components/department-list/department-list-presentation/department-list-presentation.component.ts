import {Component, Input, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {DepartmentWithCategoryAndUsername} from '../../../_shared/domain/dtos/department-with-active-assignment';

@Component({
  selector: 'c4-department-list-presentation',
  templateUrl: './department-list-presentation.component.html',
  styleUrls: ['./department-list-presentation.component.scss']
})
export class DepartmentListPresentationComponent {
  @Input() departmentList: DepartmentWithCategoryAndUsername[];

  @Input() set searchTerm(searchTerm: string) {
    this.departmentTableRef?.filterGlobal(searchTerm, 'contains');
  }

  @ViewChild('departmentTable') departmentTableRef: Table;
}
