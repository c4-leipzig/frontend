import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {createButton, labelledCreateButton} from '../../../navigation/_shared/bottom-nav';
import {createSearchConfig} from '../../../navigation/_shared/search-config';
import {setSearchConfig, setTopAndBottomNav} from '../../../navigation/_store/navigation.actions';
import {searchTerm} from '../../../navigation/_store/navigation.selectors';
import {loadDepartmentList} from '../../_store/department.actions';
import {departmentListIsLoading, departmentListWithCategoryAndUsername} from '../../_store/department.selectors';

@Component({
  selector: 'c4-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss']
})
export class DepartmentListComponent implements OnInit {

  departmentList$ = this.store.select(departmentListWithCategoryAndUsername);
  departmentListLoading$ = this.store.select(departmentListIsLoading);
  departmentSearch$ = this.store.select(searchTerm);

  constructor(private store: Store<State>,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.store.dispatch(loadDepartmentList());
    this.store.dispatch(setSearchConfig({searchConfig: createSearchConfig('Ressort suchen')}));
    this.store.dispatch(setTopAndBottomNav({
      topNav: {
        button1: labelledCreateButton(this.navigateNew, 'Ressort erstellen'),
        button2: {
          label: 'Kategorien', iconClass: 'fa fa-th', buttonClass: 'p-button-secondary',
          routerLink: '/departments/categories'
        }
      },
      bottomNav: {button4: createButton(this.navigateNew)}
    }));
  }

  private get navigateNew(): () => void {
    return () => this.router.navigate(['..', 'new'], {relativeTo: this.route});
  }
}
