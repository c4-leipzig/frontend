import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {SaveType} from '../../../../_domain/state/save-state';
import {FormComponent} from '../../../../_shared/guards/leave-form/form-component';
import {State} from '../../../../_store/base.reducer';
import {backButton, backButtonSeverity, ButtonSeverity} from '../../../../navigation/_shared/bottom-nav';
import {setTopAndBottomNav} from '../../../../navigation/_store/navigation.actions';
import {Department} from '../../../_shared/domain/department';
import {loadDepartmentList, saveDepartment} from '../../../_store/department.actions';
import {isSubmittingDepartment, singleDepartment} from '../../../_store/department.selectors';
import {categoryList} from '../../../category/_store/category.selectors';

@Component({
  selector: 'c4-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.scss']
})
export class EditDepartmentComponent implements OnInit, FormComponent {
  id: string;
  department$: Observable<Department>;
  categories$ = this.store.select(categoryList);
  isSubmitting$ = this.store.select(isSubmittingDepartment);
  isDirty = false;

  constructor(private store: Store<State>, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.setNavigation();
    this.store.dispatch(loadDepartmentList());
    this.id = this.route.snapshot.paramMap.get('id');
    this.department$ = this.store.select(singleDepartment(this.id));
  }

  save(department: Department): void {
    this.store.dispatch(saveDepartment({department: department, saveType: SaveType.update}));
  }

  setDirty(): void {
    if (!this.isDirty) {
      this.isDirty = true;
      this.setNavigation();
    }
  }

  private setNavigation(): void {
    this.store.dispatch(setTopAndBottomNav({
      topNav: {button1: {...backButton(this.navigateBack, 'p-button-text'), label: 'Abbrechen'}},
      bottomNav: {button1: backButtonSeverity(this.navigateBack, this.isDirty ? ButtonSeverity.warning : ButtonSeverity.secondary)}
    }));
  }

  private get navigateBack(): () => void {
    return () => this.router.navigate(['..'], {relativeTo: this.route});
  }
}
