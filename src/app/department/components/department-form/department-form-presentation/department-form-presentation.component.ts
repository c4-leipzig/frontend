import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith, take} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {DepartmentValidationService} from '../../../_services/department-validation.service';
import {Department} from '../../../_shared/domain/department';
import {DepartmentCategory} from '../../../_shared/domain/department-category';

@Component({
  selector: 'c4-department-form-presentation',
  templateUrl: './department-form-presentation.component.html',
  styleUrls: ['./department-form-presentation.component.scss']
})
export class DepartmentFormPresentationComponent {
  @Input('department') set departmentSetter(department: Department) {
    if (department) {
      this.department = department;
      this.initializeForm();
    }
  }

  private department: Department;

  @Input() set categories(categoryList: DepartmentCategory[]) {
    this.formCategories = categoryList.map(category => {
      return {label: category.name, value: category.id};
    });
  }

  @Output() save = new EventEmitter<Department>();
  @Output() dirtied = new EventEmitter<void>();

  formCategories: { label: string, value: string }[];
  departmentForm: FormGroup;
  descriptionCharsRemaining$: Observable<number>;
  nameCharsRemaining$: Observable<number>;

  constructor(private fb: FormBuilder, private departmentValidationService: DepartmentValidationService) {
  }

  private initializeForm(): void {
    const descriptionMaxLength = environment.validation.departments.description.maxLength;
    const nameMaxLength = environment.validation.departments.name.maxLength;
    this.departmentForm = this.fb.group({
      name: [this.department.name, [Validators.required, Validators.maxLength(nameMaxLength)],
        [this.departmentValidationService.departmentNameValidator(this.department.name)]],
      departmentCategoryId: [this.department.departmentCategoryId, Validators.required],
      description: [this.department.description, Validators.maxLength(descriptionMaxLength)]
    });

    this.descriptionCharsRemaining$ = this.departmentForm.controls.description.valueChanges
      .pipe(startWith(this.department.description), map(value => descriptionMaxLength - (value || '').length));
    this.nameCharsRemaining$ = this.departmentForm.controls.name.valueChanges
      .pipe(startWith(this.department.name), map(value => nameMaxLength - (value || '').length));

    this.departmentForm.valueChanges.pipe(take(1)).subscribe(() => this.dirtied.next());
  }

  submit(): void {
    this.save.next({
      ...this.department,
      name: this.departmentForm.controls.name.value,
      departmentCategoryId: this.departmentForm.controls.departmentCategoryId.value,
      description: this.departmentForm.controls.description.value
    });
  }
}
