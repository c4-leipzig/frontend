import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {SaveType} from '../../../../_domain/state/save-state';
import {FormComponent} from '../../../../_shared/guards/leave-form/form-component';
import {State} from '../../../../_store/base.reducer';
import {backButtonSeverity, ButtonSeverity, labelledBackButton} from '../../../../navigation/_shared/bottom-nav';
import {setTopAndBottomNav} from '../../../../navigation/_store/navigation.actions';
import {Department, emptyDepartment} from '../../../_shared/domain/department';
import {loadDepartmentList, saveDepartment} from '../../../_store/department.actions';
import {isSubmittingDepartment} from '../../../_store/department.selectors';
import {categoryList} from '../../../category/_store/category.selectors';

@Component({
  selector: 'c4-add-department',
  templateUrl: './add-department.component.html'
})
export class AddDepartmentComponent implements OnInit, FormComponent {
  department = emptyDepartment();
  categories$ = this.store.select(categoryList);
  isSubmitting$ = this.store.select(isSubmittingDepartment);
  isDirty = false;

  constructor(private store: Store<State>, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.store.dispatch(loadDepartmentList());
    this.store.dispatch(setTopAndBottomNav({
      topNav: {button1: labelledBackButton(this.navigateBack)},
      bottomNav: {button1: backButtonSeverity(this.navigateBack, ButtonSeverity.warning)}
    }));
  }

  save(department: Department): void {
    this.store.dispatch(saveDepartment({department: department, saveType: SaveType.create}));
  }

  private get navigateBack(): () => void {
    return () => this.router.navigate(['..'], {relativeTo: this.route});
  }

}
