import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AssignmentTermWithAssignmentAndUsername} from '../../../_shared/domain/dtos/assignment-term-with-assignment';
import {DepartmentWithCategoryAndUsername} from '../../../_shared/domain/dtos/department-with-active-assignment';

@Component({
  selector: 'c4-department-detail-presentation',
  templateUrl: './department-detail-presentation.component.html',
  styleUrls: ['./department-detail-presentation.component.scss']
})
export class DepartmentDetailPresentationComponent {
  @Input() department: DepartmentWithCategoryAndUsername;
  @Input() assignmentTerms: AssignmentTermWithAssignmentAndUsername[];
  @Input() assignmentTermsLoading: boolean;

  @Output() loadHistory = new EventEmitter<void>();

  initiallyCollapsed = window.innerWidth < 576;

  loadHistoryIfNotAvailable(): void {
    if (!this.assignmentTerms) {
      this.loadHistory.emit();
    }
  }
}
