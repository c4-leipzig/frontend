import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {State} from '../../../_store/base.reducer';
import {backButton, editButton, labelledBackButton, labelledEditButton} from '../../../navigation/_shared/bottom-nav';
import {setTopAndBottomNav} from '../../../navigation/_store/navigation.actions';
import {AssignmentTermWithAssignmentAndUsername} from '../../_shared/domain/dtos/assignment-term-with-assignment';
import {DepartmentWithCategoryAndUsername} from '../../_shared/domain/dtos/department-with-active-assignment';
import {loadSingleDepartment} from '../../_store/department.actions';
import {singleDepartmentWithCategoryAndUsername} from '../../_store/department.selectors';
import {loadAssignmentTerms} from '../../assignment-term/_store/assignment-term.actions';
import {
  assignmentTermsForDepartmentLoading,
  assignmentTermsForDepartmentWithUsername
} from '../../assignment-term/_store/assignment-term.selectors';

@Component({
  selector: 'c4-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.scss']
})
export class DepartmentDetailComponent implements OnInit {
  department$: Observable<DepartmentWithCategoryAndUsername>;
  assignmentTermsLoading$: Observable<boolean>;
  assignmentTerms$: Observable<AssignmentTermWithAssignmentAndUsername[]>;
  id: string;
  link = '../edit';

  constructor(private store: Store<State>, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.assignmentTermsLoading$ = this.store.select(assignmentTermsForDepartmentLoading(this.id));
    this.assignmentTerms$ = this.store.select(assignmentTermsForDepartmentWithUsername(this.id));
    this.department$ = this.store.select(singleDepartmentWithCategoryAndUsername(this.id));

    this.store.dispatch(loadSingleDepartment({id: this.id}));
    this.store.dispatch(setTopAndBottomNav({
      topNav: {button1: labelledBackButton(this.navigateBack), button2: labelledEditButton(this.navigateEdit)},
      bottomNav: {button1: backButton(this.navigateBack), button4: editButton(this.navigateEdit)}
    }));
  }

  loadAssignmentTerms(): void {
    this.store.dispatch(loadAssignmentTerms({departmentId: this.id}));
  }

  private get navigateBack(): () => void {
    return () => this.router.navigate(['../..'], {relativeTo: this.route});
  }

  private get navigateEdit(): () => void {
    return () => this.router.navigate(['..', 'edit'], {relativeTo: this.route});
  }
}
