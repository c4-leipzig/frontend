import {TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {DialogService} from 'primeng/dynamicdialog';
import {logInInitialized, logOut} from './_store/auth.actions';
import {initialState} from './_store/auth.reducer';

import {LoginService} from './login.service';

describe('LoginService', () => {
  let service: LoginService;
  let store;

  const matDialogMock = {
    open: () => {
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({initialState: {auth: initialState}}),
        {provide: DialogService, useValue: matDialogMock}
      ]
    });
    service = TestBed.inject(LoginService);
    store = TestBed.inject(MockStore);
  });

  it('should dispatch logInInitialized', () => {
    spyOn(store, 'dispatch').and.callThrough();

    service.startLoginProcess();

    expect(store.dispatch).toHaveBeenCalledWith(logInInitialized());
  });

  it('should dispatch logOut', () => {
    spyOn(store, 'dispatch').and.callThrough();

    service.logout();

    expect(store.dispatch).toHaveBeenCalledWith(logOut());
  });
});
