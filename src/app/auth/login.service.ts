import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {DialogService} from 'primeng/dynamicdialog';
import {State} from '../_store/base.reducer';
import {closeNav} from '../navigation/_store/navigation.actions';
import {logInInitialized, logOut} from './_store/auth.actions';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private dialogService: DialogService, private store: Store<State>) {
  }

  startLoginProcess(): void {
    this.store.dispatch(logInInitialized());
    this.store.dispatch(closeNav());
    this.dialogService.open(LoginDialogComponent, {
      width: '300px',
      header: 'Login'
    });
  }

  logout(): void {
    this.store.dispatch(logOut());
  }
}
