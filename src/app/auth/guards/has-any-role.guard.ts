import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {delayWhen, filter, map, take} from 'rxjs/operators';
import {State} from '../../_store/base.reducer';
import {initializedAfterStartup, loggedInUser} from '../_store/auth.selectors';
import {Role} from '../domain/role.enum';

@Injectable({
  providedIn: 'root'
})
export class HasAnyRoleGuard implements CanActivate {

  user$ = this.store.select(loggedInUser);
  isInitializedAfterStartup$ = this.store.pipe(select(initializedAfterStartup), filter(initialized => initialized));

  constructor(private store: Store<State>) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    const roles = next.data.roles as Role[];

    return this.user$.pipe(
      delayWhen(() => this.isInitializedAfterStartup$),
      map(user => user?.roles.some(role => roles.includes(role))),
      take(1));
  }
}
