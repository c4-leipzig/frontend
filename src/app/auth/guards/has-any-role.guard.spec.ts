import {TestBed} from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {initializedAfterStartup} from '../_store/auth.selectors';

import {HasAnyRoleGuard} from './has-any-role.guard';

describe('HasAnyRoleGuard', () => {
  let guard: HasAnyRoleGuard;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore()
      ]
    });
    guard = TestBed.inject(HasAnyRoleGuard);
    store = TestBed.inject(MockStore);
    store.setState({auth: {user: {roles: ['role1', 'role2']}}});
    store.overrideSelector(initializedAfterStartup, true);
  });

  it('should resolve if user has any of the accepted roles', () => {
    const routeSnapshot: any = {
      data: {
        roles: ['role1', 'role3']
      }
    };
    const routerState: any = {};

    expect(guard.canActivate(routeSnapshot, routerState)).toBeObservable(cold('(a|)', {a: true}));

  });

  it('should not resolve if user does not have an accepted role', () => {
    const routeSnapshot: any = {
      data: {
        roles: ['role3']
      }
    };
    expect(guard.canActivate(routeSnapshot, {} as any)).toBeObservable(cold('(a|)', {a: false}));
  });
});
