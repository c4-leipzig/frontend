import {Injectable} from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {fromUserInfo, User} from './domain/User';

/**
 * Wrapper um den {@link OAuthService} von angular-oauth2-oidc.
 * <br/>
 * Copyright: Copyright (c) 17.08.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private oAuthService: OAuthService) {
  }

  loginWithPassword(username: string, password: string): Observable<User> {
    return from(this.oAuthService.fetchTokenUsingPasswordFlowAndLoadUserProfile(username, password)).pipe(
      map(userInfo => fromUserInfo(userInfo)),
    );
  }

  logOut(): void {
    this.oAuthService.logOut();
  }
}
