import {AuthConfig} from 'angular-oauth2-oidc';
import {environment} from '../../environments/environment';

export const authConfig: AuthConfig = {
  issuer: environment.auth.issuer,
  redirectUri: environment.auth.redirectUri,
  clientId: environment.auth.clientId,
  timeoutFactor: environment.auth.timeoutFactor,
  scope: environment.auth.scope,
  requireHttps: environment.auth.requireHttps,
  responseType: environment.auth.responseType,
  oidc: environment.auth.oidc
};

