import {Role} from './role.enum';
import {fromUserInfo} from './User';

describe('fromUserInfo', () => {
  it('should map pa userInfo to a valid user', () => {
    const userInfo = {sub: 'id', preferred_username: 'username', realm_roles: ['MEMBER']};

    const user = fromUserInfo(userInfo);

    expect(user.id).toBe('id');
    expect(user.username).toBe('username');
    expect(user.roles).toStrictEqual([Role.MEMBER]);
  });
});
