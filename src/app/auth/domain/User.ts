import {UserInfo} from 'angular-oauth2-oidc';
import {Role} from './role.enum';

export interface User {
  id: string;
  username: string;
  roles: Role[];
}

export function fromUserInfo(userInfo: UserInfo): User {
  return {
    id: userInfo.sub,
    username: userInfo.preferred_username,
    roles: (userInfo.realm_roles as string[]).map(role => Role[role]).filter(role => role)
  };
}
