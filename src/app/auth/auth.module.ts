import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {EffectsModule} from '@ngrx/effects';
import {Store, StoreModule} from '@ngrx/store';
import {OAuthService} from 'angular-oauth2-oidc';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';
import {SharedModule} from '../_shared/shared.module';
import {State} from '../_store/base.reducer';
import {userLoggedInOnStartup, userNotLoggedInOnStartup} from './_store/auth.actions';
import {AuthEffects} from './_store/auth.effects';
import * as fromAuth from './_store/auth.reducer';
import {authConfig} from './auth-config';
import {fromUserInfo} from './domain/User';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';


@NgModule({
  declarations: [LoginDialogComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffects]),
    ReactiveFormsModule,
    DynamicDialogModule,
    ButtonModule,
    InputTextModule,
    SharedModule
  ],
  entryComponents: [
    LoginDialogComponent
  ],
  providers: [
    DialogService
  ]
})
export class AuthModule {
  constructor(private oAuthService: OAuthService,
              private store: Store<State>) {
    this.oAuthService.configure(authConfig);
    this.oAuthService.setupAutomaticSilentRefresh();
    this.oAuthService.loadDiscoveryDocument().then(() => {
      if (this.oAuthService.hasValidAccessToken()) {
        this.oAuthService.loadUserProfile().then(userInfo => store.dispatch(userLoggedInOnStartup({user: fromUserInfo(userInfo)})));
      } else {
        this.store.dispatch(userNotLoggedInOnStartup());
      }
    });
  }
}
