import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {State} from '../../_store/base.reducer';
import {logInSent} from '../_store/auth.actions';
import {isLoggedIn, isLoggingIn, logInFailed} from '../_store/auth.selectors';

@Component({
  selector: 'c4-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit, OnDestroy {
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  isLoggingIn$ = this.store.select(isLoggingIn);
  logInFailed$ = this.store.select(logInFailed);

  private destroy$ = new Subject();

  constructor(private dynamicDialogRef: DynamicDialogRef,
              private fb: FormBuilder,
              private store: Store<State>) {
  }

  ngOnInit(): void {
    this.store.pipe(takeUntil(this.destroy$), select(isLoggedIn), filter(b => b)).subscribe(() => this.dynamicDialogRef.close());
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  tryLogin(): void {
    this.store.dispatch(logInSent({username: this.loginForm.get('username').value, password: this.loginForm.get('password').value}));
  }

  abort(): void {
    this.dynamicDialogRef.destroy();
  }
}
