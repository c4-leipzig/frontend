import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {logInSent} from '../_store/auth.actions';
import {initialState} from '../_store/auth.reducer';

import {LoginDialogComponent} from './login-dialog.component';

describe('LoginDialogComponent', () => {
  let component: LoginDialogComponent;
  let fixture: ComponentFixture<LoginDialogComponent>;
  let store;

  const dialogMock = {
    close: jest.fn(),
    destroy: jest.fn()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        FormBuilder,
        {provide: DynamicDialogRef, useValue: dialogMock},
        provideMockStore({initialState: {auth: initialState}})
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    store = TestBed.inject(MockStore);
  }));

  it('should close itself on abort', () => {
    component.abort();
    expect(dialogMock.destroy).toHaveBeenCalledTimes(1);
  });

  it('should dispatch LogInSent', () => {
    spyOn(store, 'dispatch').and.callThrough();

    component.tryLogin();
    expect(store.dispatch).toHaveBeenCalledWith(logInSent({username: '', password: ''}));
  });

  it('should close itself when the login was successful', () => {
    store.setState({auth: {...initialState, isLoggedIn: true}});

    expect(dialogMock.close).toHaveBeenCalledTimes(1);
  });
});
