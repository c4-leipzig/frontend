import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {EMPTY, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {closeNav} from '../../navigation/_store/navigation.actions';
import {AuthService} from '../auth.service';
import {logInFailure, logInSent, logInSuccess, logOut} from './auth.actions';


@Injectable()
export class AuthEffects {

  constructor(private actions$: Actions,
              private authService: AuthService,
              private router: Router) {
  }

  @Effect()
  tryLogin$ = this.actions$.pipe(
    ofType(logInSent),
    switchMap(action => {
      return this.authService.loginWithPassword(action.username, action.password).pipe(
        map(user => logInSuccess({user: user})),
        catchError(() => of(logInFailure()))
      );
    })
  );

  @Effect()
  logout$ = this.actions$.pipe(
    ofType(logOut),
    switchMap(() => {
      this.authService.logOut();
      this.router.navigateByUrl('');
      return of(closeNav());
    })
  );
}
