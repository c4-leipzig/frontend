import {createReducer, on} from '@ngrx/store';
import {User} from '../domain/User';
import * as AuthActions from './auth.actions';

export const authFeatureKey = 'auth';

export interface State {
  isLoggedIn: boolean;
  isLoggingIn: boolean;
  logInFailed: boolean;
  user: User;
  initializedAfterStartup: boolean;
}

export const initialState: State = {
  isLoggedIn: false,
  isLoggingIn: false,
  logInFailed: false,
  user: null,
  initializedAfterStartup: true
};

export const initialStateOnStartUp: State = {
  ...initialState,
  initializedAfterStartup: false
};

export const reducer = createReducer(
  initialStateOnStartUp,
  on(AuthActions.logInInitialized, () => initialState),
  on(AuthActions.logInSent, state => {
    return {...state, isLoggingIn: true, logInFailed: false};
  }),
  on(AuthActions.logInSuccess, (state, props) => {
    return {...state, isLoggedIn: true, user: props.user, isLoggingIn: false, logInFailed: false};
  }),
  on(AuthActions.logInFailure, state => {
    return {...state, isLoggingIn: false, logInFailed: true};
  }),
  on(AuthActions.logOut, () => initialState),
  on(AuthActions.userLoggedInOnStartup, (state, props) => {
    return {...state, isLoggedIn: true, user: props.user, isLoggingIn: false, logInFailed: false, initializedAfterStartup: true};
  }),
  on(AuthActions.userNotLoggedInOnStartup, () => initialState)
);

