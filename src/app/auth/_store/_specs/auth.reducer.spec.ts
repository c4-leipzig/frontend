import {
  logInFailure,
  logInInitialized,
  logInSent,
  logInSuccess,
  logOut,
  userLoggedInOnStartup,
  userNotLoggedInOnStartup
} from '../auth.actions';
import {initialState, initialStateOnStartUp, reducer} from '../auth.reducer';

describe('Auth Reducer', () => {
  describe('login initialized', () => {
    it('should reset the state', () => {
      const action = logInInitialized();
      const result = reducer({isLoggingIn: true, logInFailed: true, user: null, isLoggedIn: false, initializedAfterStartup: true}, action);

      expect(result).toBe(initialState);
    });
  });

  describe('login sent', () => {
    it('should set sending flag', () => {
      const action = logInSent({username: 'username', password: 'password'});
      const result = reducer({...initialState, logInFailed: true}, action);

      expect(result.isLoggingIn).toBe(true);
      expect(result.logInFailed).toBe(false);
    });
  });

  describe('login success', () => {
    it('should set the user', () => {
      const user = {id: 'id', username: 'username', roles: []};
      const action = logInSuccess({user: user});
      const result = reducer(initialState, action);

      expect(result.user).toBe(user);
      expect(result.isLoggedIn).toBe(true);
      expect(result.isLoggingIn).toBe(false);
      expect(result.logInFailed).toBe(false);
    });
  });

  describe('login failure', () => {
    it('should set failure flag', () => {
      const action = logInFailure();
      const result = reducer({...initialState, isLoggingIn: true}, action);

      expect(result.logInFailed).toBe(true);
      expect(result.isLoggingIn).toBe(false);
    });
  });

  describe('logout', () => {
    it('should reset the state', () => {
      const action = logOut();
      const result = reducer({...initialState, isLoggedIn: true, user: {username: 'username', id: 'id', roles: []}}, action);

      expect(result.user).toBeNull();
      expect(result.isLoggedIn).toBe(false);
    });
  });

  describe('userLoggedInOnStartup', () => {
    it('should set the user and initialization-flag', () => {
      const user: any = {id: 'id'};
      const result = reducer(initialStateOnStartUp, userLoggedInOnStartup({user: user}));

      expect(result).toStrictEqual({...initialStateOnStartUp, user: user, isLoggedIn: true, initializedAfterStartup: true});
    });
  });

  describe('userNotLoggedInOnStartUp', () => {
    it('should set the initialization-flag', () => {
      const result = reducer(initialStateOnStartUp, userNotLoggedInOnStartup());

      expect(result).toStrictEqual({...initialStateOnStartUp, initializedAfterStartup: true});
    });
  });
});
