import {TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {cold, hot} from 'jest-marbles';
import {Observable, of, throwError} from 'rxjs';
import {AuthMock} from '../../../../testing/mocks';
import {AuthService} from '../../auth.service';
import {logInFailure, logInSent, logInSuccess, logOut} from '../auth.actions';

import {AuthEffects} from '../auth.effects';

describe('AuthEffects', () => {
  let actions$: Observable<any>;
  let effects: AuthEffects;

  const authMock = new AuthMock();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthEffects,
        {
          provide: AuthService,
          useValue: authMock
        },
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(AuthEffects);
  });

  it('should send login request when LogInSent action is received and trigger LogInSuccess on completion', () => {
    const user = {id: 'id', username: 'username', roles: []};
    spyOn(authMock, 'loginWithPassword').and.returnValue(of(user));

    actions$ = hot('--a-', {a: logInSent({username: 'user', password: 'pw'})});
    const expected = cold('--a-', {a: logInSuccess({user: user})});

    expect(effects.tryLogin$).toBeObservable(expected);
  });

  it('should send login request when LogInSent action is received and trigger LogInFailure', () => {
    spyOn(authMock, 'loginWithPassword').and.returnValue(throwError('error'));

    actions$ = hot('--a-', {a: logInSent({username: 'user', password: 'pw'})});
    const expected = cold('--b', {b: logInFailure()});

    expect(effects.tryLogin$).toBeObservable(expected);
  });

  it('should logout the user upon receiving LogOut', () => {
    spyOn(authMock, 'logOut').and.callThrough();

    actions$ = of(logOut());

    effects.logout$.subscribe();
    expect(authMock.logOut).toHaveBeenCalledTimes(1);
  });
});
