import {initializedAfterStartup, isLoggedIn, isLoggingIn, loggedInUser, loggedInUserName, logInFailed} from '../auth.selectors';

describe('AuthSelectors', () => {
  describe('loggedInUser', () => {
    it('should yield the currently logged in user', () => {
      const state = {auth: {user: {a: 1}}};

      expect(loggedInUser(state)).toStrictEqual({a: 1});
    });
  });

  describe('loggedInUserName', () => {
    it('should yield the name of the currently logged in user', () => {
      const state = {auth: {user: {username: 'username'}}};

      expect(loggedInUserName(state)).toStrictEqual('username');
    });

    it('should yield undefined if no user is logged in', () => {
      const state = {auth: {user: undefined}};

      expect(loggedInUserName(state)).toBeUndefined();
    });
  });

  describe('isLoggedIn', () => {
    it('should yield the isLoggedIn flag', () => {
      let state = {auth: {isLoggedIn: false}};
      expect(isLoggedIn(state)).toBe(false);
      state = {auth: {isLoggedIn: true}};
      expect(isLoggedIn(state)).toBe(true);
    });
  });

  describe('isLoggingIn', () => {
    it('should yield the isLoggingIn flag', () => {
      let state = {auth: {isLoggingIn: false}};
      expect(isLoggingIn(state)).toBe(false);
      state = {auth: {isLoggingIn: true}};
      expect(isLoggingIn(state)).toBe(true);
    });
  });

  describe('logInFailed', () => {
    it('should yield the logInFailed flag', () => {
      let state = {auth: {logInFailed: false}};
      expect(logInFailed(state)).toBe(false);
      state = {auth: {logInFailed: true}};
      expect(logInFailed(state)).toBe(true);
    });
  });

  describe('initializedAfterStartup', () => {
    it('should yield the initializedAfterStartup flag', () => {
      let state = {auth: {initializedAfterStartup: false}};
      expect(initializedAfterStartup(state)).toBe(false);
      state = {auth: {initializedAfterStartup: true}};
      expect(initializedAfterStartup(state)).toBe(true);
    });
  });
});
