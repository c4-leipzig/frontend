import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from './auth.reducer';

export const selectAuthState = createFeatureSelector<fromAuth.State>(
  fromAuth.authFeatureKey
);

export const loggedInUser = createSelector(selectAuthState, state => state.user);
export const loggedInUserName = createSelector(selectAuthState, state => state.user?.username);
export const isLoggedIn = createSelector(selectAuthState, state => state.isLoggedIn);
export const isLoggingIn = createSelector(selectAuthState, state => state.isLoggingIn);
export const logInFailed = createSelector(selectAuthState, state => state.logInFailed);
export const initializedAfterStartup = createSelector(selectAuthState, state => state.initializedAfterStartup);
