import { createAction, props } from '@ngrx/store';
import {User} from '../domain/User';

export const logInInitialized = createAction(
  '[Auth] Login initialized'
);

export const logInSent = createAction(
  '[Auth] Login sent',
  props<{username: string, password: string}>()
);

export const logInSuccess = createAction(
  '[Auth] Logged in successfully',
  props<{user: User}>()
);

export const logInFailure = createAction(
  '[Auth] Login failed'
);

export const logOut = createAction(
  '[Auth] Logout'
);

export const userLoggedInOnStartup = createAction(
  '[Auth] User logged in on startup',
  props<{user: User}>()
);

export const userNotLoggedInOnStartup = createAction(
  '[Auth] User not logged in on startup'
);
