import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {OAuthModule, OAuthService} from 'angular-oauth2-oidc';
import {OAuthMock} from '../../testing/mocks';

import {AuthService} from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  const oAuthMock = new OAuthMock();

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [OAuthModule.forRoot()],
      providers: [
        {provide: OAuthService, useValue: oAuthMock},
        AuthService
      ]
    });
    service = TestBed.inject(AuthService);
  });

  it('should call underlying service upon login', () => {
    spyOn(oAuthMock, 'fetchTokenUsingPasswordFlowAndLoadUserProfile').and.returnValue(Promise.resolve({
      sub: 'id',
      preferred_username: 'username'
    }));

    service.loginWithPassword('user', 'pw');

    expect(oAuthMock.fetchTokenUsingPasswordFlowAndLoadUserProfile).toHaveBeenCalledWith('user', 'pw');
  });

  it('should call underlying service upon logout', () => {
    spyOn(oAuthMock, 'logOut').and.callThrough();
    service.logOut();

    expect(oAuthMock.logOut).toHaveBeenCalledTimes(1);
  });
});
