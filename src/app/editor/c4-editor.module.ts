import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {EditorModule} from 'primeng/editor';
import {EditorComponent} from './editor/editor.component';

@NgModule({
  declarations: [EditorComponent],
  imports: [
    CommonModule,
    EditorModule
  ],
  exports: [
    EditorComponent
  ]
})
export class C4EditorModule {
}
