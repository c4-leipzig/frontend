import {Component, ElementRef, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Editor} from 'primeng/editor';

export const EDITOR_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => EditorComponent),
  multi: true
};

/**
 * Wrapper um den von PrimeNG zur Verfügung gestellten {@link Editor}.
 * Es wird die standardmäßig bereitgestellte Toolbar überschrieben, sodass diese nicht manuell in den Components angegeben werden muss.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Component({
  selector: 'c4-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  providers: [EDITOR_VALUE_ACCESSOR]
})
export class EditorComponent extends Editor implements ControlValueAccessor {

  constructor(el: ElementRef) {
    super(el);
  }
}
