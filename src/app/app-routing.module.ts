import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {Role} from './auth/domain/role.enum';
import {HasAnyRoleGuard} from './auth/guards/has-any-role.guard';

export const routes: Routes = [
  {
    path: 'departments',
    loadChildren: () => import('./department/department.module').then(m => m.DepartmentModule),
    canActivate: [HasAnyRoleGuard],
    data: {roles: [Role.DEPARTMENT_ADMIN]}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
