import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import {LoadMemberListService} from './_shared/load-member-list.service';
import * as fromUsers from './_store/users.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './_store/users.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromUsers.usersFeatureKey, fromUsers.reducer),
    EffectsModule.forFeature([UsersEffects])
  ],
  providers: [
    LoadMemberListService
  ]
})
export class UsersModule { }
