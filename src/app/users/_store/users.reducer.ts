import {createReducer, on} from '@ngrx/store';
import {initialListStore, ListState} from '../../_domain/state/list-state';
import {Member} from '../_shared/member';
import * as UsersActions from './users.actions';
import dayjs from 'dayjs';

export const usersFeatureKey = 'users';

export interface State {
  memberList: ListState<Member>;
}

export const initialState: State = {
  memberList: initialListStore
};


export const reducer = createReducer(
  initialState,

  on(UsersActions.loadMemberList, state => {
    return {...state, memberList: {...state.memberList, isLoading: true}};
  }),
  on(UsersActions.memberListLoaded, (state, update) => {
    return {...state, memberList: {...state.memberList, isLoading: false, cacheTime: dayjs().valueOf(), list: update.memberList}};
  }),
  on(UsersActions.memberListCached, state => {
    return {...state, memberList: {...state.memberList, isLoading: false}};
  }),
  on(UsersActions.memberListLoadError, (state, update) => {
    return {...state, memberList: {...state.memberList, isLoading: false, loadError: update.error}};
  })
);
