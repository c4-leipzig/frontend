import { createFeatureSelector, createSelector } from '@ngrx/store';
import {isCacheInvalid} from '../../_shared/helper/time-helper';
import * as fromUsers from './users.reducer';

export const selectUsersState = createFeatureSelector<fromUsers.State>(
  fromUsers.usersFeatureKey
);

export const memberListCacheInvalid = createSelector(selectUsersState, state => isCacheInvalid(state.memberList));
export const memberList = createSelector(selectUsersState, state => state?.memberList?.list || []);
