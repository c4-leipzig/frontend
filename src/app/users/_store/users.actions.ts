import {createAction, props} from '@ngrx/store';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {Member} from '../_shared/member';

export const loadMemberList = createAction(
  '[Users] loadMembers',
  (reload = false) => ({reload: reload})
);

export const memberListLoaded = createAction(
  '[Users] membersLoaded',
  props<{ memberList: Member[] }>()
);

export const memberListCached = createAction(
  '[Users] membersCached'
);

export const memberListLoadError = createAction(
  '[Users] memberListLoadError',
  props<{ error: RequestError }>()
);
