import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import {switchMap} from 'rxjs/operators';
import {LoadMemberListService} from '../_shared/load-member-list.service';
import * as UsersActions from './users.actions';


@Injectable()
export class UsersEffects {
  loadMemberList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UsersActions.loadMemberList),
      switchMap(action => this.loadMemberListService.get(action.reload))
    );
  }, {dispatch: true});

  constructor(private actions$: Actions,
              private loadMemberListService: LoadMemberListService) {
  }

}
