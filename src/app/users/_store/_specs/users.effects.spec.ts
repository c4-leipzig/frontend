import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold, hot} from 'jest-marbles';
import {Observable, of} from 'rxjs';
import {LoadMemberListService} from '../../_shared/load-member-list.service';
import {loadMemberList, memberListLoaded} from '../users.actions';
import {UsersEffects} from '../users.effects';
import {initialState} from '../users.reducer';

describe('UsersEffects', () => {
  let actions$: Observable<any>;
  let effects: UsersEffects;
  let mockStore: MockStore;
  const list: any[] = [1, 2, 3];
  let mockLoadMemberListService: LoadMemberListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UsersEffects,
        provideMockActions(() => actions$),
        provideMockStore({initialState: {users: initialState}}),
        {
          provide: LoadMemberListService,
          useValue: {get: jest.fn()}
        }
      ]
    });

    effects = TestBed.inject(UsersEffects);
    mockStore = TestBed.inject(MockStore);
    mockLoadMemberListService = TestBed.inject(LoadMemberListService);
  });

  describe('loadMemberList', () => {
    it('should load members', () => {
      actions$ = hot('--a', {a: loadMemberList()});
      spyOn(mockLoadMemberListService, 'get').and.returnValue(of(memberListLoaded({memberList: list})));
      expect(effects.loadMemberList$).toBeObservable(cold('--a', {a: memberListLoaded({memberList: list})}));
    });
  });
});
