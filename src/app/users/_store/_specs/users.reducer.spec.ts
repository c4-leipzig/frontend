import {RequestErrorType} from '../../../_shared/http-error-handling/http-error-type.enum';
import {initialListStore} from '../../../_domain/state/list-state';
import {loadMemberList, memberListCached, memberListLoaded, memberListLoadError} from '../users.actions';
import {initialState, reducer} from '../users.reducer';

describe('UsersReducer', () => {
  describe('loadMemberList', () => {
    it('should set loading flag', () => {
      const state = {memberList: initialListStore};

      expect(reducer(state, loadMemberList())).toStrictEqual({...state, memberList: {...state.memberList, isLoading: true}});
    });
  });

  describe('memberListCached', () => {
    it('should reset loading flag', () => {
      const state = {memberList: {...initialListStore, isLoading: true}};

      expect(reducer(state, memberListCached())).toStrictEqual({...state, memberList: {...state.memberList, isLoading: false}});
    });
  });

  describe('memberListLoaded', () => {
    it('should load list, reset loading flag and cache', () => {
      const state = {memberList: {...initialListStore, isLoading: true}};
      const list: any[] = [1, 2, 3];

      const result = reducer(state, memberListLoaded({memberList: list}));

      expect(result.memberList.isLoading).toBe(false);
      expect(result.memberList.list).toBe(list);
      expect(result.memberList.cacheTime).toBeGreaterThanOrEqual(new Date().getDate());
    });
  });

  describe('memberListLoadError', () => {
    it('should save error and reset loading flag', () => {
      const initial = {...initialState, memberList: {...initialState.memberList, isLoading: true}};
      const error = {type: RequestErrorType.user};
      const result = reducer(initial, memberListLoadError({error: error}));

      expect(result.memberList.isLoading).toBe(false);
      expect(result.memberList.loadError).toBe(error);
      expect(result.memberList.cacheTime).toBe(0);
    });
  });
});

