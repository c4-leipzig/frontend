import {environment} from '../../../../environments/environment';
import {currentTime} from '../../../_shared/helper/time-helper';
import * as fromUsers from '../users.reducer';
import {memberList, memberListCacheInvalid, selectUsersState} from '../users.selectors';

describe('UsersSelectors', () => {
  it('should select the feature state', () => {
    const result = selectUsersState({
      [fromUsers.usersFeatureKey]: {a: 1}
    });

    expect(result).toEqual({a: 1});
  });

  describe('memberListCacheInvalid', () => {
    it('should yield true if cacheTime is too old', () => {
      const state = {users: {memberList: {cacheTime: currentTime() - (environment.cacheTime + 100)}}};

      expect(memberListCacheInvalid(state)).toBe(true);
    });

    it('should yield false if cache is still valid', () => {
      const state = {users: {memberList: {cacheTime: currentTime() - (environment.cacheTime - 100)}}};

      expect(memberListCacheInvalid(state)).toBe(false);
    });
  });

  describe('memberList', () => {
    it('should return the list of members if available', () => {
      const list: any[] = [1, 2, 3];
      const state = {users: {memberList: {list: list}}};

      expect(memberList(state)).toBe(list);
    });

    it('should return an empty list if no members are available', () => {
      const state = {users: {memberList: {}}};

      expect(memberList(state)).toStrictEqual([]);
    });
  });
});
