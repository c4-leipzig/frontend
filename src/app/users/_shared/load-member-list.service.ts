import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {environment} from '../../../environments/environment';
import {RequestError} from '../../_shared/http-error-handling/request-error';
import {CacheableToActionLoader} from '../../_store/cacheable/cacheable-to-action-loader';
import {memberListCached, memberListLoaded, memberListLoadError} from '../_store/users.actions';
import {State} from '../_store/users.reducer';
import {memberListCacheInvalid} from '../_store/users.selectors';
import {Member} from './member';

@Injectable()
export class LoadMemberListService extends CacheableToActionLoader<Member[]> {


  constructor(http: HttpClient, store: Store<State>) {
    super(http, () => store.select(memberListCacheInvalid));
  }

  cachedAction = memberListCached();
  endpoint = environment.endpoints.users.members;

  errorHandler = (error: RequestError) => of(memberListLoadError({error: error}));

  projection = members => memberListLoaded({memberList: members});

}
