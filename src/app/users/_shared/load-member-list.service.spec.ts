import {HttpClientTestingModule} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {cold} from 'jest-marbles';
import {environment} from '../../../environments/environment';
import {RequestErrorType} from '../../_shared/http-error-handling/http-error-type.enum';
import {memberListCached, memberListLoaded, memberListLoadError} from '../_store/users.actions';
import {memberListCacheInvalid} from '../_store/users.selectors';

import { LoadMemberListService } from './load-member-list.service';

describe('LoadMemberListService', () => {
  let service: LoadMemberListService;
  let mockStore: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore(),
        LoadMemberListService
      ]
    });
    service = TestBed.inject(LoadMemberListService);
    mockStore = TestBed.inject(MockStore);
  });

  it('should use the correct endpoint', () => {
    expect(service.endpoint).toStrictEqual(environment.endpoints.users.members);
  });

  it('should select its cacheValidity correctly', () => {
    mockStore.overrideSelector(memberListCacheInvalid, 'test' as any);

    expect(service.cacheInvalidProvider()).toBeObservable(cold('(a)', {a: 'test'}));
  });

  it('should use the correct cachedAction', () => {
    expect(service.cachedAction).toStrictEqual(memberListCached());
  });

  it('should map errors to the correct action', () => {
    const error = {type: RequestErrorType.user};

    expect(service.errorHandler(error)).toBeObservable(cold('(a|)', {a: memberListLoadError({error: error})}));
  });

  it('should map the loaded result to the correct action', () => {
    const res: any[] = [1, 2, 3];
    expect(service.projection(res)).toStrictEqual(memberListLoaded({memberList: res}));
  });
});
