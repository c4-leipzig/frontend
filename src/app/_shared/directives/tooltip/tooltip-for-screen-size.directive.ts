import {Directive, ElementRef, Input, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {BehaviorSubject, combineLatest, Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {State} from '../../../_store/base.reducer';
import {displayWidth} from '../../../navigation/_store/navigation.selectors';
import {TooltipDirective} from './tooltip.directive';

/**
 * Direktive erbt von {@link TooltipDirective} und zeigt den Tooltip nur an, wenn die Screenwidht innerhalb der definierten Grenzen liegt.
 * @param minSize Minimale Screenwidth, für die der Tooltip angezeigt wird (inklusive)
 * @param maxSize Maximale Screenwidth, für die der Tooltip angezeigt wird (exklusive)
 * @param c4TooltipForScreenSize Der Wert, den der Tooltip anzeigen soll
 * <br/>
 * Copyright: Copyright (c) 04.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Directive({
  selector: '[c4TooltipForScreenSize]'
})
export class TooltipForScreenSizeDirective extends TooltipDirective implements OnInit, OnDestroy {
  @Input('c4TooltipForScreenSize') set tooltipForScreenSize(tooltip: string) {
    this.tooltipValue$.next(tooltip);
  }

  @Input() minSize = 0;
  @Input() maxSize = Number.MAX_SAFE_INTEGER;

  destroy$ = new Subject();
  tooltipValue$ = new BehaviorSubject<string>('');

  constructor(el: ElementRef, zone: NgZone, private store: Store<State>) {
    super(el, zone);
  }

  ngOnInit(): void {
    combineLatest([
      this.tooltipValue$.asObservable(),
      this.store.select(displayWidth)
        .pipe(map(width => width >= this.minSize && width < this.maxSize), distinctUntilChanged())
    ]).pipe(takeUntil(this.destroy$)).subscribe(([value, display]) => this.tooltip = display ? value : '');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    super.ngOnDestroy();
  }
}
