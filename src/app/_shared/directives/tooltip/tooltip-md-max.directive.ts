import {Directive, ElementRef, Input, NgZone} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {TooltipForScreenSizeDirective} from './tooltip-for-screen-size.directive';

@Directive({
  selector: '[c4TooltipMdMax]'
})
export class TooltipMdMaxDirective extends TooltipForScreenSizeDirective {
  @Input('c4TooltipMdMax') set tooltipMdMax(tooltip: string) {
    this.tooltipForScreenSize = tooltip;
  }

  constructor(el: ElementRef, zone: NgZone, store: Store<State>) {
    super(el, zone, store);
    this.maxSize = 992;
  }
}
