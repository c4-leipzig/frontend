import {Directive, ElementRef, Input, NgZone} from '@angular/core';
import {Tooltip} from 'primeng/tooltip';

/**
 * Extended den PrimeNG-Tooltip um Standardwerte zu setzen.
 * <br/>
 * Copyright: Copyright (c) 04.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Directive({
  selector: '[c4Tooltip]'
})
export class TooltipDirective extends Tooltip {
  @Input('c4Tooltip') set tooltip(tooltip: string) {
    this.text = tooltip;
  }

  constructor(el: ElementRef, zone: NgZone) {
    super(el, zone);
    super.showDelay = 200;
    super.hideDelay = 100;
    super.tooltipPosition = 'bottom';
  }
}
