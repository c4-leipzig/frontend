import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {ShowForScreenWidthDirective} from './show-for-screen-width.directive';

@Directive({
  selector: '[c4ShowForMobile]'
})
export class ShowForMobileDirective extends ShowForScreenWidthDirective {
  public static readonly minWidth = 0;
  public static readonly maxWidth = 576;

  constructor(store: Store<State>, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    super(ShowForMobileDirective.minWidth, ShowForMobileDirective.maxWidth, store, templateRef, viewContainerRef);
  }
}
