import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../_store/base.reducer';
import {ShowForScreenWidthDirective} from './show-for-screen-width.directive';

@Directive({
  selector: '[c4HideForMobile]'
})
export class HideForMobileDirective extends ShowForScreenWidthDirective {
  public static readonly minWidth = 576;
  public static readonly maxWidth = 10000;

  constructor(store: Store<State>, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    super(HideForMobileDirective.minWidth, HideForMobileDirective.maxWidth, store, templateRef, viewContainerRef);
  }
}
