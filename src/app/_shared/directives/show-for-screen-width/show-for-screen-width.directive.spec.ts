import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {async, TestBed} from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {State} from '../../../_store/base.reducer';
import {ShowForScreenWidthDirective} from './show-for-screen-width.directive';


@Directive({selector: '[c4TestSelector]'})
// tslint:disable-next-line:directive-class-suffix
class ShowForScreenWidthDirectiveImpl extends ShowForScreenWidthDirective {
  public static readonly minWidth = 100;
  public static readonly maxWidth = 200;

  constructor(store: Store<State>, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    super(ShowForScreenWidthDirectiveImpl.minWidth, ShowForScreenWidthDirectiveImpl.maxWidth, store, templateRef, viewContainerRef);
  }
}

describe('ShowForScreenWidthDirective', () => {
  let mockStore: MockStore;
  let viewContainerRef: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowForScreenWidthDirectiveImpl],
      providers: [
        provideMockStore()
      ]
    });

    mockStore = TestBed.inject(MockStore);
    viewContainerRef = {clear: jest.fn(), createEmbeddedView: jest.fn()};
  }));

  it('should not render initially if width is outside of boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 50}}});
    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);

    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(0);

  });

  it('should render initially if width is within boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 150}}});
    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);

    expect(viewContainerRef.clear).toHaveBeenCalledTimes(0);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);
  });

  it('should not be triggered if value stays within boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 150}}});

    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);

    mockStore.setState({navigation: {displaySize: {width: 160}}});
    mockStore.refreshState();
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(0);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);
  });

  it('should not be triggered if value stays out of boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 50}}});

    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);

    mockStore.setState({navigation: {displaySize: {width: 60}}});
    mockStore.refreshState();
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(0);
  });

  it('should clear if width gets out of boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 150}}});

    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(0);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);

    mockStore.setState({navigation: {displaySize: {width: 50}}});
    mockStore.refreshState();
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);
  });

  it('should render if width gets within boundaries', () => {
    mockStore.setState({navigation: {displaySize: {width: 50}}});

    const directive = new ShowForScreenWidthDirectiveImpl(mockStore as any, {} as any, viewContainerRef);
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(0);


    mockStore.setState({navigation: {displaySize: {width: 150}}});
    mockStore.refreshState();
    expect(viewContainerRef.clear).toHaveBeenCalledTimes(1);
    expect(viewContainerRef.createEmbeddedView).toHaveBeenCalledTimes(1);
  });
});

