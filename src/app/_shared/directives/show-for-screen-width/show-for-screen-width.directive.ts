import {Directive, OnDestroy, TemplateRef, ViewContainerRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {State} from '../../../_store/base.reducer';
import {displayWidth} from '../../../navigation/_store/navigation.selectors';

/**
 * Abstrakte Baseklasse für Strukturdirektiven, die ein Element nur für bestimmte ScreenWidths anzeigen soll.
 * Angegeben werden müssen:
 * @param minWidth Minimale Breite, für die das Element angezeigt wird (inklusive)
 * @param maxWidth Maximale Breite, für die das Element angezeigt wird (exklusive)
 * <br/>
 * Copyright: Copyright (c) 26.09.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Directive()
export abstract class ShowForScreenWidthDirective implements OnDestroy {
  protected destroy$ = new Subject();

  protected constructor(minWidth: number, maxWidth: number,
                        store: Store<State>, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    store.select(displayWidth)
      .pipe(map(width => {
        return width >= minWidth && width < maxWidth;
      }), distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe(show => {
        if (show) {
          viewContainerRef.createEmbeddedView(templateRef);
        } else {
          viewContainerRef.clear();
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
