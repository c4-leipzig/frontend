import {AfterViewInit, Directive, ElementRef, Input, Renderer2} from '@angular/core';

/**
 * Directive zur Anzeige der verbleibenden Zeichen eines Eingabefeldes. Bei Überschreitung wird eine enstprechende Meldung angezeigt.
 * Eine Validierung findet _nicht_ statt, d.h. es muss nach wie vor ein enstprechender Validator gesetzt werden.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Directive({
  selector: '[c4CharsRemaining]'
})
export class CharsRemainingDirective implements AfterViewInit {
  @Input('c4CharsRemaining') set charsLeft(remainingChars: number) {
    this.remainingChars = remainingChars;
    this.update();
  }

  private small: HTMLDivElement;
  private remainingChars: number;

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  ngAfterViewInit(): void {
    this.small = this.renderer.createElement('small');
    this.renderer.appendChild(this.el.nativeElement.parentNode, this.small);
    this.update();
  }

  private update(): void {
    if (this.small) {
      this.setText();
      this.setInvalidClass();
    }
  }

  private setText(): void {
    if (this.remainingChars > 0) {
      this.renderer.setProperty(this.small, 'innerText', `Noch ${this.remainingChars} Zeichen.`);
    } else {
      this.renderer.setProperty(this.small, 'innerText', `Maximallänge erreicht.`);
    }
  }

  private setInvalidClass(): void {
    if (this.remainingChars < 0) {
      this.renderer.addClass(this.small, 'p-invalid');
    } else {
      this.renderer.removeClass(this.small, 'p-invalid');
    }
  }

}
