import {EMPTY, Subject} from 'rxjs';
import {ValidationErrorsDirective} from './validation-errors.directive';


describe('ValidationErrorsDirective', () => {
  it('should create an empty div and append it to the element', () => {
    const renderer: any = {createElement: jest.fn(), appendChild: jest.fn()};
    spyOn(renderer, 'createElement').and.returnValue({qwer: 'asdf'});
    const el: any = {nativeElement: {parentNode: {asdf: 'qwer'}}};
    const formControl: any = {valueChanges: EMPTY};

    const directive = new ValidationErrorsDirective(formControl, renderer, el);
    directive.ngAfterViewInit();

    expect(renderer.createElement).toHaveBeenCalledWith('div');
    expect(renderer.appendChild).toHaveBeenCalledWith({asdf: 'qwer'}, {qwer: 'asdf'});
  });

  it('should render all found errors', () => {
    const renderer: any = {
      createElement: jest.fn(),
      appendChild: jest.fn(),
      setStyle: jest.fn(),
      addClass: jest.fn(),
      setProperty: jest.fn()
    };
    const errorDiv: any = {appendChild: jest.fn()};
    const errorSmall = {};
    spyOn(renderer, 'createElement').and.returnValues(errorDiv, errorSmall);
    const el: any = {nativeElement: {parentNode: {asdf: 'qwer'}}};

    const valueChanges$ = new Subject();
    const formControl: any = {valueChanges: valueChanges$.asObservable(), status: 'INVALID', errors: {error1: {}, error2: {}}};

    const directive = new ValidationErrorsDirective(formControl, renderer, el);
    directive.errors = {error1: 'error1Value', error3: 'error3Value'};
    directive.ngAfterViewInit();

    expect(renderer.createElement).toHaveBeenCalledWith('div');
    expect(renderer.appendChild).toHaveBeenCalledWith({asdf: 'qwer'}, errorDiv);

    valueChanges$.next('any');
    expect(renderer.setStyle).toHaveBeenCalledTimes(1);
    expect(renderer.addClass).toHaveBeenCalledWith(errorSmall, 'p-invalid');
    expect(renderer.setProperty).toHaveBeenCalledWith(errorSmall, 'innerText', 'error1Value');
    expect(errorDiv.appendChild).toHaveBeenCalledWith(errorSmall);
  });
});
