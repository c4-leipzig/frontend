import {AfterViewInit, Directive, ElementRef, Input, OnDestroy, Renderer2} from '@angular/core';
import {NgControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

/**
 * Directive empfängt eine Liste von Errors, die für das Parent-Element auftreten können.
 * Tritt ein Fehler auf, wird dessen Repräsentation im DOM eingefügt.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Directive({
  selector: '[c4ValidationErrors]'
})
export class ValidationErrorsDirective implements AfterViewInit, OnDestroy {
  @Input('c4ValidationErrors') errors: { [key: string]: string };
  errorDiv: HTMLDivElement;

  private destroy$ = new Subject();

  constructor(private formControl: NgControl, private renderer: Renderer2, private el: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.errorDiv = this.renderer.createElement('div');
    this.renderer.appendChild(this.el.nativeElement.parentNode, this.errorDiv);
    this.formControl.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.clearErrors();
        if (this.formControl.status === 'INVALID' && this.formControl.errors) {
          Object.keys(this.errors)
            .filter(errorKey => this.formControl.errors[errorKey])
            .forEach(errorKey => this.addError(this.errors[errorKey]));
        }
      });
  }

  private addError(error: string): void {
    const errorRepresentation = this.renderer.createElement('small');
    this.renderer.setStyle(errorRepresentation, 'display', 'block');
    this.renderer.addClass(errorRepresentation, 'p-invalid');
    this.renderer.setProperty(errorRepresentation, 'innerText', error);
    this.errorDiv.appendChild(errorRepresentation);
  }

  private clearErrors(): void {
    let lastChild = this.errorDiv.lastChild;
    while (lastChild) {
      this.errorDiv.removeChild(lastChild);
      lastChild = this.errorDiv.lastChild;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}
