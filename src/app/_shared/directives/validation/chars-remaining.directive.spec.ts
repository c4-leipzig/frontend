import {CharsRemainingDirective} from './chars-remaining.directive';

describe('CharsRemainingDirective', () => {
  let renderer: any;
  let el: any;
  let displayElement: any;
  let directive: CharsRemainingDirective;

  beforeEach(() => {
    renderer = {
      createElement: jest.fn(),
      appendChild: jest.fn(),
      setProperty: jest.fn(),
      addClass: jest.fn(),
      removeClass: jest.fn()
    };
    el = {nativeElement: {parentNode: {parent: 'node'}}};
    displayElement = {display: 'element'};
    directive = new CharsRemainingDirective(el, renderer);
    spyOn(renderer, 'createElement').and.returnValue(displayElement);
  });

  it('should create and append element to display remaining chars ', () => {
    const remaining = 3;
    directive.ngAfterViewInit();

    directive.charsLeft = remaining;

    expect(renderer.createElement).toHaveBeenCalledWith('small');
    expect(renderer.appendChild).toHaveBeenCalledWith({parent: 'node'}, displayElement);
    expect(renderer.setProperty).toHaveBeenCalledWith(displayElement, 'innerText', `Noch ${remaining} Zeichen.`);
    expect(renderer.removeClass).toHaveBeenCalledWith(displayElement, 'p-invalid');
  });

  it('should update display when remaining value changes', () => {
    directive.ngAfterViewInit();
    expect(renderer.setProperty).toHaveBeenNthCalledWith(1, displayElement, 'innerText', 'Maximallänge erreicht.');
    expect(renderer.removeClass).toHaveBeenNthCalledWith(1, displayElement, 'p-invalid');
    directive.charsLeft = 3;
    expect(renderer.setProperty).toHaveBeenNthCalledWith(2, displayElement, 'innerText', 'Noch 3 Zeichen.');
    expect(renderer.removeClass).toHaveBeenNthCalledWith(2, displayElement, 'p-invalid');
    directive.charsLeft = 2;
    expect(renderer.setProperty).toHaveBeenNthCalledWith(3, displayElement, 'innerText', 'Noch 2 Zeichen.');
    expect(renderer.removeClass).toHaveBeenNthCalledWith(3, displayElement, 'p-invalid');
    directive.charsLeft = 1;
    expect(renderer.setProperty).toHaveBeenNthCalledWith(4, displayElement, 'innerText', 'Noch 1 Zeichen.');
    expect(renderer.removeClass).toHaveBeenNthCalledWith(4, displayElement, 'p-invalid');
    directive.charsLeft = 0;
    expect(renderer.setProperty).toHaveBeenNthCalledWith(5, displayElement, 'innerText', 'Maximallänge erreicht.');
    expect(renderer.removeClass).toHaveBeenNthCalledWith(5, displayElement, 'p-invalid');
    directive.charsLeft = -1;
    expect(renderer.setProperty).toHaveBeenNthCalledWith(6, displayElement, 'innerText', 'Maximallänge erreicht.');
    expect(renderer.addClass).toHaveBeenNthCalledWith(1, displayElement, 'p-invalid');

  });
});
