/**
 * Interface für Formular-Komponenten, die üer den {@link LeaveFormGuard} geschützt werden sollen.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export interface FormComponent {
  isDirty: boolean;
}
