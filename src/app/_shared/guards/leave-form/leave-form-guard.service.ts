import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {DialogService} from 'primeng/dynamicdialog';
import {Observable} from 'rxjs';
import {UnsavedChangesLeaveDialogComponent} from '../../components/unsaved-changes-leave-dialog/unsaved-changes-leave-dialog.component';
import {FormComponent} from './form-component';

/**
 * Guard der verhindert, dass ein Formular verlassen wird, wenn es ungespeicherte Änderungen gibt.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Injectable({
  providedIn: 'root'
})
export class LeaveFormGuard implements CanDeactivate<FormComponent> {

  constructor(private router: Router, private dialogService: DialogService) {
  }

  canDeactivate(
    component: FormComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!component.isDirty || this.router.getCurrentNavigation().extras?.state?.canDeactivate) {
      return true;
    }
    const dialogRef = this.dialogService.open(UnsavedChangesLeaveDialogComponent, {
      header: 'Änderungen verwerfen',
      width: '300px',
      closable: false
    });
    return dialogRef.onClose;
  }

}
