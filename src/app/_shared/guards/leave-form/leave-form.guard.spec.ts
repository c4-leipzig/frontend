import {TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {cold} from 'jest-marbles';
import {DialogService} from 'primeng/dynamicdialog';
import {of} from 'rxjs';

import {LeaveFormGuard} from './leave-form-guard.service';

describe('LeaveFormGuard', () => {
  let guard: LeaveFormGuard;
  let router: Router;
  let dialogService: DialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: DialogService,
          useValue: {open: jest.fn()}
        }
      ]
    });
    guard = TestBed.inject(LeaveFormGuard);
    router = TestBed.inject(Router);
    dialogService = TestBed.inject(DialogService);
  });

  it('should allow deactivation if component is not dirty', () => {
    expect(guard.canDeactivate({isDirty: false}, undefined, undefined, undefined)).toBe(true);
  });

  it('should allow deactivation if component is dirty but canDeactivate-flag is set in router state', () => {
    spyOn(router, 'getCurrentNavigation').and.returnValue({extras: {state: {canDeactivate: true}}});
    expect(guard.canDeactivate({isDirty: true}, undefined, undefined, undefined)).toBe(true);
    expect(router.getCurrentNavigation).toHaveBeenCalledTimes(1);
  });

  it('should open confirmation dialog and return its result if component is dirty and flag is not set', () => {
    spyOn(dialogService, 'open').and.returnValue({onClose: of('asdf')});
    spyOn(router, 'getCurrentNavigation').and.returnValue({extras: {state: {canDeactivate: false}}});

    const result = guard.canDeactivate({isDirty: true}, undefined, undefined, undefined);
    expect(dialogService.open).toHaveBeenCalledTimes(1);
    expect(result).toBeObservable(cold('(a|)', {a: 'asdf'}));
  });
});
