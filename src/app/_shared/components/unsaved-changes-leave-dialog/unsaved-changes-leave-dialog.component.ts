import {Component} from '@angular/core';
import {DynamicDialogRef} from 'primeng/dynamicdialog';

/**
 * Simples Modal, welches abfragt, ob ungespeicherte Änderungen verworfen werden sollen und dies im Close-Event mitteilt.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Component({
  selector: 'c4-unsaved-changes-leave-dialog',
  templateUrl: './unsaved-changes-leave-dialog.component.html',
  styleUrls: ['./unsaved-changes-leave-dialog.component.scss']
})
export class UnsavedChangesLeaveDialogComponent {

  constructor(private dialogRef: DynamicDialogRef) {
  }

  confirm(): void {
    this.dialogRef.close(true);
  }

  abort(): void {
    this.dialogRef.close(false);
  }
}
