import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {DynamicDialogRef} from 'primeng/dynamicdialog';

import { UnsavedChangesLeaveDialogComponent } from './unsaved-changes-leave-dialog.component';

describe('UnsavedChangesLeaveDialogComponent', () => {
  let component: UnsavedChangesLeaveDialogComponent;
  let fixture: ComponentFixture<UnsavedChangesLeaveDialogComponent>;
  let dynamicDialogRef;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnsavedChangesLeaveDialogComponent ],
      providers: [
        {
          provide: DynamicDialogRef,
          useValue: {close: jest.fn()}
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsavedChangesLeaveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dynamicDialogRef = TestBed.inject(DynamicDialogRef);
  });

  it('should emit true on close if dialog is confirmed', () => {
    component.confirm();
    expect(dynamicDialogRef.close).toHaveBeenCalledWith(true);
  });

  it('should emit false on close if dialog is aborted', () => {
    component.abort();
    expect(dynamicDialogRef.close).toHaveBeenCalledWith(false);
  });
});
