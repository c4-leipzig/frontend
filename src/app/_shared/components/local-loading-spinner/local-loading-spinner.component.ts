import {Component} from '@angular/core';

@Component({
  selector: 'c4-local-loading-spinner',
  templateUrl: './local-loading-spinner.component.html',
  styleUrls: ['./local-loading-spinner.component.scss']
})
export class LocalLoadingSpinnerComponent {
}
