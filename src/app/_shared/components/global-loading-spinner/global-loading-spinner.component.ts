import {Component} from '@angular/core';

@Component({
  selector: 'c4-global-loading-spinner',
  templateUrl: './global-loading-spinner.component.html',
  styleUrls: ['./global-loading-spinner.component.scss']
})
export class GlobalLoadingSpinnerComponent {
}
