import {FormControl} from '@angular/forms';
import {cold} from 'jest-marbles';
import {of} from 'rxjs';
import {validateValueInList} from './validate-value-in-list';

describe('validateValueInList', () => {
  it('should return null if the value within the formControl is present in the list', () => {
    const list = [1, 2, 3];
    const validator = validateValueInList(of(list));
    const formControl: FormControl = new FormControl(2);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: null}));
  });

  it('should return a ValidationError if the value of the formControl is not present in the list', () => {
    const list = [1, 2, 3];
    const validator = validateValueInList(of(list));
    const formControl: FormControl = new FormControl(5);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: {elementMissing: {valid: false}}}));
  });
});
