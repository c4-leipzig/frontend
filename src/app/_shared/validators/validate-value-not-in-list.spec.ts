import {FormControl} from '@angular/forms';
import {cold} from 'jest-marbles';
import {of} from 'rxjs';
import {validateValueNotInList} from './validate-value-not-in-list';

describe('validateValueNotInList', () => {
  it('should return null if the value of the formControl is not present in the list', () => {
    const list = [1, 2, 3];
    const validator = validateValueNotInList(of(list));
    const formControl: FormControl = new FormControl(5);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: null}));
  });

  it('should return a ValidationError if the value of the formControl is present in the list', () => {
    const list = [1, 2, 3];
    const validator = validateValueNotInList(of(list));
    const formControl: FormControl = new FormControl(2);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: {elementPresent: {valid: false}}}));
  });

  it('should not return an error if self is provided and value equals self', () => {
    const list = [1, 2, 3];
    const validator = validateValueNotInList(of(list), 2);
    const formControl: FormControl = new FormControl(2);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: null}));
  });

  it('should should return an error if self is provided and value is present and does not equal self', () => {
    const list = [1, 2, 3];
    const validator = validateValueNotInList(of(list), 3);
    const formControl: FormControl = new FormControl(2);

    expect(validator(formControl)).toBeObservable(cold('(a|)', {a: {elementPresent: {valid: false}}}));
  });
});
