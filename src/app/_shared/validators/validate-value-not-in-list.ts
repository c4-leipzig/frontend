import {FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {map, take} from 'rxjs/operators';

/**
 * Liefert eine {@link ValidatorFn} zurück, die validiert, dass ein Wert eines {@link FormControl}s nicht in einer Liste enthalten ist.
 * @param list List (als Observable), die überprüft werden soll
 * @param self Optional, ursprünglicher eigener Wert, der bei der Überprüfung ignoriert werden soll.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export function validateValueNotInList<T>(list: Observable<T[]>, self?: T): ValidatorFn {
  return (control: FormControl): Observable<ValidationErrors | null> => {
    if (control.value === self) {
      return of(null);
    }
    return list.pipe(take(1), map(listArr => listArr.some(listElement => listElement === control.value)),
      map(exists => exists ? {elementPresent: {valid: false}} : null));
  };
}
