import {FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';

/**
 * Liefert eine {@link ValidatorFn}, die validiert, dass ein Value Teil einer Liste von Werten ist.
 * @param list Liste (als Observable), die überprüft werden soll.
 * <br/>
 * Copyright: Copyright (c) 01.10.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
export function validateValueInList<T>(list: Observable<T[]>): ValidatorFn {
  return (control: FormControl): Observable<ValidationErrors | null> => {
    return list.pipe(take(1), map(listArr => listArr.some(listElement => listElement === control.value)),
      map(exists => exists ? null : {elementMissing: {valid: false}}));
  };
}
