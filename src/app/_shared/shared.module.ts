import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ButtonModule} from 'primeng/button';
import {GlobalLoadingSpinnerComponent} from './components/global-loading-spinner/global-loading-spinner.component';
import {LocalLoadingSpinnerComponent} from './components/local-loading-spinner/local-loading-spinner.component';
import {UnsavedChangesLeaveDialogComponent} from './components/unsaved-changes-leave-dialog/unsaved-changes-leave-dialog.component';
import {HideForMobileDirective} from './directives/show-for-screen-width/hide-for-mobile.directive';
import {ShowForMobileDirective} from './directives/show-for-screen-width/show-for-mobile.directive';
import {TooltipForScreenSizeDirective} from './directives/tooltip/tooltip-for-screen-size.directive';
import {TooltipMdMaxDirective} from './directives/tooltip/tooltip-md-max.directive';
import {TooltipDirective} from './directives/tooltip/tooltip.directive';
import {CharsRemainingDirective} from './directives/validation/chars-remaining.directive';
import {ValidationErrorsDirective} from './directives/validation/validation-errors.directive';
import {CustomDatePipe} from './pipes/custom-date.pipe';
import {DefaultIfEmptyPipe} from './pipes/default-if-empty.pipe';


@NgModule({
  declarations: [
    GlobalLoadingSpinnerComponent,
    CustomDatePipe,
    LocalLoadingSpinnerComponent,
    DefaultIfEmptyPipe,
    ShowForMobileDirective,
    HideForMobileDirective,
    CharsRemainingDirective,
    ValidationErrorsDirective,
    UnsavedChangesLeaveDialogComponent,
    TooltipDirective,
    TooltipForScreenSizeDirective,
    TooltipMdMaxDirective
  ],
  exports: [
    GlobalLoadingSpinnerComponent,
    LocalLoadingSpinnerComponent,
    CustomDatePipe,
    DefaultIfEmptyPipe,
    ShowForMobileDirective,
    HideForMobileDirective,
    CharsRemainingDirective,
    ValidationErrorsDirective,
    TooltipDirective,
    TooltipMdMaxDirective
  ],
  imports: [
    CommonModule,
    ButtonModule
  ]
})
export class SharedModule {
}
