import {DatePipe} from '@angular/common';
import { CustomDatePipe } from './custom-date.pipe';

describe('CustomDatePipe', () => {
  it('create an instance', () => {
    const pipe = new CustomDatePipe('de');
    expect(pipe).toBeTruthy();
  });

  it('should yield the same results as DatePipe with specific format', () => {
    const customPipe = new CustomDatePipe('en');
    const datePipe = new DatePipe('en');
    expect(customPipe.transform('2020-09-24')).toStrictEqual(datePipe.transform('2020-09-24', CustomDatePipe.format));

    const date = new Date();
    expect(customPipe.transform(date)).toStrictEqual(datePipe.transform(date, CustomDatePipe.format));
  });

  it('should format dates as dd.MM.yyyy', () => {
    const customPipe = new CustomDatePipe('en');

    expect(customPipe.transform('2020-9-24')).toStrictEqual('24.09.2020');
  });
});
