import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultIfEmpty'
})
export class DefaultIfEmptyPipe implements PipeTransform {
  public static readonly defaultValue = '-';

  transform(value: any): any {
    return value || DefaultIfEmptyPipe.defaultValue;
  }
}
