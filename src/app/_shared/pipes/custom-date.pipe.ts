import {DatePipe} from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe extends DatePipe implements PipeTransform {
  public static readonly format = 'dd.MM.yyyy';

  transform(value: any): string | null {
    return super.transform(value, CustomDatePipe.format);
  }

}
