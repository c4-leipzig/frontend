import { DefaultIfEmptyPipe } from './default-if-empty.pipe';

describe('EmptyValuePipe', () => {
  it('create an instance', () => {
    const pipe = new DefaultIfEmptyPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return the value if it is truthy', () => {
    const pipe = new DefaultIfEmptyPipe();
    expect(pipe.transform('asdf')).toStrictEqual('asdf');
    expect(pipe.transform({asdf: 'qwer'})).toStrictEqual({asdf: 'qwer'});
  });

  it('should return a default display string if value is falsy', () => {
    const pipe = new DefaultIfEmptyPipe();

    expect(pipe.transform(undefined)).toStrictEqual(DefaultIfEmptyPipe.defaultValue);
    expect(pipe.transform('')).toStrictEqual(DefaultIfEmptyPipe.defaultValue);

  });
});
