import {parseUrl} from './parse-url';

describe('parseUrl', () => {
  it('should throw an error if number of arguments does not match with template', () => {
    expect(() => parseUrl(`{1}/{2}`, '1')).toThrowError();
  });

  it('should replace occurrences in the correct order', () => {
    expect(parseUrl('{1}/{2}', 'first', 'second')).toStrictEqual('first/second');
  });
});
