const pattern = new RegExp(/{[^/]+}/);
const countPattern = new RegExp(/{[^/]+}/g);

export function parseUrl(urlTemplate: string, ...args: any[]): string {
  let result = urlTemplate;
  const count = result.match(countPattern).length;

  if (args.length !== count) {
    throw new Error();
  }

  for (let i = 0; i < count; i++) {
    result = result.replace(pattern, args[i]);
  }

  return result;
}
