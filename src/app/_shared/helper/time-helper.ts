import dayjs from 'dayjs';
import {environment} from '../../../environments/environment';
import {ListState} from '../../_domain/state/list-state';

export function isCacheInvalid(list: ListState<any>): boolean {
  return dayjs().diff(list.cacheTime) > environment.cacheTime;
}

export function currentTime(): number {
  return dayjs().valueOf();
}
