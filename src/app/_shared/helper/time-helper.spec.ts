import {environment} from '../../../environments/environment';
import {ListState} from '../../_domain/state/list-state';
import {currentTime, isCacheInvalid} from './time-helper';

describe('TimeHelper', () => {
  describe('currentTime', () => {
    it('should yield the current timestamp', () => {
      const actual = new Date().getTime();
      const result = currentTime();

      expect(result).toBeGreaterThanOrEqual(actual);
      expect(result).toBeLessThan(actual + 10);
    });
  });

  describe('isCacheInvalid', () => {
    it('should yield true if difference between cache and now is greater than threshold', () => {
      const actual = new Date().getTime();
      const list: ListState<number> = {isLoading: false, cacheTime: actual - (environment.cacheTime + 100)};

      expect(isCacheInvalid(list)).toBe(true);
    });

    it('should yield false if difference between cache and now is less than threshold', () => {
      const actual = new Date().getTime();
      const list: ListState<number> = {isLoading: false, cacheTime: actual - (environment.cacheTime - 100)};

      expect(isCacheInvalid(list)).toBe(false);
    });
  });
});
