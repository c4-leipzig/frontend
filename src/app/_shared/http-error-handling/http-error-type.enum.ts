export const enum RequestErrorType {
  server = 'server',
  user = 'user',
  notFound = 'notFound',
  notAuthorized = 'notAuthorized'
}
