import {HttpErrorResponse, HttpHandler} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {throwError} from 'rxjs';

import {ErrorHandlerInterceptor} from './error-handler.interceptor';
import {ErrorKey} from './error-key.enum';
import {RequestErrorType} from './http-error-type.enum';
import {RequestError} from './request-error';

describe('ErrorHandlerInterceptor', () => {
  let interceptor: ErrorHandlerInterceptor;
  let mockHttpHandler: HttpHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ErrorHandlerInterceptor
      ]
    });

    interceptor = TestBed.inject(ErrorHandlerInterceptor);
  });

  it('should map bad requests correctly and populate errors', (done) => {
    mockHttpHandler = {handle: () => throwError(new HttpErrorResponse({error: {errors: ['USER_NOT_FOUND', 'ID_INVALID']}, status: 400}))};

    interceptor.intercept({} as any, mockHttpHandler)
      .subscribe({
        error: (error: RequestError) => {
          expect(error).toStrictEqual({type: RequestErrorType.user, errors: [ErrorKey.userNotFound, ErrorKey.idInvalid]});
          done();
        }
      });
  });

  it('should map not authorized requests correctly', (done) => {
    mockHttpHandler = {handle: () => throwError(new HttpErrorResponse({status: 401}))};

    interceptor.intercept({} as any, mockHttpHandler)
      .subscribe({
        error: (error: RequestError) => {
          expect(error).toStrictEqual({type: RequestErrorType.notAuthorized});
          done();
        }
      });
  });

  it('should map forbidden requests correctly', (done) => {
    mockHttpHandler = {handle: () => throwError(new HttpErrorResponse({status: 403}))};

    interceptor.intercept({} as any, mockHttpHandler)
      .subscribe({
        error: (error: RequestError) => {
          expect(error).toStrictEqual({type: RequestErrorType.notAuthorized});
          done();
        }
      });
  });

  it('should map not found errors correctly', (done) => {
    mockHttpHandler = {handle: () => throwError(new HttpErrorResponse({status: 404}))};

    interceptor.intercept({} as any, mockHttpHandler)
      .subscribe({
        error: (error: RequestError) => {
          expect(error).toStrictEqual({type: RequestErrorType.notFound});
          done();
        }
      });
  });

  it('should map server errors correctly', (done) => {
    mockHttpHandler = {handle: () => throwError(new HttpErrorResponse({status: 500}))};

    interceptor.intercept({} as any, mockHttpHandler)
      .subscribe({
        error: (error: RequestError) => {
          expect(error).toStrictEqual({type: RequestErrorType.server});
          done();
        }
      });
  });
});
