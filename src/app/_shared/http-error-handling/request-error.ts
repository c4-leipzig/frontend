import {ErrorKey} from './error-key.enum';
import {RequestErrorType} from './http-error-type.enum';

export interface RequestError {
  type: RequestErrorType;
  errors?: ErrorKey[];
}
