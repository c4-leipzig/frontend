import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {ErrorKey} from './error-key.enum';
import {RequestErrorType} from './http-error-type.enum';
import {RequestError} from './request-error';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (!environment.production) {
          console.log(error);
        }

        let errorResult: RequestError;
        switch (error.status) {
          case 400:
            errorResult = {type: RequestErrorType.user, errors: error.error?.errors as ErrorKey[]};
            break;
          case 401:
          case 403:
            errorResult = {type: RequestErrorType.notAuthorized};
            break;
          case 404:
            errorResult = {type: RequestErrorType.notFound};
            break;
          default:
            errorResult = {type: RequestErrorType.server};
        }
        return throwError(errorResult);
      }));
  }
}
