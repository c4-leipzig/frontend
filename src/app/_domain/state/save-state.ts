import {RequestError} from '../../_shared/http-error-handling/request-error';

export interface SaveState<T> {
  element?: T;
  isSubmitting: boolean;
  submitError?: RequestError;
}

export const initialSaveStore: SaveState<any> = {
  isSubmitting: false
};

export const enum SaveType {
  create,
  update
}
