import {RequestError} from '../../_shared/http-error-handling/request-error';

export interface ListState<T> {
  cacheTime: number;
  list?: T[];
  isLoading: boolean;
  loadError?: RequestError;
}

export const initialListStore: ListState<any> = {
  cacheTime: 0,
  isLoading: false
};
